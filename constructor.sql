-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Янв 14 2017 г., 11:24
-- Версия сервера: 10.1.10-MariaDB
-- Версия PHP: 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `constructor`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name_tag` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name_tag`, `name`) VALUES
(1, 'business', 'бизнес и услуги'),
(2, 'products', 'товары'),
(3, 'portfolio', 'портфолио'),
(4, 'event', 'событие'),
(5, 'startup', 'стартап');

-- --------------------------------------------------------

--
-- Структура таблицы `cat_temp`
--

CREATE TABLE `cat_temp` (
  `cat_id` int(11) NOT NULL,
  `temp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cat_temp`
--

INSERT INTO `cat_temp` (`cat_id`, `temp_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 7),
(3, 8),
(4, 9),
(4, 10),
(5, 11),
(5, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `templates`
--

INSERT INTO `templates` (`id`, `name`, `cat_id`, `path`, `image_path`) VALUES
(1, 'demo1', 1, 'templates/demo1/demo.pmbuilder', ''),
(2, 'demo2', 1, 'templates/demo2/demo2.pmbuilder', ''),
(3, 'demo3', 1, 'templates/demo1/demo.pmbuilder', ''),
(4, 'demo4', 2, 'templates/demo1/demo.pmbuilder', ''),
(5, 'demo5', 2, 'templates/demo1/demo.pmbuilder', ''),
(6, 'demo6', 2, 'templates/demo1/demo.pmbuilder', ''),
(7, 'demo7', 3, 'templates/demo1/demo.pmbuilder', ''),
(8, 'demo8', 3, 'templates/demo1/demo.pmbuilder', ''),
(9, 'demo9', 4, 'templates/demo1/demo.pmbuilder', ''),
(10, 'demo10', 4, 'templates/demo1/demo.pmbuilder', ''),
(11, 'demo11', 5, 'templates/demo1/demo.pmbuilder', ''),
(12, 'demo12', 5, 'templates/demo1/demo.pmbuilder', '');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `username`, `name`) VALUES
(244, 'a@mail.ru', '1', '16IT06', NULL),
(245, 'almat@mail.ru', '1', 'almat', NULL),
(246, 'new@mail.ru', '1', 'newuser', NULL),
(247, 'n@mail.ru', '1', 'n', NULL),
(248, 'nike@mail.ru', '1', 'nike', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_images`
--

CREATE TABLE `user_images` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_images`
--

INSERT INTO `user_images` (`id`, `path`, `user_id`) VALUES
(33, '245/10.png', 245),
(35, '245/wallpaper.png', 245),
(36, '245/frisko.jpg', 245),
(37, '245/1-min_1_9.jpg', 245),
(38, '245/edd13d41-441c-4b26-a33d-2892fc0b84b8.jpg', 245),
(39, '245/imagey.png', 245),
(40, '245/post_logo_black.png', 245),
(44, '245/astronomy.png', 245),
(45, '245/foundry.png', 245),
(46, '245/feff.png', 245),
(47, '245/home1.jpg', 245),
(48, '245/astronomy.png', 245),
(49, '245/edd13d41-441c-4b26-a33d-2892fc0b84b8.jpg', 245),
(50, '245/fased.png', 245),
(51, '245/feff.png', 245),
(52, '245/foundry.png', 245),
(53, '245/home1.jpg', 245),
(54, '245/edd13d41-441c-4b26-a33d-2892fc0b84b8.jpg', 245),
(59, '245/quantico.png', 245),
(60, '245/quantico.png', 245),
(61, '245/fased.png', 245),
(62, '245/logo-light.png', 245),
(63, '247/fased.png', 247),
(64, '247/misterybray.png', 247),
(65, '247/5-min-planka_1449832787.jpg', 247),
(66, '247/DLA2YO2jtMQ.png', 247),
(67, '247/qwe.png', 247),
(68, '247/23.png', 247),
(69, '247/321.png', 247),
(70, '248/fased.png', 248),
(71, '248/astronomy.png', 248),
(72, '248/feff.png', 248);

-- --------------------------------------------------------

--
-- Структура таблицы `user_sites`
--

CREATE TABLE `user_sites` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `template_path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_sites`
--

INSERT INTO `user_sites` (`id`, `name`, `user_id`, `template_path`) VALUES
(1, '16IT06_site_0', 244, '16IT06/0/16IT06.pmbuilder'),
(22, '16IT06_site_1', 244, '16IT06/1/16IT06.pmbuilder'),
(26, '16IT06_site_2', 244, '16IT06/2/16IT06.pmbuilder'),
(27, 'newuser_site_3', 246, 'newuser/3/newuser.pmbuilder'),
(28, 'n_site_0', 247, 'n/0/n.pmbuilder'),
(29, 'newuser_site_1', 246, 'newuser/1/newuser.pmbuilder'),
(30, 'almat_site_0', 245, 'almat/0/almat.pmbuilder'),
(31, 'almat_site_1', 245, 'almat/1/almat.pmbuilder'),
(32, 'nike_site_0', 248, 'nike/0/nike.pmbuilder');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_sites`
--
ALTER TABLE `user_sites`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;
--
-- AUTO_INCREMENT для таблицы `user_images`
--
ALTER TABLE `user_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT для таблицы `user_sites`
--
ALTER TABLE `user_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
