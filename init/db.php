<?php
$opt = [
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
  PDO::ATTR_EMULATE_PREPARES   => false
];
ini_set('display_errors', 1);
$pdo = new PDO('mysql:host=127.0.0.1;dbname=constructor;charset=utf8', 'user', '', $opt);