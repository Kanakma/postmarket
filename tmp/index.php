<?php

error_reporting(0);
$siteName='';
$errorMsg='';
if($_GET['siteName'] )
{
$sitePostName=$_GET['siteName'];
$siteNameCheck = preg_match('~^[A-Za-z0-9_]{3,20}$~i', $sitePostName);
if($siteNameCheck)
{
	$siteName=$sitePostName;
    header("Location: http://$siteName.almatybray.tk");
}
else
{
  header("Location: http://almatybray.tk/404.php");
}
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf8">
<title>Create a Subdomain using PHP and .Htaccess</title>


    <link href='https://fonts.googleapis.com/css?family=Montserrat%3A400%2C700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

</head>
<body>
<div id="container">
<?php if(empty($siteName)) { ?>
<h1>Create a Page</h1>

<form action="" method="get">
    <input type="text" id="subDomain" name="siteName" />
    <span id="domainName">.almatybray.tk</span>
    <div id="errorMsg"></div>

    <div>
        <input type="submit" value="Get Started" id="getStarted" >
    </div>
</form>


<?php } else { ?>
<h1>Welcome to <span class="dark"><?php echo ucfirst($siteName);  ?></span> Page </h1>

http://<span class="dark"><?php echo ucfirst($siteName);  ?></span>.almatybray.tk
<?php
} 
?>
</div>

</body>
</html>