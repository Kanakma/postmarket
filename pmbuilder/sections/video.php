<section id="pm-slider-2" class="vim" vbp="заголовок,полный экран,видео" vbr="Встраивание видео" icons="youtube-vimeo">
  <section class="fullscreen cover parallax image-bg overlay">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/home-2-1.jpg">
    </div>
    <div class="container v-align-transform">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h1 class="mb16">Шаблонизатор - это готовый инструмент
            <br class="hidden-sm"> дизайна,идеальный для вашего следующего проекта.</h1>
          <h6 class="uppercase mb32">Модульная архитектура для абсолютной свободы.</h6>
          <div class="modal-container">
            <div class="play-button btn-modal inline large"></div>
            <div class="pm_modal no-bg">
              <iframe data-provider="vimeo" data-video-id="25737856" data-autoplay="1" allowfullscreen="allowfullscreen"></iframe>
            </div>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-video-2" class="vim" vbp="видео" vbr="Встроенное видео youtube | vimeo" icons="youtube-vimeo">
  <section class="bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row v-align-children">
        <div class="col-md-7 col-sm-6 text-center mb-xs-24">
          <div class="embed-video-container embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" no-src="http://player.vimeo.com/video/120433187?badge=0&title=0&byline=0&title=0" allowfullscreen="allowfullscreen"></iframe>
          </div>
        </div>
        <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
          <h3>
            Lorem ipsum dolor sit amet
          </h3>
          <p>
            Lorem ipsum dolor sit amet
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<!--<section id="pm-video-3" class="vim" vbp="видео" vbr="Встроенное видео с описанием">
  <section class="bg-primary">
    <div class="container">
      <div class="row mb64 mb-xs-24">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
          <h3 class="mb40 mb-xs-24">
            Lorem ipsum dolor sit amet
          </h3>
          <p class="lead">
            Lorem ipsum dolor sit amet
          </p>
        </div>
      </div>

      <div class="row mb48 mb-xs-24">
        <div class="col-sm-8 col-sm-offset-2">
          <div class="local-video-container mb40">
            <div class="background-image-holder">
              <img alt="Background Image" class="background-image" src="../img/capital3.jpg">
            </div>
            <video controls="">
              <source src="../video/video.webm" type="video/webm">
              </source><source src="../video/video.mp4" type="video/mp4">
              </source><source src="../video/video.ogv" type="video/ogg">
              </source></video>
            <div class="play-button dark"></div>
          </div>

        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center spread-children-large">
          <img alt="pic" class="image-xxs mb-xs-8 fade-half" src="../img/c1.png">
          <img alt="pic" class="image-xxs mb-xs-8 fade-half" src="../img/c2.png">
          <img alt="pic" class="image-xxs mb-xs-8 fade-half" src="../img/c3.png">
          <img alt="pic" class="image-xxs mb-xs-8 fade-half" src="../img/c4.png">
        </div>
      </div>

    </div>

  </section>
</section>-->
