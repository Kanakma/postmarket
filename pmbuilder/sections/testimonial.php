<section id="pm-testimonial-1" class="vim" vbp="отзыв" vbr="Отзыв с изображением">
  <section class="image-edge pt120 pb120 pt-xs-40 pb-xs-40 toggle-bg-mrv">
    <div class="col-md-6 col-sm-4 p0 col-md-push-6 col-sm-push-8">
      <img alt="Screenshot" class="cast-shadow mb-xs-24" src="../img/app3.jpg">
    </div>
    <div class="container">
      <div class="col-md-5 col-md-pull-0 col-sm-7 col-sm-pull-4 v-align-transform">
        <h3 class="mb40 mb-xs-16">Используй готовые вариации текста и изображений.</h3>
        <p class="lead mb40">
          Мы построили три разных комбинаций изображений и текстов для вашего показа продукта или услуги.
        </p>
        <div class="feature boxed">
          <p>
            "Это просто потрясающе! Я всегда искал что-то подобное!"
          </p>
          <div class="spread-children">
            <img alt="Pic" class="image-xs" src="img/avatar1.png">
            <span>Джон Доу</span>
          </div>
        </div>
      </div>
    </div>

  </section>
</section>

<section id="pm-testimonial-2" class="vim" vbp="отзыв,слайдер" vbr="Слайдер с отзывами">
  <section class="image-bg overlay parallax">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/app2.jpg">
    </div>
    <div class="container">
      <div class="row mb40 mb-xs-24">
        <div class="col-sm-12 text-center spread-children">
          <img class="image-xs" alt="Pic" src="../img/avatar1.png">
          <img class="image-xs" alt="Pic" src="../img/avatar2.png">
          <img class="image-xs" alt="Pic" src="../img/avatar3.png">
        </div>
      </div>

      <div class="row mb16 mb-xs-0">
        <div class="col-sm-12 text-center">
          <h3>Людям нравится Шаблонизатор</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
          <div class="text-slider slider-arrow-controls text-center relative">
            <ul class="slides">
              <li>
                <p class="lead">
                  Видно, насколько серьезно эти ребята относятся к дизайну.
                  Я никогда не видел такой простоты в использовании.
                </p>
                <div class="quote-author">
                  <h6 class="uppercase mb0">Пользоваетль 1</h6>
                  <span>Клиент Шаблонизатора</span>
                </div>
              </li>
              <li>
                <p class="lead">
                  Я могу быстро проверить разные комбинации и поэкспериментировать с шрифтом и цветами.
                </p>
                <div class="quote-author">
                  <h6 class="uppercase mb0">Пользователь 2</h6>
                  <span>Клиент Шаблонизатора</span>
                </div>
              </li>
              <li>
                <p class="lead">
                  Как любитель шаблонов,
                  я действительно ценю последовательную стилизацию для всех одинаковых тегов,
                  это позволяет кастомизировать разделы легче.
                  5 звезд за невероятные шаблоны.
                </p>
                <div class="quote-author">
                  <h6 class="uppercase mb0">Пользователь 3</h6>
                  <span>Клиент Шаблонизатора</span>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-testimonial-4" class="vim" vbp="отзыв,слайдер" vbr="Слайдер с отзывами 2">
  <section class="bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
          <h3 class="mb64 uppercase">Люди &nbsp;
            <i class="ti-heart"></i>&nbsp; Шаблонизатор</h3>
          <div class="testimonials text-slider slider-arrow-controls">
            <ul class="slides">
              <li>
                <p class="lead">Видно, насколько серьезно эти ребята относятся к дизайну.
                  Я никогда не видел такой простоты в использовании.</p>
                <div class="quote-author">
                  <img alt="Avatar" src="../img/avatar1.png">
                  <h6 class="uppercase">Джон Доу</h6>
                  <span>Клиент Шаблонизатора</span>
                </div>
              </li>
              <li>
                <p class="lead">
                  Я могу быстро проверить разные комбинации и поэкспериментировать с шрифтом и цветами.
                </p>
                <div class="quote-author">
                  <img alt="Avatar" src="../img/avatar2.png">
                  <h6 class="uppercase">Джон Доу</h6>
                  <span>Клиент Шаблонизатора</span>
                </div>
              </li>
              <li>
                <p class="lead">
                  Как любитель шаблонов,
                  я действительно ценю последовательную стилизацию для всех одинаковых тегов,
                  это позволяет кастомизировать разделы легче.
                  5 звезд за невероятные шаблоны.
                </p>
                <div class="quote-author">
                  <img alt="Avatar" src="../img/avatar3.png">
                  <h6 class="uppercase">Джон Доу</h6>
                  <span>Клиент Шаблонизатора</span>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-testimonial-3" class="vim" vbp="отзыв" vbr="Карточки с отзывами">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row mb64 mb-xs-24">
        <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
          <h3>Людям нравится Шаблонизатор</h3>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4 text-center">
          <div class="feature boxed cast-shadow-light" vht="parent">
            <img alt="Pic" class="image-small inline-block mb24" src="../img/avatar1.png">
            <h4>"Невероятно!"</h4>
            <p>
              Видно, насколько серьезно эти ребята относятся к дизайну.
              Я никогда не видел такой простоты в использовании.
            </p>
            <span>
							<strong>Джон Доу</strong> - клиент Шаблонизатора</span>
          </div>
        </div>
        <div class="col-sm-4 text-center">
          <div class="feature boxed cast-shadow-light" vht="parent">
            <img alt="Pic" class="image-small inline-block mb24" src="../img/avatar2.png">
            <h4>"Клиент на всю жизнь"</h4>
            <p>
              Я могу быстро проверить разные комбинации и поэкспериментировать с шрифтом и цветами.
            </p>
            <span>
						<strong>Джон Доу</strong> - клиент Шаблонизатора</span>
          </div>
        </div>
        <div class="col-sm-4 text-center">
          <div class="feature boxed cast-shadow-light" vht="parent">
            <img alt="Pic" class="image-small inline-block mb24" src="../img/avatar3.png">
            <h4>"Слишком легко!"</h4>
            <p>
              Как любитель шаблонов,
              я действительно ценю последовательную стилизацию для всех одинаковых тегов,
              это позволяет кастомизировать разделы легче.
              5 звезд за невероятные шаблоны.
            </p>
            <span>
						<strong>Джон Доу</strong> - клиент Шаблонизатора</span>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-testimonial-5" class="vim" vbp="характеристика" vbr="Отзыв с изображением 2">
  <section class="image-bg bg-light parallax overlay pt160 pb160 pt-xs-80 pb-xs-80">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/intro1.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-sm-8">
          <i class="ti-quote-left icon icon-sm mb16"></i>
          <h3 class="mb32">Шаблонизатор обеспечил нас инструментами, нужными для запуска нашего проекта.</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-testimonial-6" class="vim" vbp="характеристика,новый" vbr="Слайдер с отзывами 3" icons="new">
  <section class="pt180 pb180 pt-xs-80 pb-xs-80">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center">
          <h3 class="mb48 mb-xs-32">Одна из немногих компаний, с которыми нам нравится работать</h3>
          <div class="text-slider slider-paging-controls text-center relative">
            <ul class="slides">
              <li>
                <h5>С этими ребятами одно удовольствие работать - они отнеслись к нашему проекту с таким же энтузиазмом, что и у нас.</h5>
                <div class="quote-author">
                  <img alt="Author" class="image-xs mb16" src="../img/avatar4.png">
                  <h6 class="uppercase mb0">Джон Доу</h6>
                  <span>Компания</span>
                </div>
              </li>
              <li>
                <h5>Одна из немногих компаний, с которыми нам нравится работать</h5>
                <div class="quote-author">
                  <img alt="Author" class="image-xs mb16" src="../img/avatar3.png">
                  <h6 class="uppercase mb0">Джон Доу</h6>
                  <span>Компания</span>
                </div>
              </li>
              <li>
                <h5>С этими ребятами одно удовольствие работать - они отнеслись к нашему проекту с таким же энтузиазмом, что и у нас.</h5>
                <div class="quote-author">
                  <img alt="Author" class="image-xs mb16" src="../img/avatar6.png">
                  <h6 class="uppercase mb0">Джон Доу</h6>
                  <span>Компания</span>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
