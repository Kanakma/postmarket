<section id="pm-pricing-1" class="vim" vbp="цена" vbr="Цена простая">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h4 class="uppercase mb16">Планы краткие</h4>
          <p class="lead mb64">
            Идеально подходит для простых вариантов цен.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="pricing-table pt-1 text-center" vht="parent">
            <h5 class="uppercase">Стартовый</h5>
            <span class="price">$12</span>
            <p class="lead">в месяц</p>
            <a class="btn btn-filled btn-lg" href="#">Начать</a>
            <p>
              30-дневный пробный период
              <br> Без оформления кредитной карты
            </p>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="pricing-table pt-1 text-center boxed" vht="parent">
            <h5 class="uppercase">Стартовый</h5>
            <span class="price">$12</span>
            <p class="lead">в месяц</p>
            <a class="btn btn-filled btn-lg" href="#">Начать</a>
            <p>
              30-дневный пробный период
              <br> Без оформления кредитной карты
            </p>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="pricing-table pt-1 text-center emphasis" vht="parent">
            <h5 class="uppercase">Стартовый</h5>
            <span class="price">$12</span>
            <p class="lead">в месяц</p>
            <a class="btn btn-white btn-lg" href="#">Начать</a>
            <p>
              30-дневный пробный период
              <br> Без оформления кредитной карты
            </p>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-pricing-2" class="vim" vbp="цена" vbr="Цена расширенная">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h4 class="uppercase mb16">Расширенные планы</h4>
          <p class="lead mb64">
            Идеален для планов с деталями.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="pricing-table pt-2 text-center" vht="parent">
            <h5 class="uppercase">Стартовый</h5>
            <span class="price">$12</span>
            <p class="lead">в месяц</p>
            <a class="btn btn-filled btn-lg" href="#">Начать</a>
            <ul>
              <li>
                <strong>Безграничный</strong> доступ к содержимому
              </li>
              <li>
                <strong>Польностью надежное</strong> онлайн хранилище
              </li>
              <li>
                <strong>Целый год</strong> круглосуточной поддержки
              </li>
            </ul>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="pricing-table pt-2 boxed text-center" vht="parent">
            <h5 class="uppercase">Стартовый</h5>
            <span class="price">$12</span>
            <p class="lead">в месяц</p>
            <a class="btn btn-filled btn-lg" href="#">Начать</a>
            <ul>
              <li>
                <strong>Безграничный</strong> доступ к содержимому
              </li>
              <li>
                <strong>Польностью надежное</strong> онлайн хранилище
              </li>
              <li>
                <strong>Целый год</strong> круглосуточной поддержки
              </li>
            </ul>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="pricing-table pt-2 emphasis text-center" vht="parent">
            <h5 class="uppercase">Стартовый</h5>
            <span class="price">$12</span>
            <p class="lead">в месяц</p>
            <a class="btn btn-filled btn-lg" href="#">Начать</a>
            <ul>
              <li>
                <strong>Безграничный</strong> доступ к содержимому
              </li>
              <li>
                <strong>Польностью надежное</strong> онлайн хранилище
              </li>
              <li>
                <strong>Целый год</strong> круглосуточной поддержки
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-pricing-3" class="vim" vbp="цена" vbr="Цена с текстом">
  <section>
    <div class="container">
      <div class="row v-align-children">
        <div class="col-sm-5">
          <h3>Присоединяйтесь к нашему
            <br> ежедневному обсуждению</h3>
          <p class="lead mb40">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
          </p>
          <div class="overflow-hidden mb32 mb-xs-24">
            <i class="ti-package icon icon-sm pull-left"></i>
            <h6 class="uppercase mb0 inline-block p32">Полный пакет</h6>
          </div>
          <div class="overflow-hidden mb32 mb-xs-24">
            <i class="ti-medall-alt icon icon-sm pull-left"></i>
            <h6 class="uppercase mb0 inline-block p32">Доступ к Клубу Шаблонизатора</h6>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="pricing-table pt-1 text-center emphasis">
            <h5 class="uppercase">один билет</h5>
            <span class="price">$89</span>
            <p class="lead">за билет</p>
            <a class="btn btn-white btn-lg" href="#">Приобрести Билет</a>
            <p>
              <a href="#">Свяжитесь с нами</a>
              <br> для большего количества билетов
            </p>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>