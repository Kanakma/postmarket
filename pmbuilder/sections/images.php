<section id="pm-slider-12" class="vim" vbp="заголовок,полный экран" vbr="Изображение и текст">
  <section class="cover fullscreen image-slider slider-arrow-controls controls-inside parallax">
    <ul class="slides">
      <li class="overlay image-bg">
        <div class="background-image-holder">
          <img alt="image" class="background-image" src="../img/cover7.jpg">
        </div>
        <div class="container v-align-transform">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
              <img alt="Logo" class="image-small mb8" src="../img/logo-light.png">
              <h6 class="uppercase mb32">Модульная архитектура для абсолютной свободы.</h6>
              <p class="text-justify mb0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              </p>
            </div>
          </div>

        </div>

      </li>
    </ul>
  </section>
</section>

<section id="pm-slider-5" class="vim" vbp="заголовок,полный экран,видео" vbr="Полноэкранный текст с изображением" icons="youtube-vimeo">
  <section class="fullscreen image-bg">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/vent1.jpg">
    </div>
    <div class="container v-align-transform">
      <div class="row">
        <div class="col-sm-8 col-md-6 mb24">
          <h6 class="uppercase">Путешествуй с любовью</h6>
          <img alt="Pic" src="../img/vent1.png">
        </div>
        <div class="col-sm-12">
          <div class="modal-container pull-left">
            <div class="play-button btn-modal inline"></div>
            <div class="pm_modal no-bg">
              <iframe data-provider="vimeo" data-video-id="25737856" data-autoplay="1" allowfullscreen="allowfullscreen"></iframe>
            </div>
          </div>

          <p class="lead inline-block p32 p0-xs pt8">
            Возьми отпуск на дикую природу с Приключенческими
            <br> Турами Шаблонизатора и взгляни на жизнь с другой стороны.
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-textimage-1" class="vim" vbp="текст+изобр" vbr="Блок с изображением на левой грани">
  <section class="image-edge toggle-bg-mrv">
    <div class="col-md-6 col-sm-4 p0">
      <img alt="Screenshot" class="mb-xs-24" src="../img/phone1.png">
    </div>
    <div class="container">
      <div class="col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 v-align-transform right">
        <h1 class="large mb40 mb-xs-16">Чистые блоки изображений.</h1>
        <h6 class="uppercase mb16">Левые & Правые вариации включены</h6>
        <p class="lead mb40">
          Мы построили три разных комбинаций изображений и текстов для вашего показа продукта или услуги.
          Все комбинации имеют левые и правые расположения.
        </p>
        <a class="btn-lg btn" href="#">Исследовать</a>
      </div>
    </div>

  </section>
</section>

<section id="pm-textimage-2" class="vim" vbp="текст+изобр" vbr="Блок с изображением на правой грани">
  <section class="image-edge">
    <div class="col-md-6 col-sm-4 p0 col-md-push-6 col-sm-push-8">
      <img alt="Screenshot" class="mb-xs-24" src="../img/phone2.png">
    </div>
    <div class="container">
      <div class="col-md-5 col-md-pull-0 col-sm-7 col-sm-pull-4 v-align-transform">
        <h1 class="large mb40 mb-xs-16">Изящный, Умный и Современный.</h1>
        <h6 class="uppercase mb16">Дизайн, которым можно гордиться</h6>
        <p class="lead mb40">
          Мы построили три разных комбинаций изображений и текстов для вашего показа продукта или услуги.
          Все комбинации имеют левые и правые расположения.
        </p>
        <a class="btn-lg btn" href="#">Исследовать</a>
      </div>
    </div>

  </section>
</section>

<section id="pm-textimage-12" class="vim" vbp="текст+изобр,новый" vbr="Блок с изображением на левой грани 2" icons="new">
  <section class="image-edge pt120 pb120 pt-xs-40 pb-xs-40">
    <div class="col-md-6 col-sm-4 p0">
      <img alt="Screenshot" class="mb-xs-24" src="../img/app4.jpg">
    </div>
    <div class="container">
      <div class="col-md-5 col-md-offset-1 col-sm-7 col-sm-offset-1 v-align-transform right">
        <h3 class="mb40 mb-xs-16">Сочетай разные блоки вместе для идеального макета.</h3>
        <p class="lead mb40">
          Мы построили три разных комбинаций изображений и текстов для вашего показа продукта или услуги.
        </p>
        <div class="feature feature-3">
          <div class="left">
            <i class="ti-user icon-sm"></i>
          </div>
          <div class="right">
            <h4 class="mb16">Создан для тебя</h4>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
            </p>
          </div>
        </div>

        <div class="feature feature-3">
          <div class="left">
            <i class="ti-package icon-sm"></i>
          </div>
          <div class="right">
            <h4 class="mb16">Невероятная значимость</h4>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
            </p>
          </div>
        </div>

      </div>
    </div>

  </section>
</section>

<section id="pm-textimage-13" class="vim" vbp="текст+изобр,новый" vbr="Блок с изображением на правой грани 2" icons="new">
  <section class="image-edge pt120 pb120 pt-xs-40 pb-xs-40">
    <div class="col-md-6 col-sm-4 p0 col-md-push-6 col-sm-push-8">
      <img alt="Screenshot" class="cast-shadow mb-xs-24" src="../img/app3.jpg">
    </div>
    <div class="container">
      <div class="col-md-5 col-md-pull-0 col-sm-7 col-sm-pull-4 v-align-transform">
        <h3 class="mb40 mb-xs-16">Используй готовые вариации текста и изображений.</h3>
        <p class="lead mb40">
          Мы построили три разных комбинаций изображений и текстов для вашего показа продукта или услуги.
        </p>
        <div class="feature boxed">
          <p>
            "Это просто потрясающе! Я всегда искал что-то подобное!"
          </p>
          <div class="spread-children">
            <img alt="Pic" class="image-xs" src="img/avatar1.png">
            <span>Джон Доу</span>
          </div>
        </div>
      </div>
    </div>

  </section>
</section>

<section id="pm-textimage-3" class="vim" vbp="текст+изобр" vbr="Блок с изображением слева">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row v-align-children">
        <div class="col-md-7 col-sm-6 text-center mb-xs-24">
          <img class="cast-shadow" alt="Screenshot" src="../img/home1.jpg">
        </div>
        <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
          <h3>Строй изящные, современные сайты быстрее, чем когда-либо</h3>
          <p>
            Шаблонизатор - ваш готовый инструмент дизайна, построенный с нуля.
            Строить изящные, современные сайты никогда не было так легко!
          </p>
          <a class="btn-filled btn" href="#">Исследовать</a>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-textimage-4" class="vim" vbp="текст+изобр" vbr="Блок с изображением справа">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row v-align-children">
        <div class="col-md-4 col-sm-5 mb-xs-24">
          <h3>Внутренние картинки & текст, вертикально центрированные для визуального баланса.</h3>
          <p>
            Можете добавить класс '.cast-shadow' для ваших изображений.
            Это даст изображениям утонченную тень и показывает иллюзию глубины.
          </p>
        </div>
        <div class="col-md-7 col-md-offset-1 col-sm-6 col-sm-offset-1 text-center">
          <img class="cast-shadow" alt="Screenshot" src="../img/home1.jpg">
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-textimage-5" class="vim" vbp="текст+изобр" vbr="Блок с изображением слева 2">
  <section class="image-square left toggle-bg-mrv">
    <div class="col-md-6 image">
      <div class="background-image-holder">
        <img alt="image" class="background-image" src="../img/small1.jpg">
      </div>
    </div>
    <div class="col-md-6 col-md-offset-1 content">
      <h3>Строй изящные, современные сайты быстрее, чем когда-либо</h3>
      <p class="mb0">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco.
      </p>
    </div>
  </section>
</section>

<section id="pm-textimage-6" class="vim" vbp="текст+изобр" vbr="Блок с изображением справа 2">
  <section class="image-square right toggle-bg-mrv">
    <div class="col-md-6 image">
      <div class="background-image-holder">
        <img alt="image" class="background-image" src="../img/small2.jpg">
      </div>
    </div>
    <div class="col-md-6 content">
      <h3>Строй изящные, современные сайты быстрее, чем когда-либо</h3>
      <p class="mb0">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco.
      </p>
    </div>
  </section>
</section>

<section id="pm-textimage-8" class="vim" vbp="текст+изобр" vbr="Изображение продукта">
  <section>
    <div class="container">
      <div class="row v-align-children">
        <div class="col-sm-4 col-md-offset-1 mb-xs-24">
          <h2 class="mb64 mb-xs-32">Встречай Шаблонизатор, твой новый лучший друг.</h2>
          <div class="mb40 mb-xs-24">
            <h5 class="uppercase bold mb16">Скриншоты для вас</h5>
            <p class="fade-1-4">
              Используй почти бесконечное количество макетов Apple Watch, чтобы показать свое приложение в хорошем контексте.
            </p>
          </div>
          <div class="mb40 mb-xs-24">
            <h5 class="uppercase bold mb16">Приостанови поиски</h5>
            <p class="fade-1-4">
              Вот он, идеальный шаблон, который тебе нужен. Он хорошо построен и смотрится изящно, прощай конкуренция!
            </p>
          </div>
        </div>
        <div class="col-sm-5 col-sm-6 col-sm-offset-1 text-center">
          <img alt="Screenshot" src="../img/app7.png">
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-textimage-9" class="vim" vbp="текст+изобр" vbr="Текст над изображением">
  <section class="image-bg overlay parallax pt180 pb180 pt-xs-80 pb-xs-80">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/app9.jpg">
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-5 col-sm-6 col-md-push-7 col-sm-push-6">
          <h2>Делись значимыми моментами с любимыми</h2>
          <p class="lead mb48 mb-xs-32">
            Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
            consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam.
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-textimage-10" class="vim" vbp="текст+изобр" vbr="Изображения с подтекстом">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
          <h4 class="uppercase mb16">Изображения с подтекстом</h4>
          <p class="lead mb80">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </div>
      </div>
      <div class="row mb64 mb-xs-32">
        <div class="col-sm-6">
          <div class="image-caption cast-shadow mb-xs-32" vht="parent">
            <img alt="Captioned Image" src="../img/home7.jpg">
            <div class="caption">
              <p>
                <strong>Заметка:</strong> Это текст изображения
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="image-caption cast-shadow hover-caption" vht="parent">
            <img alt="Captioned Image" src="../img/home8.jpg">
            <div class="caption">
              <p>
                <strong>Заметка:</strong> Здесь изображение текста при наведении.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>

<section id="pm-image-1" class="vim" vbp="портфолио" vbr="Сетка изображений">
  <section class="projects toggle-bg-mrv">
    <div class="container">
      <div class="masonry-loader">
        <div class="col-sm-12 text-center">
          <div class="spinner"></div>
        </div>
      </div>
      <div class="row masonry masonryFlyIn">
        <div class="col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home17.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Пространсто офиса</h3>
                <h6 class="uppercase">Технология / Фотография</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home20.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Террарий</h3>
                <h6 class="uppercase">Сан Хосе / Калифорния</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home14.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Дизайн</h3>
                <h6 class="uppercase">Технология / Фотография</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home18.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Дизайн</h3>
                <h6 class="uppercase">Продуктивность</h6>
              </a>
            </div>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-image-2" class="vim" vbp="портфолио" vbr="Широкая сетка">
  <section class="projects p0 bg-dark">
    <ul class="filters floating cast-shadow mb0 vog">
    </ul>
    <div class="row masonry-loader fixed-center vog">
      <div class="col-sm-12 text-center">
        <div class="spinner"></div>
      </div>
    </div>
    <div class="row masonry masonryFlyIn">
      <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Люди">
        <div class="image-tile inner-title hover-reveal text-center">
          <a href="#">
            <img alt="Pic" src="../img/project-single-1.jpg" vht=".project">
            <div class="title">
              <h5 class="uppercase mb0">Счастливая пара</h5>
              <span>Люди / Жизнь</span>
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Животные">
        <div class="image-tile inner-title hover-reveal text-center">
          <a href="#">
            <img alt="Pic" src="../img/project-single-2.jpg" vht=".project">
            <div class="title">
              <h5 class="uppercase mb0">Одинокая Собака</h5>
              <span>Животные / Искусство</span>
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Люди">
        <div class="image-tile inner-title hover-reveal text-center">
          <a href="#">
            <img alt="Pic" src="../img/project-single-3.jpg" vht=".project">
            <div class="title">
              <h5 class="uppercase mb0">Прелестное утро</h5>
              <span>Люди / Жизнь</span>
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Объекты">
        <div class="image-tile inner-title hover-reveal text-center">
          <a href="#">
            <img alt="Pic" src="../img/project-single-5.jpg" vht=".project">
            <div class="title">
              <h5 class="uppercase mb0">Винтажное фото</h5>
              <span>Объекты</span>
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Объекты">
        <div class="image-tile inner-title hover-reveal text-center">
          <a href="#">
            <img alt="Pic" src="../img/project-single-4.jpg" vht=".project">
            <div class="title">
              <h5 class="uppercase mb0">Летняя любовь</h5>
              <span>Объекты</span>
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Люди">
        <div class="image-tile inner-title hover-reveal text-center">
          <a href="#">
            <img alt="Pic" src="../img/project-single-6.jpg" vht=".project">
            <div class="title">
              <h5 class="uppercase mb0">Туман</h5>
              <span>Природа</span>
            </div>
          </a>
        </div>
      </div>
    </div>

  </section>
</section>

<section id="pm-image-3" class="vim" vbp="портфолио" vbr="Сетка с содержимым">
  <section class="projects pt48">
    <div class="container vog">
      <div class="row pb24 vog">
        <div class="col-sm-12 text-center vog">
          <ul class="filters mb0 vog">
          </ul>
        </div>
      </div>

      <div class="row masonry-loader vog">
        <div class="col-sm-12 text-center">
          <div class="spinner"></div>
        </div>
      </div>
      <div class="row masonry masonryFlyIn">
        <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Люди">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-1.jpg" vht=".project">
              <div class="title">
                <h5 class="uppercase mb0">Счастливая пара</h5>
                <span>Люди</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Животные">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-2.jpg" vht=".project">
              <div class="title">
                <h5 class="uppercase mb0">Одинокая Собака</h5>
                <span>Животные</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Люди">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-3.jpg" vht=".project">
              <div class="title">
                <h5 class="uppercase mb0">Прелестное утро</h5>
                <span>Люди</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Объекты">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-5.jpg" vht=".project">
              <div class="title">
                <h5 class="uppercase mb0">Винтаж</h5>
                <span>Объекты</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Объекты">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-4.jpg" vht=".project">
              <div class="title">
                <h5 class="uppercase mb0">Летняя любовь'</h5>
                <span>Объекты</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project voh" data-filter="Люди">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-6.jpg" vht=".project">
              <div class="title">
                <h5 class="uppercase mb0">Туман</h5>
                <span>Природа</span>
              </div>
            </a>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-projects-1" class="vim" vbp="портфолио,новый" vbr="Сетка с содержимым без зазоров" icons="new">
  <section class="portfolio-pullup">
    <div class="container">
      <div class="row row-gapless masonry masonryFlyIn">
        <div class="col-md-4 col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-1.jpg">
              <div class="title">
                <h5 class="uppercase mb0">Счастливая пара</h5>
                <span>Люди</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project" data-filter="Животные">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-2.jpg">
              <div class="title">
                <h5 class="uppercase mb0">Одинокая Собака</h5>
                <span>Искусство</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-3.jpg">
              <div class="title">
                <h5 class="uppercase mb0">Прелестное утро</h5>
                <span>Жизнь</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project" data-filter="Объекты">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-5.jpg">
              <div class="title">
                <h5 class="uppercase mb0">Винтаж</h5>
                <span>Объекты</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project" data-filter="Объекты">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-4.jpg">
              <div class="title">
                <h5 class="uppercase mb0">Летняя любовь</h5>
                <span>Еда</span>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-4 col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="#">
              <img alt="Pic" src="../img/project-single-6.jpg">
              <div class="title">
                <h5 class="uppercase mb0">Туман</h5>
                <span>Люди</span>
              </div>
            </a>
          </div>
        </div>
      </div>

    </div>
  </section>
</section>

<section id="pm-image-4" class="vim" vbp="текст+изобр" vbr="Список продуктов">
  <section class="projects toggle-bg-mrv">
    <div class="container">
      <div class="masonry-loader">
        <div class="col-sm-12 text-center">
          <div class="spinner"></div>
        </div>
      </div>
      <div class="row masonry masonryFlyIn">
        <div class="col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center product_height" vht="parent">
            <span class="label">Акция</span>
            <img alt="image" class="background-image" src="../img/home17.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Продукт 1</h3>
                <h6 class="uppercase">Возможное описание или подзаголовок</h6>
              </a>
            </div>
          </div>
          <br>
          <div class="description product-single mb32 mb-xs-24">
            <span class="number price old-price">5000 &#x20b8;</span>
            <span class="number price">3000 &#x20b8;</span>
            <a type="button" class="btn btn-filled" href="#">Подробнее</a>
          </div>
        </div>
        <div class="col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center product_height" vht="parent">
            <span class="label">Акция</span>
            <img alt="image" class="background-image" src="../img/home20.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Продукт 2</h3>
                <h6 class="uppercase">Возможное описание или подзаголовок</h6>
              </a>
            </div>
          </div>
          <br>
          <div class="description product-single mb32 mb-xs-24">
            <span class="number price old-price">5000 &#x20b8;</span>
            <span class="number price">3000 &#x20b8;</span>
            <a type="button" class="btn btn-filled" href="#">Подробнее</a>
          </div>
        </div>
        <div class="col-sm-6 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center product_height" vht="parent">
            <span class="label">Акция</span>
            <img alt="image" class="background-image" src="../img/home14.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Продукт 3</h3>
                <h6 class="uppercase">Возможное описание или подзаголовок</h6>
              </a>
            </div>
          </div>
          <br>
          <div class="description product-single mb32 mb-xs-24">
            <span class="number price old-price">5000 &#x20b8;</span>
            <span class="number price">3000 &#x20b8;</span>
            <a type="button" class="btn btn-filled" href="#">Подробнее</a>
          </div>
        </div>
        <div class="col-sm-6 masonry-item project product_height" data-filter="Люди">
          <div class="image-tile hover-tile text-center product_height" vht="parent">
            <span class="label">Акция</span>
            <img alt="image" class="background-image" src="../img/home18.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h3 class="uppercase mb8">Продукт 4</h3>
                <h6 class="uppercase">Возможное описание или подзаголовок</h6>
              </a>
            </div>
          </div>
          <br>
          <div class="description product-single mb32 mb-xs-24">
            <span class="number price old-price">5000 &#x20b8;</span>
            <span class="number price">3000 &#x20b8;</span>
            <a type="button" class="btn btn-filled" href="#">Подробнее</a>
          </div>
        </div>
      </div>

    </div>
  </section>
</section>

<section id="pm-image-5" class="vim" vbp="Магазин" vbr="Список продуктов со скидкой">
  <section class="projects toggle-bg-mrv">
    <div class="container">
      <div class="col-md-12 text-center mb32">
        <h2>Скидки</h2>
      </div>
    </div>
    <div class="container">
      <div class="masonry-loader">
        <div class="col-sm-12 text-center">
          <div class="spinner"></div>
        </div>
      </div>

      <div class="row masonry">
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
              <span class="label">-15%</span>
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Китая</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
              <span class="label">-15%</span>
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Италии</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
              <span class="label">-15%</span>
            </a>
            <span class="label">-15%</span>
            <div class="title">
              <h5 class="mb0">Подушка из Греции</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
              <span class="label">-15%</span>
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Египта</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
              <span class="label">-15%</span>
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из ОАЭ</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
              <span class="label">-15%</span>
            </a>
            <div class="title">
              <h5 class="mb0">Исландии</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
      </div>

    </div>
  </section>
</section>

<section id="pm-image-6" class="vim" vbp="Магазин" vbr="Список продуктов">
  <section class="projects toggle-bg-mrv">
    <div class="container">
      <div class="col-md-12 text-center mb32">
        <h2>Популярные товары</h2>
      </div>
    </div>
    <div class="container">
      <div class="masonry-loader">
        <div class="col-sm-12 text-center">
          <div class="spinner"></div>
        </div>
      </div>

      <div class="row masonry">
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Китая</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Италии</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Греции</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Египта</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из ОАЭ</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
        <div class="col-md-4 col-sm-4 masonry-item col-xs-12">
          <div class="image-tile outer-title text-center">
            <a href="#">
              <img alt="Pic" class="product-thumb" src="../img/home17.jpg" />
            </a>
            <div class="title">
              <h5 class="mb0">Подушка из Исландии</h5>
              <span class="display-block mb16">3000 &#x20b8;</span>
            </div>
          </div>
        </div>
        <!--end three col-->
      </div>

    </div>
  </section>
</section>

<section id="pm-image-7" class="vim" vbp="Магазин" vbr="Категории">
  <section class="projects toggle-bg-mrv">
    <h2 class="text-center">Категории</h2>
    <hr>
    <div class="container">
      <div class="masonry-loader">
        <div class="col-sm-12 text-center">
          <div class="spinner"></div>
        </div>
      </div>
      <div class="row masonry masonryFlyIn">
        <div class="col-sm-4 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home17.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h4 class="uppercase mb8">Категория</h4>
                <h6 class="uppercase">Дополнительный текст</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-4 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home20.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h4 class="uppercase mb8">Категория</h4>
                <h6 class="uppercase">Дополнительный текст</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-4 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home14.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h4 class="uppercase mb8">Категория</h4>
                <h6 class="uppercase">Дополнительный текст</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-4 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home18.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h4 class="uppercase mb8">Категория</h4>
                <h6 class="uppercase">Дополнительный текст</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-4 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home18.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h4 class="uppercase mb8">Категория</h4>
                <h6 class="uppercase">Дополнительный текст</h6>
              </a>
            </div>
          </div>

        </div>
        <div class="col-sm-4 masonry-item project" data-filter="Люди">
          <div class="image-tile hover-tile text-center" vht="parent">
            <img alt="image" class="background-image" src="../img/home18.jpg" vht=".project">
            <div class="hover-state">
              <a href="#">
                <h4 class="uppercase mb8">Категория</h4>
                <h6 class="uppercase">Дополнительный текст</h6>
              </a>
            </div>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>
<section id="pm-product-1" class="vim" vbp="Магазин" vbr="Страница продукта">
  <section class="projects toggle-bg-mrv">
    <div class="custom_alert">
			Товар добавлен в корзину
		</div>
    <div class="container">
      <div class="product-single">
        <div class="row mb24 mb-xs-48">
          <div class="col-md-5 col-sm-6">
            <div class="image-slider slider-thumb-controls controls-inside">
              <span class="label">Акции</span>
              <ul class="slides">
                <li>
                  <img alt="Image" src="../img/shop-product-4.jpg">
                </li>
                <li>
                  <img alt="Image" src="../img/shop-product-4_1.jpg">
                </li>
                <li>
                  <img alt="Image" src="../img/shop-product-4_2.jpg">
                </li>
              </ul>
            </div>

          </div>
          <div class="col-md-5 col-md-offset-1 col-sm-6">
            <div class="description">
              <h4 class="uppercase product-name">Сумка</h4>
              <div class="mb32 mb-xs-24">
                <span class="number price old-price">$149.95</span>
                <span class="number price product-price">$119.95</span>
              </div>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
              </p>
              <ul class="product-details">
                <li>
                  <strong>SKU:</strong> 8660</li>
                <li>
                  <strong>Цвета:</strong> Черная, Синяя</li>
                <li>
                  <strong>Размеры:</strong> XS,S,M,L,XL</li>
              </ul>
            </div>
            <hr class="mb48 mb-xs-24">
            <form class="add-to-cart" action="util.php" method="POST">
              <input type="text" placeholder="Количество" name="qty">
              <input type="submit" value="Добавить в корзину">
            </form>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="tabbed-content text-tabs">
              <ul class="tabs">
                <li class="active">
                  <div class="tab-title">
                    <span>Описание</span>
                  </div>
                  <div class="tab-content">
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.
                    </p>
                  </div>
                </li>
                <li>
                  <div class="tab-title">
                    <span>Гид по размеру</span>
                  </div>
                  <div class="tab-content">
                    <table class="table table-striped table-bordered">
                      <thead>
                      <tr>
                        <th>Размер</th>
                        <th>Значение</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <th scope="row">Маленький</th>
                        <td>
                          <span class="number">16"</span> Ворот /
                          <span class="number">8"</span> Рукав</td>
                      </tr>
                      <tr>
                        <th scope="row">Средний</th>
                        <td>
                          <span class="number">18"</span> Ворот /
                          <span class="number">9"</span> Рукав</td>
                      </tr>
                      <tr>
                        <th scope="row">Большой</th>
                        <td>
                          <span class="number">20"</span> Ворот /
                          <span class="number">10"</span> Рукав</td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </li>
                <li>
                  <div class="tab-title">
                    <span>Оценки</span>
                  </div>
                  <div class="tab-content">
                    <ul class="ratings">
                      <li>
                        <div class="user">
                          <ul class="list-inline star-rating">
                            <li>
                              <i class="ti-star"></i>
                            </li>
                            <li>
                              <i class="ti-star"></i>
                            </li>
                            <li>
                              <i class="ti-star"></i>
                            </li>
                            <li>
                              <i class="ti-star"></i>
                            </li>
                          </ul>
                          <span class="bold-h6">Джон Доу</span>
                          <span class="date number">23/02/2015</span>
                        </div>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                      </li>
                      <li>
                        <div class="user">
                          <ul class="list-inline star-rating">
                            <li>
                              <i class="ti-star"></i>
                            </li>
                            <li>
                              <i class="ti-star"></i>
                            </li>
                            <li>
                              <i class="ti-star"></i>
                            </li>
                            <li>
                              <i class="ti-star"></i>
                            </li>
                            <li>
                              <i class="ti-star"></i>
                            </li>
                          </ul>
                          <span class="bold-h6">Джон Доу</span>
                          <span class="date number">15/02/2015</span>
                        </div>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        </p>
                      </li>
                    </ul>

                    <h6 class="uppercase">Поставьте оценку</h6>
                    <form class="ratings-form">
                      <div class="overflow-hidden">
                        <input type="text" placeholder="Your Name">
                        <input type="text" placeholder="Email Address">
                      </div>
                      <div class="select-option">
                        <i class="ti-angle-down"></i>
                        <select>
                          <option selected="selected" value="Default">Select A Rating</option>
                          <option value="1">1 Star</option>
                          <option value="2">2 Stars</option>
                          <option value="3">3 Stars</option>
                          <option value="4">4 Stars</option>
                          <option value="5">5 Stars</option>
                        </select>
                      </div>
                      <textarea placeholder="Comment" rows="3"></textarea>
                      <input type="submit" value="Leave Comment">
                    </form>
                  </div>
                </li>
              </ul>
            </div>

          </div>
        </div>

      </div>

    </div>

  </section>
</section>
<section id="pm-cart-1" class="vim" vbp="Магазин" vbr="Корзина">
  <section class="projects toggle-bg-mrv">
    <div class="modal fade" id="order_finish_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Оформление заказа</h4>
          </div>
          <div class="modal-body">
            <p>Ваш заказ успешно оформлен! За два дня до доставки, мы вам сообщим по почте или по телефону</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-to-main" data-dismiss="modal">На главную</button>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-md-offset-0 col-sm-10 col-sm-offset-1">
          <table class="table cart mb48">
            <thead>
            <tr>
              <th>&nbsp;</th>
              <th>Название продукта</th>
              <th>Количество</th>
              <th>Цена</th>
            </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
        <!--end of items-->
        <div class="col-md-3 col-md-offset-0 col-sm-10 col-sm-offset-1">
          <div class="mb24 amount">
            <h5 class="uppercase ">Итоговая сумма</h5>
            <table class="table">
              <tbody>
              <tr>
                <th scope="row">Сумма товаров</th>
                <td class="total-price"></td>
              </tr>
              <tr>
                <th scope="row">НДС или доставка</th>
                <td class="nds">0</td>
              </tr>
              <tr>
                <th scope="row">Сумма заказа</th>
                <td>
                  <strong class="total-order">0</strong>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!--end of totals-->
      </div>
      <!--end of row-->
    </div>
    <!--end of container-->
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="feature bordered">
            <h3 class="mb64 mb-xs-24">Оформить заказ</h3>
            <form class="form_order" action="util.php" method="post">
              <input type="text" name="cname" placeholder="Ваше имя *" required>
              <input type="text" name="cemail" placeholder="Электронная почта *" required>
              <input type="text" name="cphone" placeholder="Телефон">
              <input type="text" name="city" placeholder="Город *" required>
              <input type="text" name="address" placeholder="Адрес *" required>
              <button type="submit">Продолжить</button>
            </form>
            <span>Поля, отмеченные звездочкой(*), обязательны</span>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="map-holder pt160 pb160 mt80 mt-xs-0 mb24">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2907.051485208183!2d76.94960796643439!3d43.22938018956905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836f1c97907697%3A0x45ca12c9fb96dcda!2z0JHQpiAi0KHTmdGC0YLRliIsINCh0LDQvNCw0LstMiA1OCwgQWxtYXR5IDA1MDA1MSwgS2F6YWtoc3Rhbg!5e0!3m2!1sen!2sus!4v1482484903552">
            </iframe>
          </div>
          <div class="feature bordered">
            <h4 class="uppercase">Информация для самовывоза</h4>
            <p>
              Проспект Абая, 144
              <br> Алматы, 050000
            </p>
            <p>
              <strong>P:</strong> +9 (999) 999-99-99
              <br>
              <strong>E:</strong> info@magazin.kz
            </p>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>
