<section id="pm-title-1" class="vim" vbp="заголовок стр" vbr="Выровненный слева заголовок">
  <section class="page-title page-title-4 bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3 class="uppercase mb0">Краткий Заголовок</h3>
        </div>
        <div class="col-md-6 text-right">
          <ol class="breadcrumb breadcrumb-2">
            <li>
              <a vht="parent" href="#">Главная</a>
            </li>
            <li>
              <a vht="parent" href="#">Элементы</a>
            </li>
            <li class="active">Заголовки Страниц</li>
          </ol>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-title-2" class="vim" vbp="заголовок стр" vbr="Выровненный слева заголовок с изображением">
  <section class="page-title page-title-4 image-bg overlay parallax">
    <div class="background-image-holder">
      <img alt="Background Image" class="background-image" src="../img/cover14.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3 class="uppercase mb0">Краткий Заголовок</h3>
        </div>
        <div class="col-md-6 text-right">
          <ol class="breadcrumb breadcrumb-2">
            <li>
              <a vht="parent" href="#">Главная</a>
            </li>
            <li>
              <a vht="parent" href="#">Элементы</a>
            </li>
            <li class="active">Заголовки Страниц</li>
          </ol>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-title-3" class="vim" vbp="заголовок стр" vbr="Выровненный слева крупный">
  <section class="page-title page-title-2 bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2 class="uppercase mb8">Крупный левый</h2>
          <p class="lead mb0">Подзаголовок с подробным описанием.</p>
        </div>
        <div class="col-md-6 text-right">
          <ol class="breadcrumb breadcrumb-2">
            <li>
              <a vht="parent" href="#">Главная</a>
            </li>
            <li>
              <a vht="parent" href="#">Элементы</a>
            </li>
            <li class="active">Заголовки Страниц</li>
          </ol>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-title-4" class="vim" vbp="заголовок стр" vbr="Выровненный слева заголовок с крупным изображением">
  <section class="page-title page-title-2 image-bg overlay parallax">
    <div class="background-image-holder">
      <img alt="Background Image" class="background-image" src="../img/cover15.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2 class="uppercase mb8">Крупное изображение</h2>
          <p class="lead mb0">Подзаголовок с подробным описанием.</p>
        </div>
        <div class="col-md-6 text-right">
          <ol class="breadcrumb breadcrumb-2">
            <li>
              <a vht="parent" href="#">Главная</a>
            </li>
            <li>
              <a vht="parent" href="#">Элементы</a>
            </li>
            <li class="active">Заголовки Страниц</li>
          </ol>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-title-5" class="vim" vbp="заголовок стр" vbr="Центрированный заголовок">
  <section class="page-title page-title-3 bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h3 class="uppercase mb0">Небольшой центр</h3>
        </div>
      </div>

    </div>

    <ol class="breadcrumb breadcrumb-2">
      <li>
        <a vht="parent" href="#">Главная</a>
      </li>
      <li>
        <a vht="parent" href="#">Элементы</a>
      </li>
      <li class="active">Заголовки Страниц</li>
    </ol>
  </section>
</section>

<section id="pm-title-6" class="vim" vbp="заголовок стр" vbr="Центрированный заголовок с изображением">
  <section class="page-title page-title-3 image-bg overlay parallax">
    <div class="background-image-holder">
      <img alt="Background Image" class="background-image" src="../img/cover15.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h3 class="uppercase mb0">Небольшое изображение</h3>
        </div>
      </div>

    </div>

    <ol class="breadcrumb breadcrumb-2">
      <li>
        <a vht="parent" href="#">Главная</a>
      </li>
      <li>
        <a vht="parent" href="#">Элементы</a>
      </li>
      <li class="active">Заголовки Страниц</li>
    </ol>
  </section>
</section>

<section id="pm-title-7" class="vim" vbp="заголовок стр" vbr="Центрированный заголовок крупный">
  <section class="page-title page-title-1 bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2 class="uppercase mb0">Крупный Заголовок</h2>
        </div>
      </div>

    </div>

    <ol class="breadcrumb breadcrumb-2">
      <li>
        <a vht="parent" href="#">Главная</a>
      </li>
      <li>
        <a vht="parent" href="#">Элементы</a>
      </li>
      <li class="active">Заголовки Страниц</li>
    </ol>
  </section>
</section>

<section id="pm-title-8" class="vim" vbp="заголовок стр" vbr="Центрированный заголовок с крупным изображением">
  <section class="page-title page-title-1 image-bg overlay parallax">
    <div class="background-image-holder">
      <img alt="Background Image" class="background-image" src="../img/cover14.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2 class="uppercase mb0">Крупный Заголовок</h2>
        </div>
      </div>

    </div>

    <ol class="breadcrumb breadcrumb-2">
      <li>
        <a vht="parent" href="#">Главная</a>
      </li>
      <li>
        <a vht="parent" href="#">Элементы</a>
      </li>
      <li class="active">Заголовки Страниц</li>
    </ol>
  </section>
</section>