<section id="pm-slider-1" class="vim" vbp="слайдер" vbr="Полноэкранный слайдер">
    <section class="cover fullscreen image-slider slider-all-controls controls-inside parallax">
        <ul class="slides">
            <li class="overlay image-bg">
                <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/home22.jpg">
                </div>
                <div class="container v-align-transform">
                    <div class="row">
                        <div class="col-md-6 col-sm-8">
                            <h1 class="mb40 mb-xs-16 large">Ваш модульный инструмент.</h1>
                            <h6 class="uppercase mb16">Решение на базе блоков</h6>
                            <p class="lead mb40">
                                Строй изящные сайты всего за минуты
                            </p>
                            <a class="btn btn-lg" href="#">Подробнее</a>
                        </div>
                    </div>

                </div>

            </li>
        </ul>
    </section>
</section>

<section id="pm-slider-14" class="vim" vbp="слайдер" vbr="Слайдер продукта с текстом">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-push-3 text-center">
          <div class="image-slider slider-paging-controls controls-outside">
            <ul class="slides">
              <li class="mb32">
                <img alt="App" src="../img/app5.png">
              </li>
              <li class="mb32">
                <img alt="App" src="../img/app5.png">
              </li>
              <li class="mb32">
                <img alt="App" src="../img/app5.png">
              </li>
            </ul>
          </div>
        </div>

        <div class="col-md-3 col-md-pull-6">
          <div class="mt80 mt-xs-80 text-right text-left-xs">
            <h5 class="uppercase bold mb16">Не забывай снова</h5>
            <p class="fade-1-4">
              Напоминалка, которая просто напоминает! Больше никаких навязчивых уведомлений.
            </p>
          </div>

          <div class="mt80 mt-xs-0 text-right text-left-xs">
            <h5 class="uppercase bold mb16">Оставайся активным, будь здоровым.</h5>
            <p class="fade-1-4">
              Уведмоления, чтобы просто встать с места и двигаться.
            </p>
          </div>
        </div>

        <div class="col-md-3">
          <div class="mt80 mt-xs-0">
            <h5 class="uppercase bold mb16">Вдохновлен тобой</h5>
            <p class="fade-1-4">
              Забудь про пустые обещания, эта штука вдохновлена тобой.
            </p>
          </div>

          <div class="mt80 mt-xs-0">
            <h5 class="uppercase bold mb16">Оставь свой номер</h5>
            <p class="fade-1-4">

              Будь в курсе самых последних обновлений и новостей!
            </p>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<!--<section id="pm-slider-15" class="vim" vbp="слайдер,видео" vbr="Видео слайдер">-->
<!--  <section class="cover fullscreen image-slider slider-all-controls">-->
<!--    <ul class="slides">-->
<!--      <li class="vid-bg image-bg overlay">-->
<!--        <div class="background-image-holder">-->
<!--          <img alt="Background Image" class="background-image" src="https://unsplash.imgix.net/photo-1425321395722-b1dd54a97cf3?q=75&fm=jpg&w=1080&fit=max&s=9e4ce3e023621d6f94259eea8fa3b856">-->
<!--        </div>-->
<!--        <div class="fs-vid-background">-->
<!--          <video muted loop>-->
<!--            <source src="../video/video.webm" type="video/webm">-->
<!--            </source><source src="../video/video.mp4" type="video/mp4">-->
<!--            </source><source src="../video/video.ogv" type="video/ogg">-->
<!--            </source></video>-->
<!--        </div>-->
<!--        <div class="container v-align-transform">-->
<!--          <div class="row">-->
<!--            <div class="col-sm-12 text-center">-->
<!--              <h1 class="large">Гибкий слайдер + Фоновые Видео</h1>-->
<!--              <p class="lead">-->
<!--                Простая и умная функциональность автопроигрывания и приостановки.-->
<!--              </p>-->
<!--            </div>-->
<!--          </div>-->
<!---->
<!--        </div>-->
<!---->
<!--      </li>-->
<!--      <li class="image-bg overlay">-->
<!--        <div class="background-image-holder">-->
<!--          <img alt="Background Image" class="background-image" src="../img/home5.jpg">-->
<!--        </div>-->
<!--        <div class="container v-align-transform">-->
<!--          <div class="row">-->
<!--            <div class="col-sm-12 text-center">-->
<!--              <h1 class="large">Гибкий слайдер + Фоновые Видео</h1>-->
<!--              <p class="lead">-->
<!--                Простая и умная функциональность автопроигрывания и приостановки.-->
<!--              </p>-->
<!--            </div>-->
<!--          </div>-->
<!---->
<!--        </div>-->
<!---->
<!--      </li>-->
<!--      <li class="vid-bg image-bg overlay">-->
<!--        <div class="background-image-holder">-->
<!--          <img alt="Background Image" class="background-image" src="https://unsplash.imgix.net/photo-1425321395722-b1dd54a97cf3?q=75&fm=jpg&w=1080&fit=max&s=9e4ce3e023621d6f94259eea8fa3b856">-->
<!--        </div>-->
<!--        <div class="fs-vid-background">-->
<!--          <video muted loop>-->
<!--            <source src="../video/video2.webm" type="video/webm">-->
<!--            </source><source src="../video/video2.mp4" type="video/mp4">-->
<!--            </source><source src="../video/video2.ogv" type="video/ogg">-->
<!--            </source></video>-->
<!--        </div>-->
<!--        <div class="container v-align-transform">-->
<!--          <div class="row">-->
<!--            <div class="col-sm-12 text-center">-->
<!--              <h1 class="large">Гибкий слайдер + Фоновые Видео</h1>-->
<!--              <p class="lead">-->
<!--                Простая и умная функциональность автопроигрывания и приостановки.-->
<!--              </p>-->
<!--            </div>-->
<!--          </div>-->
<!---->
<!--        </div>-->
<!---->
<!--      </li>-->
<!--      <li class="image-bg overlay">-->
<!--        <div class="background-image-holder">-->
<!--          <img alt="Background Image" class="background-image" src="../img/home4.jpg">-->
<!--        </div>-->
<!--        <div class="container v-align-transform">-->
<!--          <div class="row">-->
<!--            <div class="col-sm-12 text-center">-->
<!--              <h1 class="large">Гибкий слайдер + Фоновые Видео</h1>-->
<!--              <p class="lead">-->
<!--                Простая и умная функциональность автопроигрывания и приостановки.-->
<!--              </p>-->
<!--            </div>-->
<!--          </div>-->
<!---->
<!--        </div>-->
<!---->
<!--      </li>-->
<!--    </ul>-->
<!--  </section>-->
<!--</section>-->

<section id="pm-gallery-1" class="vim" vbp="галерея,слайдер" vbr="Простой слайдер">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h4 class="uppercase mb16">Стандартный слайдер</h4>
          <p class="lead mb64">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque iusto pariatur magni!
            Molestias aliquam reiciendis consequuntur officiis facilis fugiat pariatur cupiditate expedita iure possimus reprehenderit
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="image-slider slider-all-controls controls-inside">
            <ul class="slides">
              <li>
                <img alt="Image" src="../img/cover14.jpg">
              </li>
              <li>
                <img alt="Image" src="../img/cover15.jpg">
              </li>
              <li>
                <img alt="Image" src="../img/cover16.jpg">
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-logos-1" class="vim" vbp="изображение,слайдер" vbr="Слайдер с изображением">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h4 class="uppercase mb64 mb-xs-40">Партнеры</h4>
        </div>
      </div>

      <div class="row">
        <div class="logo-carousel">
          <ul class="slides">
            <li>
              <a href="#" vht="parent">
                <img alt="Logo" src="../img/logo-dark.png" vht="li">
              </a>
            </li>
            <li>
              <a href="#" vht="parent">
                <img alt="Logo" src="../img/logo-dark.png" vht="li">
              </a>
            </li>
            <li>
              <a href="#" vht="parent">
                <img alt="Logo" src="../img/logo-dark.png" vht="li">
              </a>
            </li>
            <li>
              <a href="#" vht="parent">
                <img alt="Logo" src="../img/logo-dark.png" vht="li">
              </a>
            </li>
            <li>
              <a href="#" vht="parent">
                <img alt="Logo" src="../img/logo-dark.png" vht="li">
              </a>
            </li>
            <li>
              <a href="#" vht="parent">
                <img alt="Logo" src="../img/logo-dark.png" vht="li">
              </a>
            </li>
          </ul>
        </div>

      </div>

    </div>

  </section>
</section>
