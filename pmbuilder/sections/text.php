<section id="pm-text-1" class="vim" vbp="текст" vbr="Текст в колонках">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <h2 class="number">2008</h2>
          <p>
            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.
          </p>
        </div>
        <div class="col-sm-4">
          <h2 class="number">150+</h2>
          <p>
            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.
          </p>
        </div>
        <div class="col-sm-4">
          <h2>Миллионы</h2>
          <p>
            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-text-2" class="vim" vbp="текст" vbr="Текст в колонках с изображением">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="image-tile outer-title text-center" vht="parent">
            <img alt="Pic" src="../img/team-1.jpg">
            <div class="title mb16">
              <h5 class="uppercase mb0">Джон Доу</h5>
              <span>Креативный Директор</span>
            </div>
            <p class="mb0">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
            </p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="image-tile outer-title text-center" vht="parent">
            <img alt="Pic" src="../img/team-2.jpg">
            <div class="title mb16">
              <h5 class="uppercase mb0">Джон Доу</h5>
              <span>iOS разработчик</span>
            </div>
            <p class="mb0">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
            </p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="image-tile outer-title text-center" vht="parent">
            <img alt="Pic" src="../img/team-3.jpg">
            <div class="title mb16">
              <h5 class="uppercase mb0">Джон Доу</h5>
              <span>Директор бренда</span>
            </div>
            <p class="mb0">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis.
            </p>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-text-3" class="vim" vbp="текст" vbr="Текст со статистикой">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row v-align-children">
        <div class="col-sm-6 col-md-5">
          <h2 class="uppercase color-primary">
            Lorem ipsum dolor sit amet
          </h2>
          <hr>
          <p>
            Adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
        </div>
        <div class="col-sm-6 col-md-offset-1 p0">
          <div class="col-sm-6 text-center">
            <div class="feature bordered mb30">
              <h1 class="large uppercase">5</h1>
              <h5 class="uppercase">спален</h5>
            </div>
          </div>

          <div class="col-sm-6 text-center">
            <div class="feature bordered mb30">
              <h1 class="large">2</h1>
              <h5 class="uppercase">жилых</h5>
            </div>
          </div>

          <div class="col-sm-6 text-center">
            <div class="feature bordered mb30">
              <h1 class="large">4</h1>
              <h5 class="uppercase">ванных</h5>
            </div>
          </div>

          <div class="col-sm-6 text-center">
            <div class="feature bordered mb30">
              <h1 class="large">3</h1>
              <h5 class="uppercase">парковок</h5>
            </div>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-text-4" class="vim" vbp="текст" vbr="Текст с прогрессом">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="uppercase">Разнообразность & Разность</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.
          </p>
        </div>
        <div class="col-sm-7">
          <h4 class="uppercase">Расширенные Услуги</h4>
          <div class="progress-bars">
            <div class="progress progress-1">
              <div class="progress-bar" data-progress="90" vht="parent">
                <span class="title">Брендинг</span>
              </div>
            </div>

            <div class="progress progress-1">
              <div class="progress-bar" data-progress="70" vht="parent">
                <span class="title">Электронная Коммерция</span>
              </div>
            </div>

            <div class="progress progress-1">
              <div class="progress-bar" data-progress="60" vht="parent">
                <span class="title">Веб сайты</span>
              </div>
            </div>

            <div class="progress progress-1">
              <div class="progress-bar" data-progress="50" vht="parent">
                <span class="title">iOS приложения</span>
              </div>
            </div>

          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-text-5" class="vim" vbp="текст" vbr="Простая статья">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="feed-item mb96 mb-xs-48">
        <div class="row mb16 mb-xs-0">
          <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
            <h6 class="uppercase mb16 mb-xs-8"> 8 сентября</h6>
            <h3>
              Lorem ipsum dolor sit amet</h3>
          </div>
        </div>

        <div class="row mb32 mb-xs-16">
          <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <img alt="Article Image" class="mb32 mb-xs-16" src="../img/blog-single.jpg">
            <p class="lead">
              Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.
            </p>
            <p class="lead">
              Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
            </p>
          </div>
        </div>

        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
            <hr>
          </div>
        </div>
      </div>

    </div>
  </section>
</section>

<section id="pm-text-6" class="vim" vbp="текст" vbr="Текст со статистикой 2">
  <section class="bg-dark toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h3 class="uppercase">
            Создание великолепных
            <br> пространств
          </h3>
        </div>
      </div>

      <div class="row mb80 mb-xs-24">
        <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 text-center">
          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-5 col-sm-offset-1 text-center">
          <div class="feature boxed">
            <h1 class="large">64</h1>
            <h5 class="uppercase">
              Lorem ipsum dolor sit amet
            </h5>
          </div>
        </div>
        <div class="col-sm-5 text-center">
          <div class="feature boxed">
            <h1 class="large">#3</h1>
            <h5 class="uppercase">
              Lorem ipsum dolor sit amet
            </h5>
          </div>
        </div>
      </div>
    </div>

  </section>
</section>

<section id="pm-text-7" class="vim" vbp="текст" vbr="Широкий текст">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1 text-center">
          <h3 class="uppercase color-primary mb40 mb-xs-24">
            Lorem ipsum dolor sit amet
          </h3>
          <p class="lead">
            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-text-8" class="vim" vbp="текст" vbr="Текст со статистикой 3">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6 class="uppercase">О Шаблонизаторе</h6>
          <hr class="mb160 mb-xs-24">
        </div>
      </div>

      <div class="row">
        <div class="col-md-10">
          <h1 class="thin">
            Lorem ipsum dolor sit amet
          </h1>
        </div>
      </div>

      <div class="row mb160 mb-xs-0">
        <div class="col-md-6 col-sm-8">
          <p class="lead">
            Lorem ipsum dolor sit amet
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4 mb-xs-24">
          <h1 class="large color-primary mb0">140+</h1>
          <h5 class="color-primary mb0">
            Lorem ipsum dolor sit amet
          </h5>
        </div>
        <div class="col-sm-4 mb-xs-24">
          <h1 class="large color-primary mb0">$1.2b+</h1>
          <h5 class="color-primary mb0">
            Lorem ipsum dolor sit amet
          </h5>
        </div>
        <div class="col-sm-4">
          <h1 class="large color-primary mb0">4/5</h1>
          <h5 class="color-primary mb0">
            Lorem ipsum dolor sit amet
          </h5>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-text-10" class="vim" vbp="текст" vbr="Блок Текста">
  <section class="bg-primary">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-1 text-right text-left-xs col-sm-5">
          <h1 class="uppercase mb24 bold italic">Три клика<br>пару слов</h1>
          <h5 class="uppercase italic fade-1-4">&ldquo;
            Lorem ipsum dolor sit amet
            &rdquo;</h5>
          <hr class="visible-xs">
        </div>
        <div class="col-md-5 col-sm-7">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </p>

          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
          </p>

          <p>
            Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur
          </p>
          <img alt="signature" src="../img/fitness1.png" class="image-small">
        </div>
      </div>
    </div>
  </section>
</section>

<section id="pm-blog-1" class="vim" vbp="текст,блог" vbr="Статья">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <div class="post-snippet mb64">
            <img class="mb24" alt="Post Image" src="../img/blog-single.jpg">
            <div class="post-title">
              <span class="label">23 сентября</span>
              <h4 class="inline-block">
                Lorem ipsum dolor sit amet
              </h4>
            </div>
            <ul class="post-meta">
              <li>
                <i class="ti-user"></i>
                <span>Джон Доу</a>
								</span>
              </li>
              <li>
                <i class="ti-tag"></i>
                <span>жизнь</a>
								</span>
              </li>
            </ul>
            <hr>
            <p class="lead">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>
            <p>
              At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
            </p>
            <blockquote>
              Lorem ipsum dolor sit amet
            </blockquote>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
            </p>
            <ul class="bullets">
              <li>Beatae vitae dicta sunt explicabo</li>
              <li>Inventore veritatis et quasi architecto</li>
              <li>Sed do eiusmod tempor incididunt</li>
              <li>Accusamus et iusto odio dignissimos ducimus</li>
            </ul>
          </div>

          <hr>
        </div>

      </div>

    </div>

  </section>
</section>
