<section id="pm-slider-7" class="vim" vbp="заголовок,полный экран" vbr="Зеленый фон">
  <section class="fullscreen image-bg parallax background-multiply">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/capital5.jpg">
    </div>
    <div class="container v-align-transform">
      <div class="row">
        <div class="col-sm-12">
          <h1 class="thin mb0">
            Базирующийся на Астане фирма венчурного капитала.
          </h1>
        </div>
      </div>

    </div>

    <div class="align-bottom text-center">
      <a class="btn btn-white mb32" href="#">Подробнее</a>
      <ul class="list-inline social-list mb24">
        <li>
          <a href="#">
            <i vht="li" class="ti-twitter-alt"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i vht="li" class="ti-facebook"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i vht="li" class="ti-dribbble"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i vht="li" class="ti-vimeo-alt"></i>
          </a>
        </li>
      </ul>
    </div>
  </section>
</section>

<section id="pm-slider-10" class="vim" vbp="заголовок,полный экран" vbr="Кнопка снизу">
  <section class="cover fullscreen image-bg overlay parallax">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/prop1.jpg">
    </div>
    <div class="container v-align-transform">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h1 class="large uppercase mb16">Проспект Абая, 43.</h1>
          <h5 class="uppercase mb0">Современная жизнь в центре Алматы</h5>
        </div>
      </div>

    </div>

    <div class="align-bottom relative-xs text-center">
      <a class="btn btn-filled btn-lg mb32 mt-xs-40" href="#">Подробнее</a>
    </div>
  </section>
</section>

<section id="pm-banner-1" class="vim" vbp="заголовок" vbr="Ссылка App Store">
  <section class="image-bg parallax pt240 pb180 pt-xs-80 pb-xs-80">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/app8.jpg">
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-12">
          <h1>Сделано дизайнерами<br> специально для тебя.</h1>
          <p class="lead mb48 mb-xs-32">
            Простой, стильный способ показать свой продукт
          </p>
          <a href="#">
            <img class="image-xs" alt="App Store" src="../img/appstore.png">
          </a>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-banner-2" class="vim" vbp="заголовок" vbr="Личный">
  <section class="image-bg overlay pt240 pb240 pt-xs-180 pb-xs-180">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/fitness3.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h1 class="mb0 uppercase bold italic">Джон Доу</h1>
          <h5 class="uppercase mb32">Личная тренировка & спортивная диета</h5>
          <p class="lead mb0">
            &ldquo;Каждый заслуживает отличное здоровье и счастье.<br class="hidden-sm">Моя цель - помочь людям достич оба.&rdquo;
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-banner-3" class="vim" vbp="заголовок" vbr="Образ">
  <section class="image-slider slider-all-controls parallax controls-inside pt0 pb0">
    <ul class="slides">
      <li class="overlay image-bg pt240 pb240 pt-xs-120 pb-xs-120">
        <div class="background-image-holder">
          <img alt="image" class="background-image" src="../img/fashion1.jpg">
        </div>
        <div class="container">
          <div class="row text-center">
            <div class="col-md-12">
              <h2 class="mb40 uppercase">Все Для Тебя.</h2>
              <a class="btn btn-lg" href="#">Исследовать Коллекцию</a>
            </div>
          </div>

        </div>

      </li>
    </ul>
  </section>
</section>

<section id="pm-gallery-3" class="vim" vbp="галерея" vbr="Сеточная галерея">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h4 class="uppercase mb16">Сеточная Галерея</h4>
          <p class="lead mb64">
            Lorem ipsum dolor sit amet
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="lightbox-grid square-thumbs lightbox-gallery-mrv" data-gallery-title="Gallery">
            <ul>
              <li class="veo voh">
                <a href="../img/cover1.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/cover1.jpg">
                  </div>
                </a>
              </li>
              <li class="veo voh">
                <a href="../img/home12.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/home12.jpg">
                  </div>
                </a>
              </li>
              <li class="veo voh">
                <a href="../img/home14.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/home14.jpg">
                  </div>
                </a>
              </li>
              <li class="veo voh">
                <a href="../img/home17.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/home17.jpg">
                  </div>
                </a>
              </li>
              <li class="veo voh">
                <a href="../img/cover5.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/cover5.jpg">
                  </div>
                </a>
              </li>
              <li class="veo voh">
                <a href="../img/cover6.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/cover6.jpg">
                  </div>
                </a>
              </li>
              <li class="veo voh">
                <a href="../img/cover7.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/cover7.jpg">
                  </div>
                </a>
              </li>
              <li class="veo voh">
                <a href="../img/cover8.jpg" class="ven" data-lightbox="true" vht="parent">
                  <div class="background-image-holder">
                    <img alt="image" class="background-image" src="../img/cover8.jpg">
                  </div>
                </a>
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-text-11" class="vim" vbp="текст" vbr="Вопросы и ответы">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h4 class="mb16">Заголовок FAQ</h4>
          <p class="lead mb64">
            Lorem ipsum dolor sit amet
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <ul class="accordion accordion-2 one-open">
            <li>
              <div class="title" vht="li">
                <h4 class="inline-block mb0" vht="li">
                  Lorem ipsum dolor sit amet
                </h4>
              </div>
              <div class="content" vht="li">
                <p vht="li">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
            </li>
            <li>
              <div class="title" vht="li">
                <h4 class="inline-block mb0" vht="li">
                  Lorem ipsum dolor sit amet
                </h4>
              </div>
              <div class="content" vht="li">
                <p vht="li">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
            </li>
            <li>
              <div class="title" vht="li">
                <h4 class="inline-block mb0" vht="li">
                  Lorem ipsum dolor sit amet
                </h4>
              </div>
              <div class="content" vht="li">
                <p vht="li">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
            </li>
            <li>
              <div class="title" vht="li">
                <h4 class="inline-block mb0" vht="li">
                  Lorem ipsum dolor sit amet
                </h4>
              </div>
              <div class="content" vht="li">
                <p vht="li">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
            </li>
          </ul>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-image-4" class="vim" vbp="портфолио" vbr="Широкий проект">
  <section class="image-bg bg-dark parallax overlay pt120 pb120">
    <div class="background-image-holder">
      <img alt="Background Image" class="background-image" src="../img/project-single-3.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2 class="uppercase mb8">Заголовок проекта</h2>
          <p class="lead mb40">
            Lorem ipsum dolor sit amet
          </p>
          <a class="btn btn-lg btn-white mb0" href="#">Открыть</a>
        </div>
      </div>

    </div>

  </section>
</section>
