<section id="pm-slider-3" class="vim" vbp="заголовок,полный экран,форма" vbr="Полноэкранная форма" icons="mailchimp-cmonitor">
  <section class="cover fullscreen overlay parallax">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/home13.jpg">
    </div>
    <div class="container v-align-transform">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
          <h1 class="mb8">Шаблонизатор - ваш инструмент дизайна.</h1>
          <p class="lead mb48">
            Идеально подходит для рекламирующих новый продукт или сервис.
          </p>
          <form class="form-newsletter thirds" data-success="Спасибо! Мы скоро свяжемся с вами." data-error="Пожалуйста, заполните все поля.">
            <input type="text" name="name" class="validate-required signup-name-field" placeholder="Ваше имя">
            <input type="text" name="email" class="validate-required validate-email signup-email-field" placeholder="Адрес эл.почты">
            <button type="submit">Держать меня в курсе</button>
            <p class="sub text-center">
              При входе в систему, вы соглашаетесь с нашими
              <a href="#">Условиями Использования</a>
            </p>
            <iframe srcdoc="<!-- Begin MailChimp Signup Form --><link href=&quot;https://cdn-images.mailchimp.com/embedcode/classic-081711.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;><style type=&quot;text/css&quot;>    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }    /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */</style><div id=&quot;mc_embed_signup&quot;><form action=&quot;//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=94d040322a&quot; method=&quot;post&quot; id=&quot;mc-embedded-subscribe-form&quot; name=&quot;mc-embedded-subscribe-form&quot; class=&quot;validate&quot; target=&quot;_blank&quot; novalidate>    <div id=&quot;mc_embed_signup_scroll&quot;>    <h2>Subscribe to our mailing list</h2><div class=&quot;indicates-required&quot;><span class=&quot;asterisk&quot;>*</span> indicates required</div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-EMAIL&quot;>Email Address  <span class=&quot;asterisk&quot;>*</span></label>    <input type=&quot;email&quot; value=&quot;&quot; name=&quot;EMAIL&quot; class=&quot;required email&quot; id=&quot;mce-EMAIL&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-FNAME&quot;>First Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;FNAME&quot; class=&quot;&quot; id=&quot;mce-FNAME&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-LNAME&quot;>Last Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;LNAME&quot; class=&quot;&quot; id=&quot;mce-LNAME&quot;></div>    <div id=&quot;mce-responses&quot; class=&quot;clear&quot;>        <div class=&quot;response&quot; id=&quot;mce-error-response&quot; style=&quot;display:none&quot;></div>        <div class=&quot;response&quot; id=&quot;mce-success-response&quot; style=&quot;display:none&quot;></div>    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->    <div style=&quot;position: absolute; left: -5000px;&quot;><input type=&quot;text&quot; name=&quot;b_77142ece814d3cff52058a51f_94d040322a&quot; tabindex=&quot;-1&quot; value=&quot;&quot;></div>    <div class=&quot;clear&quot;><input type=&quot;submit&quot; value=&quot;Subscribe&quot; name=&quot;subscribe&quot; id=&quot;mc-embedded-subscribe&quot; class=&quot;button&quot;></div>    </div></form></div><script type='text/javascript' src='https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script><!--End mc_embed_signup-->" class="mail-list-form">
            </iframe>
          </form>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-contact-1" class="vim" vbp="форма" vbr="Форма контакта">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-md-5">
          <h4 class="uppercase">Связаться</h4>
          <p>
            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident,
          </p>
          <hr>
          <p>
            Проспект Абая 222
            <br> Алматы
            <br> 050000
          </p>
          <hr>
          <p>
            <strong>E:</strong> hello@kazpost.net
            <br>
            <strong>P:</strong> +00000000000
            <br>
          </p>
        </div>
        <div class="col-sm-6 col-md-5 col-md-offset-1">
          <form class="form-email" data-success="Спасибо! Мы скоро свяжемся с вами." data-error="Пожалуйста, заполните все поля.">
            <input type="text" class="validate-required" name="name" placeholder="Ваше имя">
            <input type="text" class="validate-required validate-email" name="email" placeholder="Email Address">
            <textarea class="validate-required" name="message" rows="4" placeholder="Сообщение"></textarea>
            <button type="submit">Отправить сообщение</button>
          </form>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-contact-6" class="vim" vbp="форма" vbr="Форма резерва">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="feature bordered">
            <h1 class="mb64 mb-xs-24">Забронировать</h1>
            <form class="form-email" data-success="Спасибо! Мы скоро свяжемся с вами" data-error="Пожалуйста, заполните все поля.">
              <input type="text" class="validate-required" name="name" placeholder="Забронировать на имя">
              <input type="text" class="validate-required" name="phone" placeholder="Номер телефона">
              <div class="select-option col-md-6 p0">
                <i class="ti-angle-down"></i>
                <select class="validate-required" name="persons">
                  <option selected="selected" value="">Число гостей</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
              </div>

              <div class="select-option col-md-6 p0">
                <i class="ti-angle-down"></i>
                <select class="validate-required" name="time">
                  <option selected="selected" value="">Время</option>
                  <option value="6pm">18:00pm</option>
                  <option value="630pm">18:30pm</option>
                  <option value="7pm">19:00pm</option>
                  <option value="730pm">19:30pm</option>
                  <option value="8pm">20:00pm</option>
                  <option value="830pm">20:30pm</option>
                  <option value="9pm">21:00pm</option>
                </select>
              </div>

              <input type="text" class="validate-required" name="date" placeholder="Date (DD/MM/YYYY)">
              <button type="submit">Зарезервировать Столик</button>
            </form>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="map-holder pt160 pb160 mt80 mt-xs-0 mb24">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2907.051485208183!2d76.94960796643439!3d43.22938018956905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836f1c97907697%3A0x45ca12c9fb96dcda!2z0JHQpiAi0KHTmdGC0YLRliIsINCh0LDQvNCw0LstMiA1OCwgQWxtYXR5IDA1MDA1MSwgS2F6YWtoc3Rhbg!5e0!3m2!1sen!2sus!4v1482484903552"></iframe>
          </div>
          <div class="feature bordered">
            <h4 class="uppercase">Связаться с нами</h4>
            <p>
              Проспект Абая, 144
              <br> Алматы, 050000
            </p>
            <p>
              <strong>P:</strong> +0000000000
              <br>
              <strong>E:</strong> eat@kazpost.net
            </p>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-coming-soon-1" class="vim" vbp="форма" vbr="Скоро открытие">
  <section class="cover fullscreen image-bg overlay">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/page-coming-soon.jpg">
    </div>
    <div class="container v-align-transform">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
          <div class="feature bordered text-center">
            <h3 class="uppercase">Проверьте позже</h3>
            <p>
              Lorem ipsum dolor sit amet
            </p>
            <form class="halves form-newsletter" data-success="Спасибо! Мы скоро свяжемся с вами." data-error="Пожалуйста, заполните все поля.">
              <input class="mb16 validate-required validate-email signup-email-field" type="text" placeholder="Email Address" name="email">
              <button class="mb16" type="submit">Уведомить меня</button>
              <span>*
							Lorem ipsum dolor sit amet
							</span>
              <iframe srcdoc="<!-- Begin MailChimp Signup Form --><link href=&quot;https://cdn-images.mailchimp.com/embedcode/classic-081711.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;><style type=&quot;text/css&quot;>    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }    /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */</style><div id=&quot;mc_embed_signup&quot;><form action=&quot;//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=94d040322a&quot; method=&quot;post&quot; id=&quot;mc-embedded-subscribe-form&quot; name=&quot;mc-embedded-subscribe-form&quot; class=&quot;validate&quot; target=&quot;_blank&quot; novalidate>    <div id=&quot;mc_embed_signup_scroll&quot;>    <h2>Subscribe to our mailing list</h2><div class=&quot;indicates-required&quot;><span class=&quot;asterisk&quot;>*</span> indicates required</div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-EMAIL&quot;>Email Address  <span class=&quot;asterisk&quot;>*</span></label>    <input type=&quot;email&quot; value=&quot;&quot; name=&quot;EMAIL&quot; class=&quot;required email&quot; id=&quot;mce-EMAIL&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-FNAME&quot;>First Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;FNAME&quot; class=&quot;&quot; id=&quot;mce-FNAME&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-LNAME&quot;>Last Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;LNAME&quot; class=&quot;&quot; id=&quot;mce-LNAME&quot;></div>    <div id=&quot;mce-responses&quot; class=&quot;clear&quot;>        <div class=&quot;response&quot; id=&quot;mce-error-response&quot; style=&quot;display:none&quot;></div>        <div class=&quot;response&quot; id=&quot;mce-success-response&quot; style=&quot;display:none&quot;></div>    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->    <div style=&quot;position: absolute; left: -5000px;&quot;><input type=&quot;text&quot; name=&quot;b_77142ece814d3cff52058a51f_94d040322a&quot; tabindex=&quot;-1&quot; value=&quot;&quot;></div>    <div class=&quot;clear&quot;><input type=&quot;submit&quot; value=&quot;Subscribe&quot; name=&quot;subscribe&quot; id=&quot;mc-embedded-subscribe&quot; class=&quot;button&quot;></div>    </div></form></div><script type='text/javascript' src='https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script><!--End mc_embed_signup-->" class="mail-list-form">
              </iframe>
            </form>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>