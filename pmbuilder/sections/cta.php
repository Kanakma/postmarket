<section id="pm-header-1" class="vim" vbp="заголовок,новый" vbr="Изображение с действием" icons="новое">
  <section class="bg-primary background-multiply pt240 pb240 pt-xs-120 pb-xs-120 overlay image-bg parallax">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/home24.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
          <img alt="Logo" class="image-small mb40 mb-xs-0" src="../img/logo-light.png">
          <h4 class="mb56 mb-xs-24">
            Шаблонизатор решает разные потребности: портфолио, продвижение продукта или услуги
          </h4>
          <a class="btn btn-lg btn-white mb0" href="#">Отправить запрос</a>
        </div>
      </div>
    </div>
  </section>
</section>

<section id="pm-cta-1" class="vim" vbp="действие" vbr="Вызов к действию на изображении">
  <section class="image-bg parallax overlay">
    <div class="background-image-holder">
      <img alt="Background Image" class="background-image" src="../img/home2.jpg">
    </div>
    <div class="container">
      <div class="row mb64 mb-xs-24">
        <div class="col-sm-6 col-md-5 text-right text-center-xs">
          <h1 class="large mb8">10,000+</h1>
          <h6 class="uppercase">Клиентов, пользующихся нашими продуктами</h6>
        </div>
        <div class="col-md-2 text-center hidden-sm hidden-xs">
          <i class="ti-infinite icon icon-lg mt8 mt-xs-0"></i>
        </div>
        <div class="col-sm-6 col-md-5 text-center-xs">
          <h1 class="large mb8">Безграничный</h1>
          <h6 class="uppercase">Потенциал использования Шаблонизатора</h6>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center">
          <h3 class="mb40 mb-xs-24">Аутентичный дизайн, который придает
            <br> утонченный, уверенный вид вашему сайту.</h3>
          <a class="btn btn-lg btn-filled" href="chooser.html">Посмотреть</a>
          <!--Check Out The Demos-->
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-cta-2" class="vim" vbp="действие,форма" vbr="Вызов к действию с формой" icons="mailchimp-cmonitor">
  <section class="image-bg parallax overlay">
    <div class="background-image-holder">
      <img alt="image" class="background-image" src="../img/home-2-5.jpg">
    </div>
    <div class="container">
      <div class="row mb40 mb-xs-24">
        <div class="col-sm-12 text-center">
          <h3 class="mb8">Подписаться на ежемесячные обновления</h3>
          <p>Не беспокойтесь, мы тоже не любим спам</p>
        </div>
      </div>

      <div class="row mb80 mb-xs-24">
        <div class="col-sm-6 col-sm-offset-3">
          <form class="form-newsletter halves" data-success="Спасибо! Мы скоро свяжемся." data-error="Пожалуйста, заполните все поля.">
            <input type="text" name="email" class="mb0 transparent validate-email validate-required signup-email-field" placeholder="Адрес эл.почты">
            <button type="submit" class="mb0">Подписаться</button>
            <iframe srcdoc="<!-- Begin MailChimp Signup Form --><link href=&quot;https://cdn-images.mailchimp.com/embedcode/classic-081711.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;><style type=&quot;text/css&quot;>    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }    /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */</style><div id=&quot;mc_embed_signup&quot;><form action=&quot;//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=94d040322a&quot; method=&quot;post&quot; id=&quot;mc-embedded-subscribe-form&quot; name=&quot;mc-embedded-subscribe-form&quot; class=&quot;validate&quot; target=&quot;_blank&quot; novalidate>    <div id=&quot;mc_embed_signup_scroll&quot;>    <h2>Subscribe to our mailing list</h2><div class=&quot;indicates-required&quot;><span class=&quot;asterisk&quot;>*</span> indicates required</div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-EMAIL&quot;>Email Address  <span class=&quot;asterisk&quot;>*</span></label>    <input type=&quot;email&quot; value=&quot;&quot; name=&quot;EMAIL&quot; class=&quot;required email&quot; id=&quot;mce-EMAIL&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-FNAME&quot;>First Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;FNAME&quot; class=&quot;&quot; id=&quot;mce-FNAME&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-LNAME&quot;>Last Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;LNAME&quot; class=&quot;&quot; id=&quot;mce-LNAME&quot;></div>    <div id=&quot;mce-responses&quot; class=&quot;clear&quot;>        <div class=&quot;response&quot; id=&quot;mce-error-response&quot; style=&quot;display:none&quot;></div>        <div class=&quot;response&quot; id=&quot;mce-success-response&quot; style=&quot;display:none&quot;></div>    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->    <div style=&quot;position: absolute; left: -5000px;&quot;><input type=&quot;text&quot; name=&quot;b_77142ece814d3cff52058a51f_94d040322a&quot; tabindex=&quot;-1&quot; value=&quot;&quot;></div>    <div class=&quot;clear&quot;><input type=&quot;submit&quot; value=&quot;Subscribe&quot; name=&quot;subscribe&quot; id=&quot;mc-embedded-subscribe&quot; class=&quot;button&quot;></div>    </div></form></div><script type='text/javascript' src='https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script><!--End mc_embed_signup-->" class="mail-list-form">
            </iframe>
          </form>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center spread-children-large">
          <img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="../img/c1.png">
          <img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="../img/c2.png">
          <img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="../img/c3.png">
          <img alt="pic" class="image-xxs mb-xs-8 fade-1-4" src="../img/c4.png">
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-cta-3" class="vim" vbp="действие" vbr="Вызов к действию с аватарками">
  <section class="bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row mb64 mb-xs-24">
        <div class="col-sm-12 text-center spread-children-large">
          <img alt="Pic" src="../img/avatar1.png">
          <img alt="Pic" src="../img/avatar2.png">
          <img alt="Pic" src="../img/avatar3.png">
          <img alt="Pic" src="../img/avatar4.png">
          <img alt="Pic" src="../img/avatar5.png">
        </div>
      </div>

      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
          <h2>Создано профессионалами для тебя.</h2>
          <p class="mb40 mb-xs-24">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque iusto pariatur magni!
            Molestias aliquam reiciendis consequuntur officiis facilis fugiat pariatur cupiditate expedita iure possimus reprehenderit id,
            natus quos quas nulla?
          </p>
          <a class="mb0 btn btn-lg btn-filled" href="#purchase-template">Приобрести</a>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-cta-4" class="vim" vbp="действие,форма" vbr="Вызов к действию с формой 2" icons="mailchimp-cmonitor">
  <section class="bg-primary">
    <div class="container">
      <div class="row mb48 mb-xs-24">
        <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 text-center">
          <img alt="Pic" class="mb16" src="../img/vent2.png">
          <h5 class="uppercase">Lorem ipsum dolor sit amet</h5>
        </div>
      </div>

      <div class="row mb40 mb-xs-24">
        <div class="col-sm-6 col-sm-offset-3">
          <form class="form-newsletter halves" data-success="Спасибо! Мы скоро свяжемся." data-error="Пожалуйста, заполните все поля.">
            <input type="text" name="email" class="mb0 transparent validate-email validate-required signup-email-field" placeholder="Email Address">
            <button type="submit" class="btn-white mb0">Держать меня в курсе</button>
            <iframe srcdoc="<!-- Begin MailChimp Signup Form --><link href=&quot;https://cdn-images.mailchimp.com/embedcode/classic-081711.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot;><style type=&quot;text/css&quot;>    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }    /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */</style><div id=&quot;mc_embed_signup&quot;><form action=&quot;//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=94d040322a&quot; method=&quot;post&quot; id=&quot;mc-embedded-subscribe-form&quot; name=&quot;mc-embedded-subscribe-form&quot; class=&quot;validate&quot; target=&quot;_blank&quot; novalidate>    <div id=&quot;mc_embed_signup_scroll&quot;>    <h2>Subscribe to our mailing list</h2><div class=&quot;indicates-required&quot;><span class=&quot;asterisk&quot;>*</span> indicates required</div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-EMAIL&quot;>Email Address  <span class=&quot;asterisk&quot;>*</span></label>    <input type=&quot;email&quot; value=&quot;&quot; name=&quot;EMAIL&quot; class=&quot;required email&quot; id=&quot;mce-EMAIL&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-FNAME&quot;>First Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;FNAME&quot; class=&quot;&quot; id=&quot;mce-FNAME&quot;></div><div class=&quot;mc-field-group&quot;>    <label for=&quot;mce-LNAME&quot;>Last Name </label>    <input type=&quot;text&quot; value=&quot;&quot; name=&quot;LNAME&quot; class=&quot;&quot; id=&quot;mce-LNAME&quot;></div>    <div id=&quot;mce-responses&quot; class=&quot;clear&quot;>        <div class=&quot;response&quot; id=&quot;mce-error-response&quot; style=&quot;display:none&quot;></div>        <div class=&quot;response&quot; id=&quot;mce-success-response&quot; style=&quot;display:none&quot;></div>    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->    <div style=&quot;position: absolute; left: -5000px;&quot;><input type=&quot;text&quot; name=&quot;b_77142ece814d3cff52058a51f_94d040322a&quot; tabindex=&quot;-1&quot; value=&quot;&quot;></div>    <div class=&quot;clear&quot;><input type=&quot;submit&quot; value=&quot;Subscribe&quot; name=&quot;subscribe&quot; id=&quot;mc-embedded-subscribe&quot; class=&quot;button&quot;></div>    </div></form></div><script type='text/javascript' src='https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script><!--End mc_embed_signup-->" class="mail-list-form">
            </iframe>
          </form>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 text-center">
          <p class="fade-half lead mb0">*Lorem ipsum dolor sit amet</p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-cta-5" class="vim" vbp="действие" vbr="Вызов к действию">
  <section class="bg-primary">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h3 class="mb0 inline-block p32 p0-xs">Lorem ipsum dolor sit amet</h3>
          <a class="btn btn-lg btn-white mb8 mt-xs-24" href="#">Приобрести</a>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-cta-6" class="vim" vbp="действие" vbr="Вызов к действию на черном фоне">
  <section class="bg-dark pt64 pb64">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2 class="mb8">Lorem ipsum dolor sit amet</h2>
          <p class="lead mb40 mb-xs-24">
            Конструктор Страниц, Lorem ipsum dolor sit amet.
          </p>
          <a class="btn btn-filled btn-lg mb0" href="#purchase-template">Приобрести Шаблонизатор</a>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-cta-7" class="vim" vbp="действие" vbr="Вызов к действию с изображением">
  <section class="bg-dark pb0">
    <div class="container">
      <div class="row mb64 mb-xs-32">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
          <h1 class="large">Иди и купи это.</h1>
          <p class="lead mb48 mb-xs-32 fade-1-4">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque.
          </p>
          <a class="btn btn-lg btn-filled" href="#purhcase-template">Продолжить</a>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
          <img alt="App" src="../img/app6.png">
        </div>
      </div>
    </div>
  </section>
</section>