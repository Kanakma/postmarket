<section id="pm-slider-4" class="vim" vbp="заголовок" vbr="Изображение со значком">
  <section class="pt240 pb240 parallax image-bg overlay bg-light">
    <div class="background-image-holder">
      <img alt="изображение" class="background-image" src="../img/home19.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <img alt="Pic" src="../img/logo-dark.png" class="image-small">
        </div>
      </div>

    </div>

    <div class="align-bottom text-center">
      <ul class="list-inline social-list mb24">
        <li>
          <a href="#">
            <i vht="li" class="ti-twitter-alt"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i vht="li" class="ti-facebook"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i vht="li" class="ti-dribbble"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i vht="li" class="ti-vimeo-alt"></i>
          </a>
        </li>
      </ul>
    </div>
  </section>
</section>

<section id="pm-icons-3" class="vim" vbp="значки" vbr="Округленные значки">
  <section class="pb64 pb-xs-40 toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="feature feature-2 filled text-center">
            <div class="text-center" vht=".col-sm-4">
              <i class="ti-layers icon-sm"></i>
              <h5 class="uppercase">Модульный Дизайн</h5>
            </div>
            <p>
              Со множеством блоков, цветов и шрифтов можно придумать огромное количество комбинаций.
            </p>
          </div>

        </div>
        <div class="col-sm-4">
          <div class="feature feature-2 filled text-center">
            <div class="text-center" vht=".col-sm-4">
              <i class="ti-gallery icon-sm"></i>
              <h5 class="uppercase">Шелковый Параллакс</h5>
            </div>
            <p>
              Мы построили плавный параллакс с вниманием на производительность.
              Протестировано на разных браузерах.
            </p>
          </div>

        </div>
        <div class="col-sm-4">
          <div class="feature feature-2 filled text-center">
            <div class="text-center" vht=".col-sm-4">
              <i class="ti-package icon-sm"></i>
              <h5 class="uppercase">Уникальные Концепции</h5>
            </div>
            <p>
              10 свежих и уникальных готовых концепций.
            </p>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-4" class="vim" vbp="значки" vbr="Центрированные значки">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 text-center">
          <div class="feature" vht="parent">
            <i class="ti-gallery icon fade-3-4 inline-block mb16"></i>
            <h4>Шелковый Параллакс</h4>
            <p>
              Мы построили плавный параллакс с вниманием на производительность.
              Протестировано на разных браузерах.
            </p>
          </div>
        </div>
        <div class="col-sm-4 text-center">
          <div class="feature" vht="parent">
            <i class="ti-package icon fade-3-4 inline-block mb16"></i>
            <h4>Уникальные Концепции</h4>
            <p>
              10 свежих и уникальных готовых концепций.
            </p>
          </div>
        </div>
        <div class="col-sm-4 text-center">
          <div class="feature" vht="parent">
            <i class="ti-layers icon fade-3-4 inline-block mb16"></i>
            <h4>Модульный Дизайн</h4>
            <p>
              Со множеством блоков, цветов и шрифтов можно придумать огромное количество комбинаций.
            </p>
          </div>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-5" class="vim" vbp="значки" vbr="Значки на изображении">
  <section class="image-bg overlay parallax">
    <div class="background-image-holder">
      <img alt="Background" class="background-image" src="../img/cover1.jpg">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h2 class="mb16">Здесь есть много чего, что можно полюбить</h2>
          <p class="lead mb64">
            Универсальные шаблоны с особенностями.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4">
          <div class="feature feature-1 boxed">
            <div class="text-center" vht=".col-sm-4">
              <i class="ti-package icon"></i>
              <h5 class="uppercase">Уникальные Концепции</h5>
            </div>
            <p>
              10 свежих и уникальных готовых концепций.
            </p>
          </div>

        </div>
        <div class="col-sm-4">
          <div class="feature feature-1 boxed">
            <div class="text-center" vht=".col-sm-4">
              <i class="ti-infinite icon"></i>
              <h5 class="uppercase">Безграничный Потенциал</h5>
            </div>
            <p>
              С конструктором вы найдете подходящий вам дизайн.
            </p>
          </div>

        </div>
        <div class="col-sm-4">
          <div class="feature feature-1 boxed">
            <div class="text-center" vht=".col-sm-4">
              <i class="ti-heart icon"></i>
              <h5 class="uppercase">Удобный для Разработчиков</h5>
            </div>
            <p>
              Мы не забыли про разработчиков, поэтому мы создали Три Клика
              с нуля, как мощный, простой фреймворк
              с особым вниманием на читаемость кода.
            </p>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-6" class="vim" vbp="значки" vbr="Значки со списком">
  <section class="bg-dark toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h6 class="uppercase">Заголовок Раздела</h6>
          <hr class="mb160 mb-xs-24">
        </div>
      </div>

      <div class="row">
        <div class="col-md-10">
          <h1 class="thin">Сфокусированный, Разнообразный, Разрушительный.</h1>
        </div>
      </div>

      <div class="row mb160 mb-xs-0">
        <div class="col-md-6 col-sm-8">
          <p class="lead">
            Шаблонизатор поддерживает множественные разделы
            Разрушительная технология - это наша объединяющая тема
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-3 col-sm-6 mb-xs-24">
          <i class="ti-pulse icon mb32"></i>
          <h6 class="uppercase">Контроль Здоровья</h6>
          <ul>
            <li>Элемент 1</li>
            <li>Элемент 2</li>
            <li>Элемент 3</li>
            <li>Элемент 4</li>
            <li>Элемент 5</li>
            <li>Элемент 6</li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 mb-xs-24">
          <i class="ti-map-alt icon mb32"></i>
          <h6 class="uppercase">Сервисы место нахождения</h6>
          <ul>
            <li>Элемент 1</li>
            <li>Элемент 2</li>
            <li>Элемент 3</li>
            <li>Элемент 4</li>
            <li>Элемент 5</li>
            <li>Элемент 6</li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 mb-xs-24">
          <i class="ti-mobile icon mb32"></i>
          <h6 class="uppercase">Социальные Данные</h6>
          <ul>
            <li>Элемент 1</li>
            <li>Элемент 2</li>
            <li>Элемент 3</li>
            <li>Элемент 4</li>
            <li>Элемент 5</li>
            <li>Элемент 6</li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 mb-xs-24">
          <i class="ti-harddrives icon mb32"></i>
          <h6 class="uppercase">Облачное Предприятие</h6>
          <ul>
            <li>Элемент 1</li>
            <li>Элемент 2</li>
            <li>Элемент 3</li>
            <li>Элемент 4</li>
          </ul>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-7" class="vim" vbp="значки" vbr="Значки в ограниченных колонках">
  <section class="bg-dark pt120 pb120 pt-xs-40 pb-xs-40 toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-2">
          <div class="feature feature-3 mb64 mb-xs-24">
            <div class="left">
              <i class="ti-signal icon-sm"></i>
            </div>
            <div class="right">
              <h4 class="mb16">Будь Организован</h4>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
              </p>
            </div>
          </div>

        </div>
        <div class="col-sm-6 col-md-4">
          <div class="feature feature-3 mb64 mb-xs-24">
            <div class="left">
              <i class="ti-ruler-alt icon-sm"></i>
            </div>
            <div class="right">
              <h4 class="mb16">Стань Креативным</h4>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
              </p>
            </div>
          </div>

        </div>
        <div class="col-sm-6 col-md-4 col-md-offset-2">
          <div class="feature feature-3 mb-xs-24">
            <div class="left">
              <i class="ti-layers icon-sm"></i>
            </div>
            <div class="right">
              <h4 class="mb16">Шелковый Параллакс</h4>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
              </p>
            </div>
          </div>

        </div>
        <div class="col-sm-6 col-md-4">
          <div class="feature feature-3">
            <div class="left">
              <i class="ti-heart icon-sm"></i>
            </div>
            <div class="right">
              <h4 class="mb16">Элитный Элемент Автора</h4>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
              </p>
            </div>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-8" class="vim" vbp="значки" vbr="Выровненные слева значки">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="feature feature-3 mb-xs-24 mb64" vht="parent">
            <div class="left">
              <i class="ti-panel icon-sm"></i>
            </div>
            <div class="right">
              <h5 class="uppercase mb16">Экспертный, Модульный дизайн</h5>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.
              </p>
            </div>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="feature feature-3 mb-xs-24 mb64" vht="parent">
            <div class="left">
              <i class="ti-medall icon-sm"></i>
            </div>
            <div class="right">
              <h5 class="uppercase mb16">Доверенный, Элитный Автор</h5>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.
              </p>
            </div>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="feature feature-3 mb-xs-24 mb64" vht="parent">
            <div class="left">
              <i class="ti-layout icon-sm"></i>
            </div>
            <div class="right">
              <h5 class="uppercase mb16">Максимальная Гибкость</h5>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.
              </p>
            </div>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="feature feature-3 mb-xs-24 mb64" vht="parent">
            <div class="left">
              <i class="ti-comment-alt icon-sm"></i>
            </div>
            <div class="right">
              <h5 class="uppercase mb16">Целенаправленная Поддержка</h5>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.
              </p>
            </div>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="feature feature-3 mb-xs-24" vht="parent">
            <div class="left">
              <i class="ti-infinite icon-sm"></i>
            </div>
            <div class="right">
              <h5 class="uppercase mb16">Бесконечные Макеты</h5>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.
              </p>
            </div>
          </div>

        </div>
        <div class="col-md-4 col-sm-6">
          <div class="feature feature-3 mb-xs-24" vht="parent">
            <div class="left">
              <i class="ti-dashboard icon-sm"></i>
            </div>
            <div class="right">
              <h5 class="uppercase mb16">Создано для Производительности</h5>
              <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.
              </p>
            </div>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-9" class="vim" vbp="значки" vbr="Коробочные значки">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="feature feature-1 boxed" vht="parent">
            <div class="text-center">
              <i class="ti-pulse icon"></i>
              <h4>Элегантные и практичные шаблоны,
                <br class="hidden-sm"> созданный с целью.</h4>
            </div>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
            </p>
          </div>

        </div>
        <div class="col-sm-6">
          <div class="feature feature-1 boxed" vht="parent">
            <div class="text-center">
              <i class="ti-dashboard icon"></i>
              <h4>Созданный с нуля
                <br class="hidden-sm"> для скорости и производительности.</h4>
            </div>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
            </p>
          </div>

        </div>
        <div class="col-sm-6">
          <div class="feature feature-1 boxed" vht="parent">
            <div class="text-center">
              <i class="ti-layers icon"></i>
              <h4>Система дизайна, которая включает
                <br class="hidden-sm"> массив разных элементов.</h4>
            </div>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
            </p>
          </div>

        </div>
        <div class="col-sm-6">
          <div class="feature feature-1 boxed" vht="parent">
            <div class="text-center">
              <i class="ti-package icon"></i>
              <h4>Полный пакет
                <br class="hidden-sm"> с интуитивным конструктором страниц.</h4>
            </div>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.
            </p>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-10" class="vim" vbp="значки" vbr="Значок - разделитель">
  <section class="bg-secondary toggle-bg-mrv">
    <div class="container">
      <div class="row mb40 mb-xs-0">
        <div class="col-sm-12 text-center">
          <h4 class="uppercase">Дизайн, которым можно гордиться</h4>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 col-md-5 text-right text-center-xs">
          <h1 class="large mb8">10,000+</h1>
          <h6 class="uppercase">Клиентов, пользующихся нашим дизайном</h6>
        </div>
        <div class="col-md-2 text-center hidden-sm hidden-xs">
          <i class="ti-infinite icon icon-lg color-primary mt8 mt-xs-0"></i>
        </div>
        <div class="col-sm-6 col-md-5 text-center-xs">
          <h1 class="large mb8">Безграничный</h1>
          <h6 class="uppercase">потенциал использования Шаблонизатора</h6>
        </div>
      </div>

    </div>

  </section>
</section>