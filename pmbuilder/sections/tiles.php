<section id="pm-textimage-11" class="vim" vbp="текст+изобр" vbr="Панели с содержимым">
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
          <h4 class="uppercase mb16">Плитки с содержимым</h4>
          <p class="lead mb80">
            Эти комплексные плитки могут послужить идеальными представлениями проектаили как ссылки для разных страниц.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-sm-10 col-sm-offset-1 col-md-offset-2">
          <div class="horizontal-tile vpe">
            <div class="tile-left">
              <a href="#">
                <div class="background-image-holder">
                  <img alt="image" class="background-image" src="../img/project-single-1.jpg">
                </div>
              </a>
            </div>
            <div class="tile-right bg-secondary">
              <div class="description">
                <h4 class="mb8">Брачный союз</h4>
                <h6 class="uppercase">
                  Графический дизайн
                </h6>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
          </div>
          <div class="horizontal-tile vpe">
            <div class="tile-left">
              <a href="#">
                <div class="background-image-holder">
                  <img alt="image" class="background-image" src="../img/project-single-3.jpg">
                </div>
              </a>
            </div>
            <div class="tile-right bg-secondary">
              <div class="description">
                <h4 class="mb8">Встречая рассвет</h4>
                <h6 class="uppercase">
                  ФотосЪемка образа
                </h6>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>

<section id="pm-icons-1" class="vim" vbp="значки" vbr="Центрированные колонки">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row mb64 mb-xs-24">
        <div class="col-sm-12 col-md-10 col-md-offset-1 text-center">

          <h3>Стройте свой сайт с помощью блоков с содержимым</h3>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6 text-center">
          <div class="feature">
            <div class="text-center">
              <i class="ti-layout-grid2 icon-lg mb40 mb-xs-24 inline-block color-primary"></i>
              <h5 class="uppercase">Экспертный, Модульный дизайн</h5>
            </div>
            <p>
              Клиенты любят наш блочный подход к шаблонам,
              <br class="hidden-sm" />это позволяет строить красивые сайты быстро и удовлетворительно, оставляя
              <br class="hidden-sm" /> больше времени отточить ваш идеальный макет.
            </p>
          </div>

        </div>
        <div class="col-sm-6 text-center">
          <div class="feature">
            <div class="text-center">
              <i class="ti-heart icon-lg mb40 mb-xs-24 inline-block color-primary"></i>
              <h5 class="uppercase">Гибкость для Разработчиков</h5>
            </div>
            <p>
              Мы не забыли про разработчиков, поэтому мы создали Три Клика
              <br class="hidden-sm" /> с нуля, как мощный, простой фреймворк
              <br class="hidden-sm" /> с особым вниманием на читаемость кода.
            </p>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-icons-2" class="vim" vbp="значки" vbr="Выровненные слева колонки">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row mb64 mb-xs-24">
        <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
          <h3>Наша экспертиза</h3>
        </div>
      </div>

      <div class="row">
        <div class="col-md-5 col-md-offset-1 col-sm-6 mb40 mb-xs-24">
          <i class="ti-layers icon inline-block mb16 fade-3-4"></i>
          <h4>Модульный дизайн</h4>
          <p>
            Со множеством блоков, цветов и шрифтов можно придумать.
          </p>
        </div>
        <div class="col-md-5 col-sm-6 mb40 mb-xs-24">
          <i class="ti-gallery icon inline-block mb16 fade-3-4"></i>
          <h4>Шелковый Параллакс</h4>
          <p>
            lorem ipsum dolor sit amet. conseqtuo
          </p>
        </div>
        <div class="col-md-5 col-md-offset-1 col-sm-6 mb40 mb-xs-24">
          <i class="ti-package icon inline-block mb16 fade-3-4"></i>
          <h4>Уникальные Концепции</h4>
          <p>
            10 свежих и уникальных готовых концепций.
          </p>
        </div>
        <div class="col-md-5 col-sm-6 mb40 mb-xs-24">
          <i class="ti-infinite icon inline-block mb16 fade-3-4"></i>
          <h4>Безграничные Возможности</h4>
          <p>
            С конструктором вы найдете подходящий вам дизайн.
          </p>
        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-tabs-1" class="vim" vbp="панели" vbr="Кнопочные панели">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
          <h4 class="uppercase mb80">Кнопочная Панель</h4>
          <div class="tabbed-content button-tabs">
            <ul class="tabs">
              <li class="active">
                <div class="tab-title" vht="li">
                  <span vht="li">История</span>
                </div>
                <div class="tab-content">
                  <p>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="li">
                  <span vht="li">Подход</span>
                </div>
                <div class="tab-content">
                  <p>
                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="li">
                  <span vht="li">Культура</span>
                </div>
                <div class="tab-content">
                  <p>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title">
                  <span vht="li">Метод</span>
                </div>
                <div class="tab-content">
                  <p>
                    Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
                  </p>
                </div>
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-tabs-3" class="vim" vbp="панели" vbr="Текстовые панели">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
          <h4 class="uppercase mb80">Текстовые Панели</h4>
          <div class="tabbed-content text-tabs">
            <ul class="tabs">
              <li class="active">
                <div class="tab-title" vht="parent">
                  <span vht="li">История</span>
                </div>
                <div class="tab-content">
                  <p>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="parent">
                  <span vht="li">Подход</span>
                </div>
                <div class="tab-content">
                  <p>
                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="parent">
                  <span vht="li">Культура</span>
                </div>
                <div class="tab-content">
                  <p>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="parent">
                  <span vht="li">Метод</span>
                </div>
                <div class="tab-content">
                  <p>
                    Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
                  </p>
                </div>
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-tabs-2" class="vim" vbp="панели" vbr="Панели со значками">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-12 text-center">
          <h4 class="uppercase mb80">Панели со значками</h4>
          <div class="tabbed-content icon-tabs">
            <ul class="tabs">
              <li class="active">
                <div class="tab-title" vht="parent">
                  <i class="ti-layers icon" vht="li"></i>
                  <span vht="li">История</span>
                </div>
                <div class="tab-content">
                  <p>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="parent">
                  <i class="ti-package icon" vht="li"></i>
                  <span vht="li">Подход</span>
                </div>
                <div class="tab-content">
                  <p>
                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="parent">
                  <i class="ti-stats-up icon" vht="li"></i>
                  <span vht="li">Культура</span>
                </div>
                <div class="tab-content">
                  <p>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="parent">
                  <i class="ti-layout-media-center-alt icon" vht="li"></i>
                  <span vht="li">Метод</span>
                </div>
                <div class="tab-content">
                  <p>
                    Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.
                  </p>
                </div>
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-tabs-4" class="vim" vbp="панели" vbr="Вертикальные кнопочные панели">
  <section class="toggle-bg-mrv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="uppercase mb80">Вертикальные Кнопки</h4>
          <div class="tabbed-content button-tabs vertical">
            <ul class="tabs">
              <li class="active">
                <div class="tab-title" vht="li">
                  <span vht="li">История</span>
                </div>
                <div class="tab-content">
                  <h5 class="uppercase">Панель 1</h5>
                  <hr>
                  <p>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
                  </p>
                  <p>
                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="li">
                  <span vht="li">Подход</span>
                </div>
                <div class="tab-content">
                  <h5 class="uppercase">Панель 2</h5>
                  <hr>
                  <p>
                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                  </p>
                  <p>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="li">
                  <span vht="li">Культура</span>
                </div>
                <div class="tab-content">
                  <h5 class="uppercase">Панель 3</h5>
                  <hr>
                  <p>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est.
                  </p>
                </div>
              </li>
              <li>
                <div class="tab-title" vht="li">
                  <span vht="li">Метод</span>
                </div>
                <div class="tab-content">
                  <h5 class="uppercase">Панель 4</h5>
                  <hr>
                  <p>
                    Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                  </p>
                  <p>
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est.
                  </p>
                </div>
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>

<section id="pm-tabs-5" class="vim" vbp="панели,новый" vbr="Панели с режимом" icons="new">
  <section>
    <div class="container">
      <div class="row mb64 mb-xs-24">
        <div class="col-sm-12 text-center">
          <h3>Окружи себя идеями</h3>
          <p class="lead">
            Подготовься к долгому обсуждению про лучшее и яркое в вебе.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="tabbed-content button-tabs">
            <ul class="tabs thirds mb64 mb-xs-24">
              <li class="active">
                <div class="tab-title">
                  <span>Утро</span>
                </div>
                <div class="tab-content text-left">
                  <div>
                    <div class="overflow-hidden">
                      <img alt="Pic" class="mb24 pull-left" src="../img/avatar1.png">
                      <div class="pull-left p32 p0-xs pt24">
                        <h6 class="uppercase mb8 number">9:30 - 10:30</h6>
                        <h4>Джон Доу - Электронная Коммерция</h4>
                      </div>
                    </div>
                    <p>
                      Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                    </p>
                    <hr class="mt40 mb40 mt-xs-0 mb-xs-24">
                  </div>
                  <div>
                    <div class="overflow-hidden">
                      <img alt="Pic" class="mb24 pull-left" src="../img/avatar2.png">
                      <div class="pull-left p32 p0-xs pt24">
                        <h6 class="uppercase mb8 number">11:00 - 12:00</h6>
                        <h4>Джон Доу - Больше Продаж</h4>
                      </div>
                    </div>
                    <p>
                      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                    </p>
                    <hr class="mt40 mb40 mt-xs-0 mb-xs-24">
                  </div>
                </div>
              </li>
              <li>
                <div class="tab-title">
                  <span>Обед</span>
                </div>
                <div class="tab-content text-left">
                  <div>
                    <div class="overflow-hidden">
                      <img alt="Pic" class="mb24 pull-left" src="../img/avatar3.png">
                      <div class="pull-left p32 p0-xs pt24">
                        <h6 class="uppercase mb8 number">13:30 - 14:30</h6>
                        <h4>Джон Доу - Дизайн</h4>
                      </div>
                    </div>
                    <p>
                      At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda.
                    </p>
                    <hr class="mt40 mb40 mt-xs-0 mb-xs-24">
                  </div>
                  <div>
                    <div class="overflow-hidden">
                      <img alt="Pic" class="mb24 pull-left" src="../img/avatar4.png">
                      <div class="pull-left p32 p0-xs pt24">
                        <h6 class="uppercase mb8 number">15:00 - 16:00</h6>
                        <h4>Джон Доу</h4>
                      </div>
                    </div>
                    <p>
                      Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                    </p>
                    <hr class="mt40 mb40 mt-xs-0 mb-xs-24">
                  </div>
                </div>
              </li>
              <li>
                <div class="tab-title">
                  <span>Вечер</span>
                </div>
                <div class="tab-content text-left">
                  <div>
                    <div class="overflow-hidden">
                      <img alt="Pic" class="mb24 pull-left" src="../img/avatar5.png">
                      <div class="pull-left p32 p0-xs pt24">
                        <h6 class="uppercase mb8 number">17:30 - 18:30</h6>
                        <h4>Джон Доу - Журналистика</h4>
                      </div>
                    </div>
                    <p>
                      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
                    </p>
                    <hr class="mt40 mb40 mt-xs-0 mb-xs-24">
                  </div>
                  <div>
                    <div class="overflow-hidden">
                      <img alt="Pic" class="mb24 pull-left" src="../img/avatar6.png">
                      <div class="pull-left p32 p0-xs pt24">
                        <h6 class="uppercase mb8 number">19:00pm - 20:00pm</h6>
                        <h4>Джон Доу - Встреча</h4>
                      </div>
                    </div>
                    <p>
                      At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda.
                    </p>
                    <hr class="mt40 mb40 mt-xs-0 mb-xs-24">
                  </div>
                </div>
              </li>
            </ul>
          </div>

        </div>
      </div>

    </div>

  </section>
</section>