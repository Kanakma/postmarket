<section id="pm-contact-2" class="vim" vbp="форма,карта" vbr="Форма контакта с картой">
	<section class="image-square right">
		<div class="col-md-6 p0 image">
			<div class="map-holder background-image-holder">
				<iframe 
					no-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2907.051485208183!2d76.94960796643439!3d43.22938018956905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836f1c97907697%3A0x45ca12c9fb96dcda!2z0JHQpiAi0KHTmdGC0YLRliIsINCh0LDQvNCw0LstMiA1OCwgQWxtYXR5IDA1MDA1MSwgS2F6YWtoc3Rhbg!5e0!3m2!1sen!2sus!4v1482484903552">
				</iframe>
			</div>
		</div>
		<div class="col-md-6 content">
			<form class="form-email" data-success="Спасибо! Мы скоро свяжемся с вами." data-error="Пожалуйста, заполните все поля.">
				<h6 class="uppercase text-center">Отправить сообщение</h6>
				<input type="text" class="validate-required" name="name" placeholder="Your Name">
				<input type="text" class="validate-required validate-email" name="email" placeholder="Email Address">
				<textarea class="validate-required" name="message" rows="4" placeholder="Message"></textarea>
				<button type="submit">Отправить сообщение</button>
			</form>
		</div>
	</section>
</section>

<!--<section id="pm-contact-4" class="vim" vbp="форма,карта" vbr="Форма контакта с картой 2">
	<section class="image-square right">
		<div class="col-md-6 p0 image">
			<div class="map-canvas" data-address="Melbourne, Victoria, Australia" data-map-zoom="12"></div>
		</div>
		<div class="col-md-6 content">
			<form class="form-email" data-success="Спасибо! Мы скоро свяжемся с вами." data-error="Пожалуйста, заполните все поля.">
				<h6 class="uppercase text-center">Отправить сообщение</h6>
				<input type="text" class="validate-required" name="name" placeholder="Your Name">
				<input type="text" class="validate-required validate-email" name="email" placeholder="Email Address">
				<textarea class="validate-required" name="message" rows="4" placeholder="Message"></textarea>
				<button type="submit">Отправить сообщение</button>
			</form>
		</div>
	</section>
</section>-->

<section id="pm-contact-3" class="vim" vbp="карта" vbr="Карта">
	<section class="p0">
		<div class="map-holder pt180 pb180">
			<iframe no-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2907.051485208183!2d76.94960796643439!3d43.22938018956905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836f1c97907697%3A0x45ca12c9fb96dcda!2z0JHQpiAi0KHTmdGC0YLRliIsINCh0LDQvNCw0LstMiA1OCwgQWxtYXR5IDA1MDA1MSwgS2F6YWtoc3Rhbg!5e0!3m2!1sen!2sus!4v1482484903552"></iframe>
		</div>
	</section>
</section>

<!--<section id="pm-contact-5" class="vim" vbp="карта" vbr="Карта 2">
	<section class="pt240 pb240">
			<div class="map-canvas" data-address="Melbourne, Victoria, Australia" data-map-zoom="12"></div>
	</section>
</section>-->