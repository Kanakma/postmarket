<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include_once '../../../includes/db_connect.php';
session_start();
if (isset($_POST['product-name']) && isset($_POST['product-price'])) {

	$_SESSION['cart'][] = array("name"=>$_POST['product-name'], "price"=>$_POST['product-price'], "qty"=>$_POST['qty'], "details"=>$_POST['details']);
	echo json_encode($_SESSION['cart'], JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	exit();
} else {
	// echo json_encode("error");
	// exit();
}
if (isset($_GET['useless'])) {
	if (isset($_SESSION['cart'])) {
		echo json_encode($_SESSION['cart'], JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	}
	exit();
}

if (isset($_POST['index'])) {
	$index = $_POST['index'];
	try {
		unset($_SESSION['cart'][$index]);
		echo json_encode($_SESSION['cart'], JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
	} catch(Exception $e) {
		echo $e;
	}
}

if (isset($_POST['customer_name']) and isset($_POST['customer_email']) and isset($_POST['customer_city']) and isset($_POST['customer_address']) and isset($_SESSION['cart'])) {
	$name = $_POST['customer_name'];
	$email = $_POST['customer_email'];
	$phone = $_POST['customer_phone'];
	$city = $_POST['customer_city'];
	$address = $_POST['customer_address'];
	$customer_id = '';
	$query = $mysqli->query("SELECT id FROM customers WHERE email = '$email'");
	if ($query->num_rows == 0) {
		$query = $mysqli->query("INSERT INTO customers VALUES (NULL, '$name', '$email', '$phone', '$city', '$address')");
		if ($query->affected_rows != 0) {
			http_response_code(200);
			$customer_id = $query->insert_id;
		} else {
			// http_response_code(500);
		}
	} else {
		$customer_id = intval($query->fetch_array()['id']);
	}

	$total_cost = floatval($_POST['total-cost']);
	$status = 'Не обработано';
	$order_id = 0;
	$request_uri = $_SERVER['REQUEST_URI'];
	$uri_parts = explode('/', $request_uri);
	$admin_name = $uri_parts[count($uri_parts)-3];
	$which_site = $uri_parts[count($uri_parts)-2];
	$sql = "INSERT INTO orders VALUES (NULL, $customer_id, '$total_cost', '$status', '$admin_name', '$which_site', NOW())";
	$query = $mysqli->query($sql);
	if ($mysqli->affected_rows != 0) {
		http_response_code(200);
		$order_id = $mysqli->insert_id;
	} else {
		echo $sql;
		http_response_code(200);
	}

	$query = $mysqli->prepare("INSERT INTO products VALUES (NULL, ?, ?, ?, ?, ?)");
	foreach ($_SESSION['cart'] as $item) {
		$query->bind_param('siiis', $item['name'], $order_id, $item['price'], $item['qty'], $item['details']);
		$query->execute();
	}

}
