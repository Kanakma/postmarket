$(window).load(function(){	
  if(window.mr_parallax != undefined){
    window.mr_parallax.callback = function(element){
      
      if( (!$(element).hasClass('parallax'))  &&  $(element).is('section:nth-of-type(1), header:nth-of-type(1)')){
        $(element).find('.background-image-holder').each(function(){
          $(this).css('top','0px');
          window.mr_parallax.mr_setTranslate3DTransform($(this).get(0), 0);
        });
      }else if($(element).hasClass('parallax')  &&  $(element).is('section:nth-of-type(1), header:nth-of-type(1)')){	
        if(!$('.viu nav').hasClass('absolute') && !$('.viu nav').hasClass('transparent'))
        {
          $(element).find('.background-image-holder').css('top', -($('.viu nav').outerHeight(true)));
        }
        else{
          $(element).find('.background-image-holder').css('top', 0);	
        }
      }else if(!$(element).hasClass('parallax')  &&  !$(element).is('section:nth-of-type(1), header:nth-of-type(1)')){
        $(element).find('.background-image-holder').each(function(){
          $(this).css('top','0px');
          window.mr_parallax.mr_setTranslate3DTransform($(this).get(0), 0);
        });
      }else if($(element).hasClass('parallax')  &&  !$(element).is('section:nth-of-type(1), header:nth-of-type(1)')){
        $(element).find('.background-image-holder').each(function(){
          $(this).css('top','0px');
          window.mr_parallax.mr_setTranslate3DTransform($(this).get(0), 0);
        });
      }
    };
  }
});
$('.viu').on('mouseenter', '.offscreen-container', function(){
  // /$('.vnj').remove();
});
$('.viu').on('click', '.offscreen-toggle', function(){
  $('.offscreen-container a').not('.close-nav').unbind('click');
  $('.vnj').addClass('reveal-nav');
  $('body').addClass('has-offscreen-nav');
  $('.vnj').each(function(){
      $(this).addClass('reveal-nav');
      $(this).css('left', (1 * $(this).attr('data-left')) + (1 * $('.vjc').outerWidth(true)) );
      //$(this).css('left', (1 * $(this).attr('data-left') + $('.vjc').outerWidth(true) ));
  });
});
$('.viu').on('click', '.main-container, .offscreen-container .close-nav', function(){
  if($('body').hasClass('has-offscreen-nav')){
    $('.vnj').each(function(){
      $(this).removeClass('reveal-nav');
      $(this).css('left', (1 * $(this).attr('data-left')));
    });
    $('.offscreen-container').removeClass('reveal-nav');
    $('.main-container').removeClass('reveal-nav');
    $('nav').removeClass('reveal-nav');
  }
});