
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/theme-purple.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
		<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,600,700" rel="stylesheet" type="text/css">
		<link href="css/font-roboto.css" rel="stylesheet" type="text/css">
	</head>

	<body class="scroll-assist">
				
		<div class="nav-container">
		</div>
		
		<div class="main-container">
		<section class="bg-secondary">
				<div class="container">
					<div class="feed-item mb96 mb-xs-48">
						<div class="row mb16 mb-xs-0">
							<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
								
								<h3>Привяжите свой домен на созданный сайт</h3>
							</div>
						</div>
						
						<div class="row mb32 mb-xs-16">
							<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
								
								<p class="lead">1. Перейдите на панель управления доменом своего регистратора</p>
								<p class="lead">
									2. Перейдите к настройкам перенаправления
								</p>
								<p class="lead">
									3. Вставьте ссылку <mark style="user-select:all;"><u>http://<?php echo $_GET['u']; ?>.almatybray.tk</u></mark>, где указано поле адреса перенаправления
								</p>
								<p class="lead">
									4. Осталось подождать, пока изменения домена вступят в силу (это занимает от нескольких минут до 15 часов).
								</p>
							</div>
							
						</div>
						<!--<div class="row">
							<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
								<hr>
							</div>
						</div>-->
					</div>
					
				</div></section></div>
		
				
	<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/parallax.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>
				