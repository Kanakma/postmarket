<?php

session_start();
session_set_cookie_params(86400);
error_reporting(E_ALL & ~E_NOTICE);

include "../includes/db_connect.php";
if (isset($_GET['demo'])) {
	$_SESSION['user']['cur_site'] = getUserSites();
  $stmt = $mysqli->query("SELECT path FROM templates WHERE name = \"" . $_GET['demo'] . "\"");
		if($row = $stmt->fetch_array()) {
			$demo = $row['path'];
		} else {
			echo $_GET['demo'];
		}
} else if(isset($_GET['s'])) {
	$stmt = $mysqli->query("SELECT template_path FROM user_sites WHERE id = " . $_GET['s']);
	if($row = $stmt->fetch_array()) {
		$demo = $row['template_path'];
	} else {
		echo "fail";
	}
	getCurrentTemplate();
}

function getCurrentTemplate() {
	global $mysqli;
	$num_site = 0;
	try {
		$result = $mysqli ->query("SELECT * FROM user_sites WHERE id = " . $_GET['s']);
		if($row = $result->fetch_array()) {
			$parts = explode("_", $row['name']);
			$_SESSION['user']['cur_site'] = $parts[2];
		}
	} catch(Exception $e) {
		echo $e;
	}
}

function getUserSites() {
	global $mysqli;
	$num_site = 0;
	try {
			$query = $mysqli->query("SELECT * FROM user_sites WHERE user_id = " . $_SESSION['user']['id']);
			$num_site = $query->num_rows;
		} catch(Exception $e) {
			echo $e;
		}
		return $num_site;
}

include "saveFile.php";
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Конструктор пользователя <?php echo $_SESSION['user']['username']; ?></title>
	<link rel="stylesheet" href="css/pm-icons.css">
	<link rel="stylesheet" href="css/pm-social-icons.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/style.css">
	<link href="http://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet" type="text/css">

	<link href="theme/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
	<link href="theme/css/themify-icons.css" rel="stylesheet" type="text/css" media="all">
	<link href="theme/css/flexslider.css" rel="stylesheet" type="text/css" media="all">
	<link href="theme/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all">
	<link href="theme/css/ytplayer.css" rel="stylesheet" type="text/css" media="all">
	<link href="theme/css/theme.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/custom.css" rel="stylesheet" type="text/css" media="all">
  <link rel="stylesheet" href="css/custom.new.css">
	<!--<link href="http://fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600" rel="stylesheet" type="text/css">-->

	<style>
		.instafeed:before{ position: absolute; width: 100%; height: 100%; top: 0; left: 0; content: ''; z-index: 4; }
		.background-image-holder{ transition: all 0s !important; }
		.viu{ background: #ffffff; }
		.image-edge .vnu, .imade-edge img{ z-index: 50; }
		nav.absolute{ z-index: 20; padding-left: 48px; }
		.boxed-layout .viu{ background: #eee; }
		.boxed-layout nav.absolute{ max-width: 1366px; left: 0; right: 0; margin: 0 auto; }
		.nav-bar .vnu{ max-height: 60%; display: inline-block; }
		.masonry-loader.fadeOut{ display: none; }
		section.projects > .masonry .project:nth-of-type(2) .vnw{ padding-top: 56px;}
		.main-container.reveal-nav{ left: 48px; }
		.menu > li ul{ z-index: 10000000; }
		input[type="submit"] { margin-bottom: 24px; }
		.vnj.reveal-nav{  transform: translate3d(-50vw, 0, 0);  -webkit-transform: translate3d(-50vw, 0, 0);  -moz-transform: translate3d(-50vw, 0, 0); }
		.btn{ user-select: text !important; -webkit-user-select: text !important; }
		.offscreen-container .close-nav{ position: absolute; right: 50px; float: right; display: block; padding: 20px; text-align: right; top: 0; z-index: 10; font-size: 20px; transition: all 0.3s ease; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease; opacity: .5; }
		.nav-is-overlay nav {opacity: 1; visibility: visible; transform: translate3d(0, 0, 0) !important; -webkit-transform: translate3d(0, 0, 0) !important; -moz-transform: translate3d(0, 0, 0) !important;}
		.vnw{ top: 0; }
		.tabbed-content .vnu{ margin-bottom: 24px; }
	</style>


	<style>
		.vhc{position:fixed;width:100%;height:100%;z-index:9999;background:#2b2b2b;font-family:Roboto,"Helvetica Neue",Helvetica,Helvetica,Arimo,Arial,sans-serif!important;font-smoothing:antialiased;-webkit-font-smoothing:antialiased;opacity:1}.vhb{position:absolute;top:50%;left:50%;width:200px;height:40px;margin-left:-100px;margin-top:-20px;z-index:2}.vhb img{width:100%}.vha{position:absolute;bottom:40px;left:50%;width:200px;margin-left:-100px;text-align:center;color:#fff;font-size:13px}.vha span{display:block;line-height:19px;opacity:.5}.vlb{max-width:60px;display:inline-block;margin-bottom:24px !important;opacity:.5}.vla{position:absolute;width:500px;top:80px;margin-left:-250px;left:50%;overflow:hidden;text-align:center;opacity:0;transition:all .3s ease;-webkit-transition:all .3s ease}.vla.vhr{opacity:1}.vlc{background:#333;color:#eee;padding:24px;border-radius:2px}.vlc .oi{display:inline-block;margin-right:8px;font-size:18px;color:#f6f6f6}.vlc p{display:inline-block}.vld{cursor:wait;background-color:transparent;opacity:.3;border-radius:30px;animation:loading .5s infinite linear;-moz-animation:loading .5s infinite linear;-webkit-animation:loading .5s infinite linear;border:5px solid #fff;border-top:5px solid transparent;border-left:5px solid transparent;width:30px;height:30px;position:absolute;left:50%;margin-left:-15px;top:50%;margin-top:50px}@keyframes loading{0%{transform:rotate(0deg);-moz-transform:rotate(0deg)}100%{transform:rotate(360deg);-moz-transform:rotate(360deg)}}@-moz-keyframes loading{0%{-moz-transform:rotate(0deg)}100%{-moz-transform:rotate(360deg)}}@-webkit-keyframes loading{0%{-webkit-transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg)}} .vnu{ display: inline-block; }
	</style>

</head>
<body mrv_namespace="" mrv_contenttarget=".main-container" mrv_navtarget=".nav-container" mrv_footertarget=".main-container">

	<div class="vhc vhq">

		<div class="vld">

		</div>

		<div class="vhb">
			<img alt="Post Market Page Builder" src="../img/post_logo_black.png">
		</div>

		<div class="vha">
			<span>&copy; Automato DevTeam<br>Все права защищены</span>
		</div>

		<div class="vjg"></div>
	</div>

	<div class="vhv vba" style="width:auto;">

		<div class="vpm vih"></div>

		<div class="vjc vjf vle">

			<div class="vlf">
				<ul>
					<li title="Страницы" class="vlg vfp"><span class="oi" data-glyph="file"></span></li>
					<li title="Содеримое" class="vlh vhr"><span class="oi" data-glyph="layers"></span></li>
					<li title="Стиль" class="vlj"><span class="oi" data-glyph="brush"></span></li>
					<li title="Метаданные" class="vpo"><span class="oi" data-glyph="browser"></span></li>
					<!--<li title="User" class=""><span class="oi" data-glyph="person"></span></li>-->
				</ul>

				<div class="vll">
          <div title="На главное меню" class="vgw">
            <a href="../index.php?page=list"><span class="oi" data-glyph="account-logout"></span></a>
          </div>
					<div title="Settings" class="vlm">
						<span class="oi" data-glyph="cog"></span>
					</div>

					<div title="Lock Sidebar" class="vgw vhq">
						<span class="oi" data-glyph="lock-unlocked"></span>
					</div>
				</div>

			</div>


			<div class="vkm vjz">
				<div class="vgx">

				<div class="vlo vlp">

					<div class="vlq">
						<span>Страницы</span>
					</div>

					<div class="vhs vgc">Сохранить страницу как</div>
					<!-- <div class="vhs ved">Import</div> -->
					<input type="file" class="vef" name="vef[]">

					<div class="vmd">

						<div class="vfw empty-vfw">

						</div>
					</div>

					<div class="vls">

						<div class="vlt">
							<span
								type="button"
								class="vah"
                data-toggle='modal'
                data-target="#myModal">Опубликовать</span>

							<!-- <span class="oi" data-glyph="caret-bottom">
							</span> -->
						</div>

					</div>

				</div>

				<div class="vlv vlp vhr">

					<div class="vlq">
						<span class="vcr">Содержимое страницы</span>
					</div>

					<div class="vmd">

						<div class="vho">
							<span class="vly oi" data-glyph="compass">Тип навигации</span>
							<ul class="vfj">

							</ul>
							<ul class="vfn">
								<li class="vfo">Без навигации</li>
							</ul>
						</div>

						<div class="vho">
							<span class="vly oi" data-glyph="expand-down">Вид нижней секции</span>
							<ul class="vdm">

							</ul>
							<ul class="vfk">
								<li class="vfl">Без нижней секции</li>
							</ul>
						</div>

						<div class="vem empty-vem">
						</div>

						<div class="vac">
							<span></span>
						</div>

					</div>

					<div class="vgr">
						<span class="vhq">ПОКАЗАТЬ</span>
						<span class="oi" data-glyph="trash"></span>
					</div>

					<div class="vad">


						<div class="vgt">

							<div class="vlz">
								<div class="vma">
									<span>Фильтр</span>
								</div>

								<div class="vgi">
									<div class="vgh vhr" vbp="*">
										Показать все
									</div>
								</div>
							</div>
						</div>

						<div class="vgm">
						</div>


					</div>

					<!--<div class="vls">

						<div class="vdq">
							<span class="vhq">Получить HTML</span>
						</div>

					</div>-->


				</div>

				<div class="vmb vlp">

					<div class="vlq">
						<span>Стиль страницы</span>
					</div>

					<div class="vmd vnb empty-vmd">

						<div class="vho vmu vih">
							<span class="vly oi" data-glyph="droplet">Цветовая схема</span>
							<ul class="vay">

							</ul>
						</div>

						<div class="vho vmy vih">
							<span class="vly vmz oi" data-glyph="text">Шрифты</span>
							<ul class="vms">

							</ul>
						</div>

					</div>

					<!--<div class="vls">

						<div class="vdq">
							<span class="vhq">Get HTML</span>
						</div>

					</div>-->


				</div>


				<div class="vpn vlp">

					<div class="vlq">
						<span>Метаданные страницы</span>
					</div>


					<div class="vmd">

						<div class="vlw">
							<label>Заголовок:</label>
							<input type="text" class="vei vlx">
						</div>

						<div class="vlw">
							<label>Описание страницы (SEO):</label>
							<input type="text" class="vpp">
						</div>

						<div class="vlw">
							<label>ID Google Аналитики:</label>
							<input type="text" class="vpq">
						</div>

						<div class="vlw">
							<label>ID Яндекс Метрики:</label>
							<input type="text" class="vpq">
						</div>

					</div>


					<!--<div class="vls">
						<div class="vdq">
							<span class="vhq">Get HTML</span>
						</div>
					</div>-->


				</div>

			</div>
			</div>


		</div>
    <!-- <div class="popover-content">
      <p>Чтобы посмотреть результат, перейдите по ссылке ниже:
      <a href="http://<?php echo $_SESSION['user']['username']; ?>.almatybray.tk/pmbuilder/<?php echo $_SESSION['user']['username'] . '/'. $_SESSION['user']['cur_site']; ?>" target="_blank">
        <code><?php echo $_SESSION['user']['username']; ?>.almatybray.tk/<?php echo $_SESSION['user']['cur_site']; ?></code>
      </a></p>
      <p>
        Вы также можете привязать свой домен на сайт.
        <a href="./domain.php?u=<?php echo $_SESSION['user']; ?>" target="_blank">
          Узнайте как
        </a>
      </p>
    </div> -->
		<div class="viu">

		</div>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Ваш сайт готов</h4>
          </div>
          <div class="modal-body">
            <h5>Адрес вашего сайта доступен по уникальной ссылке <a href="http://<?php echo $_SESSION['user']['username']; ?>.almatybray.tk/pmbuilder/<?php echo $_SESSION['user']['username'] . '/'. $_SESSION['user']['cur_site']; ?>" target="_blank">
              <?php echo $_SESSION['user']['username'] . '.almatybray.tk/' . $_SESSION['user']['cur_site']; ?>
            </a></h5><hr>
            <h5>Также вы можете <a href="domain.php" target="_blank">привязать</a> свой домен.</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-filled" data-dismiss="modal">Закрыть</button>
          </div>
        </div>
      </div>
    </div>
		<!-- <div class="vhm">
			<div class="vet vih vhe"><span>Загрузить последнюю страницу</span></div>
			<div class="vhd vih vhe"><span>Создать новую страницу</span></div>
		</div> -->

	</div>

	<div class="vaf vin vds">
		<div class="vfa">
			<span class="vjl vag"></span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<p class="vae">

			</p>
		</div>
		<div class="vjp">
			<div class="vhs vak vix vex">Ок</div>
		</div>

	</div>

	<div class="vfc vin vds">
			<div class="vfa">
				<span class="vjl">Сохранить пользовательскую навигацию</span>
				<i class="pm-icon pm-close-circle vex"></i>
			</div>

			<div class="vew">
				<p>
					Дайте этому блоку навигации название, чтобы в дальнейшем получить к нему доступ в разделе "Стили навигации"
				</p>
				<div class="vej">
					<i class="oi" data-glyph="compass"></i>
					<input type="text" placeholder="Название навигации" class="vgb vez">
				</div>
			</div>

			<div class="vjp">
				<div class="vhs vex val vil">Отменить</div>
				<div class="vhs vga vak vix">Сохранить</div>
			</div>

		</div>

	<div class="vfb vin vds">
		<div class="vfa">
			<span class="vjl">Сохранить пользовательскую нижнюю секцию</span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<p>
				Дайте этой нижней секции название, чтобы в дальнейшем получить к нему доступ в разделе "Стили нижней секции"
			</p>
			<div class="vej">
				<i class="oi" data-glyph="expand-down"></i>
				<input type="text" placeholder="Название нижней секции" class="vfz vez">
			</div>
		</div>

		<div class="vjp">
			<div class="vhs vex val vil">Отменить</div>
			<div class="vhs vfy vak vix">Сохранить</div>
		</div>

	</div>

	<div class="vge vin vds">
		<div class="vfa">
			<span class="vjl">Сохранить текущую страницу</span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<p>
				Дайте название и сохраните страницу для дальнейшего пользования.
			</p>
			<div class="vej">
				<i class="oi" data-glyph="file"></i>
				<input type="text" placeholder="Имя страницы" class="vgf vez">
			</div>
		</div>

		<div class="vjp">
			<div class="vhs vex val vil">Отменить</div>
			<div class="vhs vgd vak vix">Сохранить</div>
		</div>

	</div>

	<div class="vcw vin">
		<div class="vfa">
			<span class="vjl">Редактировать Ссылку </span><span class="vcq"></span>
		</div>

		<div class="vew">
			<form name="vcw">
				<div class="vej vgp">
					<span>Внутренняя ссылка:</span>
					<select class="veh vja" autocomplete="off">
						<option value="">Выбрать</option>
					</select>
				</div>

				<div class="vej">
					<span>Редактировать href ссылки:</span>
					<span class="oi" data-glyph="link-intact"></span>
					<input type="text" class="vcu vez">
					<input type="text" class="vcv vez vih">
				</div>
				<div class="vej">
					<span>Открыть в:</span>
					<span class="oi" data-glyph="browser"></span>
					<select class="vcy vja" name="vcy" autocomplete="off">
						<option class="vcz edit-link-target-_blank-mrv" value="_blank">Новой Вкладке</option>
						<option class="vcz edit-link-target-_self-mrv" value="_self" selected="selected">Этой Вкладке</option>
					</select>
				</div>
			<form>
		</form></form></div>

		<div class="vjp">
			<div class="vhs vex val vil">Отменить</div>
			<div class="vhs vcx vak vix">Сохранить</div>
		</div>

	</div>

	<div class="vcl vin">
		<div class="vfa">
			<span class="vjl">Редактировать изображение</span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<div class="vhs vhp vaj vil"><span class="oi" data-glyph="plus"></span></div>
			<div class="vea">
				<div class="vey">
					<div class="vej">
						<span>Редактировать ссылку:</span>
						<span class="oi" data-glyph="image"></span>
						<input type="text" placeholder="Значение 'src'" class="vco vez">
					</div>

					<div class="vej">
						<span>Редактировать альтернативный текст:</span>
						<span class="oi" data-glyph="tags"></span>
						<input type="text" placeholder="Значение 'alt'" class="vcf vez">
					</div>
          <div class="vjp">
          </div>
					<input class="vck vih" type="text">
				</div>

				<div class="vey">
					<div class="vcm">
						<img alt="Редактировать изображение" src="" class="vcp">
						<span class="veb">1280x800</span>
					</div>
				</div>

				<div class="vjp">
					<div class="vhs vex val vil">Отменить</div>
					<div class="vhs vcn vak vix">Сохранить</div>
				</div>
			</div>


		</div>
<!-- app1.jpg,app1.png,app2.jpg,app3.jpg,app4.jpg,arch1.jpg,arch1.png,arch2.jpg,arch3.jpg,arch4.jpg,arch5.jpg,arch6.jpg,avatar1.png,avatar2.png,avatar3.png,avatar4.png,avatar5.png,avatar6.png,blog-single-2.jpg,blog-single-3.jpg,blog-single-4.jpg,blog-single.jpg,c1.png,c2.png,c3.png,c4.png,capital-t-1.jpg,capital-t-2.jpg,capital-t-3.jpg,capital-t-4.jpg,capital-t-5.jpg,capital-t-6.jpg,capital-t-7.jpg,capital-t-8.jpg,capital2.jpg,capital3.jpg,capital4.jpg,capital5.jpg,cover1.jpg,cover2.jpg,cover5.jpg,cover6.jpg,cover7.jpg,cover8.jpg,cover9.jpg,cover10.jpg,cover11.jpg,cover12.jpg,cover13.jpg,cover14.jpg,cover15.jpg,cover16.jpg,event1.jpg,hero1.jpg,home-2-1.jpg,home-2-1.png,home-2-2.jpg,home-2-3.jpg,home-2-4.jpg,home-2-5.jpg,home-2-6.jpg,home-2-7.jpg,home-2-8.jpg,home-2-9.jpg,home-4-1.jpg,home1.jpg,home2.jpg,home3.jpg,home4.jpg,home5.jpg,home6.jpg,home7.jpg,home8.jpg,home10.jpg,home11.jpg,home12.jpg,home13.jpg,home14.jpg,home15.jpg,home16.jpg,home17.jpg,home18.jpg,home19.jpg,home20.jpg,home21.jpg,home22.jpg,home23.jpg,intro1.jpg,l1.png,l2.png,l3.png,l4.png,logo-dark.png,logo-light.png,logo-p-dark.png,music1.jpg,music2.jpg,nav-dark.jpg,nav-info.jpg,nav-light.jpg,nav-trans.jpg,page-coming-soon.jpg,page-register.jpg,phone1.png,phone2.png,phot1.jpg,phot2.jpg,phot3.jpg,project-case-study-1.jpg,project-case-study-2.jpg,project-case-study-3.jpg,project-case-study-4.jpg,project-single-1.jpg,project-single-2.jpg,project-single-3.jpg,project-single-4.jpg,project-single-5.jpg,project-single-6.jpg,prop1.jpg,prop2.jpg,prop3.jpg,prop4.jpg,prop5.jpg,prop6.jpg,resto1.jpg,resto2.jpg,resto3.jpg,resto4.jpg,resto5.jpg,resto6.jpg,screenshot.jpg,screenshot2.jpg,shop-product-1.jpg,shop-product-2.jpg,shop-product-3.jpg,shop-product-4_1.jpg,shop-product-4_2.jpg,shop-product-4.jpg,shop-product-5.jpg,shop-product-6.jpg,shop-product-7.jpg,shop-product-8.jpg,shop-product-9.jpg,shop-product-10.jpg,shop-product-11.jpg,shop-product-12.jpg,shop-product-13.jpg,shop-widget-1.png,shop-widget-2.png,signature.png,small1.jpg,small2.jpg,stars.png,team-1.jpg,team-2.jpg,team-3.jpg,pm-logo.png,vent1.jpg,vent1.png,vent2.jpg,vent2.png,vent3.jpg,vent4.jpg,vent5.jpg,wide-1.jpg -->
		<div class="vdz" vbv="">
			<input type="file" class="vch" name="vci[]" form>
			<div class="vdx">
			</div>
      <div class="vhs vee">Импортировать изображение</div>
		</div>


	</div>

	<div class="vde vin vdo">
		<div class="vfa">
			<span class="vjl">Редактировать источник видео:</span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<div class="vej">
				<span>Редактировать источник MP4:</span>
				<span class="oi" data-glyph="video"></span>
				<input type="text" class="vda vez">
			</div>

			<div class="vej">
				<span>Редактировать источник WEBM:</span>
				<span class="oi" data-glyph="video"></span>
				<input type="text" class="vdg vez">
			</div>

			<div class="vej">
				<span>Редактировать источник OGV:</span>
				<span class="oi" data-glyph="video"></span>
				<input type="text" class="vdb vez">
				<input class="vih vdd" type="text">

			</div>
		</div>
		<div class="vjp">
			<div class="vhs vex val vil">Отменить</div>
			<div class="vhs vdf vak vix">Сохранить</div>
		</div>

	</div>

	<div class="vph vin vdo">
		<div class="vfa">
			<span class="vjl">Редактировать форму:</span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<div class="vej">
				<span>Код Вставки Формы:</span>
				<textarea class="vpi" placeholder="Код вставки с Mailchimp"></textarea>
				<input class="vih vpj" type="text">
			</div>
		</div>
		<div class="vjp">
			<div class="vhs vex val vil">Отменить</div>
			<div class="vhs vpk vak vix">Сохранить</div>
		</div>

	</div>


	<div class="vcd vin">
		<div class="vfa">
			<span class="vjl">Редактировать значок</span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<div class="vox">
				<span class="oi" data-glyph="magnifying-glass"></span>
				<input type="text" class="vok vez" placeholder="Фильтр значков">
			</div>
			<a href="#" class="vhs vol vih">очистить</a>
			<div class="vce">


			</div>
		</div>

		<div class="vjp">
			<input id="vgo" class="vih" type="text">
			<div class="vhs vex vcc vak vix">Готово</div>
		</div>

	</div>

	<div class="vdr vin">
		<div class="vfa">
			<span class="vjl">Исходный код в HTML</span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew">
			<div class="vft">
				<textarea class="vfu" cols="256" readonly="readonly" wrap="soft">
				</textarea>
			</div>
		</div>

		<div class="vjp">
			<div class="vhs vgn vaj vil">Выделить все</div>
			<div class="vhs vgy vaj vil">Скачать HTML файл</div>
			<div class="vhs vex vak vix">Готово</div>
		</div>

	</div>

	<div class="global-vfr vin vkf">
		<img alt="Logo" src="../img/post.png">
		<div class="vew">
			<div class="vhs vap val">Очистить навигации</div>
			<div class="vhs vao val">Очистить нижние секции</div>
			<div class="vhs vaq val">Очистить страницы</div>
			<div class="vhs vpc val">Сбросить галерею изображений</div>
			<div class="vhs van val">Удалить все</div>
			<div class="vhs val"><a href="../index.php?page=list">На главное меню</a></div>
		</div>
		<p>
			&copy; Copyright <span class="vkg"><?php echo getdate('Y'); ?></span> Automato DevTeam
		</p>
	</div>

	<div class="vbi vin vdo">
		<div class="vfa">
			<span class="vjl vbk"> </span>
			<i class="pm-icon pm-close-circle vex"></i>
		</div>

		<div class="vew vbd">
			<p class="vbh"></p>

			<div class="vej">
				<span class="vbf"> </span>
				<span class="oi vkh" data-glyph=""></span>
				<input type="text" class="vbg vez">
				<input type="text" class="vbe vih">
				<input type="text" class="vbc vih">
			</div>
		</div>

		<div class="vjp">
			<div class="vhs vex val vil">Отменить</div>
			<div class="vhs vbj vak vix">Сохранить</div>
		</div>

	</div>



	<div class="vhw vjr">
			<div class="vau">тип элемента</div>
			<div class="vki vih vaw vav vfq">Опции &gt;</div>
			<div class="vgk vih vaw vav vfq oi"></div>
			<div class="vdh vih vaw vav vfq oi"></div>
			<div class="vct vih vaw vav vfq">Редактировать Ссылку</div>
			<div class="vcg vih vaw vav vfq">Редактировать Изображение</div>
			<div class="vdc vih vaw vav vfq">Редактировать Источник Видео</div>
			<div class="vcc vih vaw vav vfq">Выбрать Значок</div>
			<div class="veg vih vaw vav vfq">Расширить Колонну</div>
			<div class="vbt vih vaw vav vfq">Сжать Колонну</div>
			<div class="vas vih vaw vav vfq">Клонировать Весь Слайд</div>
			<div class="vcb vih vaw vav vfq">Удалить Весь Слайд</div>
			<div class="nav-class-context-options"></div>
			<div class="var vaw vav">Клонировать</div>
			<div class="vbw vaw vav">Удалить</div>
			<ul class="vnc context vih">

			</ul>
		</div>

	<div id="vgg" class="vih">

<?php
include 'sections/slider.php';
include 'sections/images.php';
include 'sections/text.php';
// include 'sections/form.php';
include 'sections/aligned.php';
// include 'sections/countdown.php';
// include 'sections/cta.php';
include 'sections/icons.php';
include 'sections/tiles.php';
include 'sections/video.php';
// include 'sections/pricing.php';
include 'sections/testimonial.php';
include 'sections/maps.php';
include 'sections/other.php';
?>

<section id="pm-nav-1" class="vev" data-vjt="Стандартная полоса" vbs="nav" vbr="Main Nav">

<script class="options" type="application/json">
	{"options":[
		{
			"type":"toggle",
			"class":"bg-dark",
			"text":"Темный",
			"initial":"auto",
			"icon":"brush"
		},
		{
			"type":"toggle",
			"class":"absolute transparent",
			"text":"Прозрачный",
			"initial":"auto",
			"icon":"brush",
			"refresh":"true"
		}
	]}
</script>

<a id="top"></a>
<nav>
	<div class="nav-bar">
		<div class="module left">
			<a href="index.html">
				<img class="logo logo-light" alt="Шаблонизатор" src="../img/logo-light.png">
				<img class="logo logo-dark" alt="Шаблонизатор" src="../img/logo-dark.png">
			</a>
		</div>
		<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
			<i class="ti-menu"></i>
		</div>
		<div class="module-group right">
			<div class="module left">
				<ul class="menu vjd">
					<li>
						<a vht="parent" href="#">Одиночный</a>
					</li>
					<li class="has-dropdown">
						<a href="#" vht="parent">
							Крупное Меню
						</a>
						<ul class="mega-menu">
							<li>
								<ul>
									<li>
										<span class="title" vht="parent">Категория</span>
									</li>
									<li>
										<a href="#" vht="parent">Одиночный</a>
									</li>
								</ul>
							</li>
							<li>
								<ul>
									<li>
										<span class="title" vht="parent">Категория</span>
									</li>
									<li>
										<a href="#" vht="parent">Одиночный</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="#" vht="parent">
							Одиночное меню
						</a>
						<ul>
							<li>
								<a href="#" vht="parent">
									Второй уровень
								</a>
								<ul>
									<li>
										<a href="#" vht="parent">
											Одиночный
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="module widget-handle language left">
				<ul class="menu" vht=".module">
					<li class="has-dropdown" vht=".module">
						<a href="#" vht=".module">Виджет</a>
						<ul>
							<li>
								<a href="#" vht="parent">Ссылка</a>
							</li>
							<li>
								<a href="#" vht="parent">Ссылка</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

	</div>
</nav>
</section>

<section id="pm-nav-2" class="vev" data-vjt="Стандартная с информацией" vbs="nav" vbr="Main Nav">

<script class="options" type="application/json">
	{"options":[
		{
			"type":"toggle",
			"class":"bg-dark",
			"text":"Dark skin",
			"initial":"auto",
			"icon":"brush"
		},
		{
			"type":"toggle",
			"class":"absolute transparent",
			"text":"Transparent",
			"initial":"auto",
			"icon":"brush",
			"refresh":"true"
		}
	]}
</script>

<a id="top"></a>
<nav>
	<div class="nav-utility">
		<div class="module left">
			<i class="ti-location-arrow" vht="parent">&nbsp;</i>
			<span class="sub" vht="parent">Проспект Абая 1, Алматы, 050000</span>
		</div>
		<div class="module left">
			<i class="ti-email" vht="parent">&nbsp;</i>
			<span class="sub" vht="parent">hello@kazpost.net</span>
		</div>
	</div>
	<div class="nav-bar">
		<div class="module left">
			<a href="index.html">
				<img class="logo logo-light" alt="Шаблонизатор" src="../img/logo-light.png">
				<img class="logo logo-dark" alt="Шаблонизатор" src="../img/logo-dark.png">
			</a>
		</div>
		<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
			<i class="ti-menu"></i>
		</div>
		<div class="module-group right">
			<div class="module left">
				<ul class="menu vjd">
					<li>
						<a vht="parent" href="#">Одиночный</a>
					</li>
					<li class="has-dropdown">
						<a href="#" vht="parent">
							Крупное меню
						</a>
						<ul class="mega-menu">
							<li>
								<ul>
									<li>
										<span class="title" vht="parent">Категория</span>
									</li>
									<li>
										<a href="#" vht="parent">Одиночный</a>
									</li>
								</ul>
							</li>
							<li>
								<ul>
									<li>
										<span class="title" vht="parent">Категория</span>
									</li>
									<li>
										<a href="#" vht="parent">Одиночный</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="#" vht="parent">
							Простое меню
						</a>
						<ul>
							<li class="has-dropdown">
								<a href="#" vht="parent">
									Второй уровень
								</a>
								<ul>
									<li>
										<a href="#" vht="parent">
											Одиночный
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="module widget-handle language left">
				<ul class="menu" vht=".module">
					<li class="has-dropdown" vht=".module">
						<a vht=".module" href="#">Виджет</a>
						<ul>
							<li>
								<a href="#" vht="parent">Ссылка</a>
							</li>
							<li>
								<a href="#" vht="parent">Ссылка</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

	</div>
</nav>
</section>

<section id="pm-nav-3" class="vev" data-vjt="Центрированное лого" vbs="nav" vbr="Main Nav">

<script class="options" type="application/json">
	{"options":[
		{
			"type":"toggle",
			"class":"bg-dark",
			"text":"Dark skin",
			"initial":"auto",
			"icon":"brush"
		},
		{
			"type":"toggle",
			"class":"absolute transparent",
			"text":"Transparent",
			"initial":"auto",
			"icon":"brush",
			"refresh":"true"
		}
	]}
</script>

<nav>

	<div class="nav-bar text-center">
		<div class="col-md-2 col-md-push-5 col-sm-12 text-center">
			<a href="#">
				<img alt="logo" class="image-xxs" src="../img/logo-dark.png">
			</a>
		</div>

		<div class="col-sm-12 col-md-5 col-md-pull-2 overflow-hidden-xs">
			<ul class="menu inline-block pull-right vjd">
				<li><a vht="parent" href="#">Одиночный</a></li>
				<li><a vht="parent" href="#">Меню</a>
					<ul>
						<li><a vht="parent" href="#">Одиночный</a></li>
					</ul>
				</li>
			</ul>
		</div>

		<div class="col-sm-12 col-md-5 pb-xs-24">
			<ul class="menu vjd">
				<li><a vht="parent" href="#">Одиночный</a></li>
				<li><a vht="parent" href="#">Меню</a>
					<ul>
						<li><a vht="parent" href="#">Одиночный</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>

	<div class="module widget-handle mobile-toggle right visible-sm visible-xs absolute-xs">
		<i class="ti-menu"></i>
	</div>
</nav>
</section>

<section id="pm-nav-4" class="vev" data-vjt="Выдвигаемый справа" vbs="nav" vbr="Offscreen">

<script class="options" type="application/json">
	{"options":[
		{
			"type":"toggle",
			"class":"bg-dark",
			"text":"Dark skin",
			"initial":"auto",
			"icon":"brush"
		},
		{
			"type":"toggle",
			"class":"absolute transparent",
			"text":"Transparent",
			"initial":"auto",
			"icon":"brush",
			"refresh":"true"
		}
	]}
</script>

<nav class="absolute transparent">
		<div class="nav-bar">
			<div class="module left">
				<a href="index.html">
					<img class="logo logo-light" alt="Шаблонизатор" src="../img/logo-light.png">
					<img class="logo logo-dark" alt="Шаблонизатор" src="../img/logo-dark.png">
				</a>
			</div>
			<div class="module widget-handle offscreen-toggle right">
				<i class="ti-menu vog"></i>
			</div>
		</div>
		<div class="offscreen-container bg-dark text-center vog">
			<div class="close-nav vog vjx">
				<a href="#" class="vog">
					<i class="ti-close vog"></i>
				</a>
			</div>
			<div class="v-align-transform text-center">
				<a href="#"><img alt="Logo" class="image-xs mb40 mb-xs-24" src="../img/logo-light.png"></a>
				<ul class="mb40 mb-xs-24">
					<li class="fade-on-hover"><a href="#" vht="li"><h5 class="uppercase mb8" vht="li">Ссылка</h5></a></li>
				</ul>
				<p class="fade-half">
					Проспект Абая 222<br>
					Алматы, 050000<br>
					00000000000<br>
					hello@kazpost.net
				</p>
				<ul class="list-inline social-list">
					<li>
						<a href="#" vht="li">
							<i class="ti-twitter-alt" vht="li"></i>
						</a>
					</li>
					<li>
						<a href="#" vht="li">
							<i class="ti-facebook" vht="li"></i>
						</a>
					</li>
					<li>
						<a href="#" vht="li">
							<i class="ti-instagram" vht="li"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</section>

<section id="pm-nav-5" class="vev" data-vjt="Крупный в центре" vbs="nav" vbr="Large Centered">

<script class="options" type="application/json">
	{"options":[
		{
			"type":"toggle",
			"class":"bg-dark",
			"text":"Dark skin",
			"initial":"auto",
			"icon":"brush"
		},
		{
			"type":"toggle",
			"class":"absolute transparent",
			"text":"Transparent",
			"initial":"auto",
			"icon":"brush",
			"refresh":"true"
		}
	]}
</script>

<nav class="nav-centered">
	<div class="nav-utility">
		<div class="module left">
			<i class="ti-location-arrow">&nbsp;</i>
			<span class="sub">Проспект Абая, 222, Алматы, 050000</span>
		</div>
		<div class="module left">
			<i class="ti-email">&nbsp;</i>
			<span class="sub">hello@kazpost.net</span>
		</div>
		<!-- <div class="module right">
			<a class="btn btn-sm" href="#purchase-template">Купить сейчас</a>
		</div> -->
	</div>
	<div class="text-center">
		 <a href="index.html">
			<img class="logo logo-light" alt="Шаблонизатор" src="../img/logo-light.png">
			<img class="logo logo-dark" alt="Шаблонизатор" src="../img/logo-dark.png">
		</a>
	</div>
	<div class="nav-bar text-center">
		<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
			<i class="ti-menu"></i>
		</div>
		<div class="module-group text-left">
			<div class="module left">
				<ul class="menu vjd">
					<li>
						<a vht="parent" href="#">Одиночный</a>
					</li>
					<li class="has-dropdown">
						<a href="#" vht="parent">
							Крупное меню
						</a>
						<ul class="mega-menu">
							<li>
								<ul>
									<li>
										<span class="title" vht="parent">Категория</span>
									</li>
									<li>
										<a href="#" vht="parent">Одиночный</a>
									</li>
								</ul>
							</li>
							<li>
								<ul>
									<li>
										<span class="title" vht="parent">Категория</span>
									</li>
									<li>
										<a href="#" vht="parent">Одиночный</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="has-dropdown">
						<a href="#" vht="parent">
							Простое меню
						</a>
						<ul>
							<li class="has-dropdown">
								<a href="#" vht="parent">
									Второй уровень
								</a>
								<ul>
									<li>
										<a href="#" vht="parent">
											Одиночный
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="module widget-handle language left">
				<ul class="menu" vht=".module">
					<li class="has-dropdown" vht=".module">
						<a vht=".module" href="#">Простое меню</a>
						<ul>
							<li>
								<a href="#" vht="parent">Одиночный</a>
							</li>
							<li>
								<a href="#" vht="parent">Одиночный</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>

	</div>
	</nav>
</section>
	</div>

	<div id="vdk" class="vih">

<section id="pm-footer-1" class="vim" vbs="footer" vbp="другое" vbr="Виджеты">
<footer class="footer-1 bg-dark toggle-bg-mrv">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<img alt="Logo" class="logo" src="../img/logo-light.png">
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="widget">
					<h6 class="title">Недавние статьи</h6>
					<hr>
					<ul class="link-list recent-posts">
						<li>
                        <a href="#">Самая последняя новость</a>
                        <span class="date">Декабрь
                            <span class="number">5, 2016</span>
                        </span>
                    </li>
                    <li>
                        <a href="#">Самая предпоследняя новость</a>
                        <span class="date">Декабрь
                            <span class="number">1, 2016</span>
                        </span>
                    </li>
                    <li>
                        <a href="#">Самая пред-предпоследняя новость</a>
                        <span class="date">Ноябрь
                            <span class="number">27, 2016</span>
                        </span>
                    </li>
					</ul>
				</div>

			</div>
			<div class="col-md-3 col-sm-6">
				<div class="widget">
					<h6 class="title">Блог</h6>
					<hr>
					<ul class="link-list recent-posts">
						<li>
												<a href="#">Самая последняя новость</a>
												<span class="date">Декабрь
														<span class="number">5, 2016</span>
												</span>
										</li>
										<li>
												<a href="#">Самая предпоследняя новость</a>
												<span class="date">Декабрь
														<span class="number">1, 2016</span>
												</span>
										</li>
										<li>
												<a href="#">Самая пред-предпоследняя новость</a>
												<span class="date">Ноябрь
														<span class="number">27, 2016</span>
												</span>
										</li>
					</ul>
				</div>

			</div>
			<!-- <div class="col-md-3 col-sm-6">
				<div class="widget">
					<h6 class="title">Последние обновления</h6>
					<hr>
					<div class="twitter-feed">
						<div class="tweets-feed" data-widget-id="492085717044981760">
						</div>
					</div>
				</div>

			</div> -->
			<!-- <div class="col-md-3 col-sm-6">
				<div class="widget">
					<h6 class="title">Инстаграм</h6>
					<hr>
					<div class="instafeed" data-user-name="misterybray">
						<ul></ul>
					</div>
				</div>

			</div> -->
		</div>

		<div class="row">
			<div class="col-sm-6">
				<span class="sub">&copy; Copyright 2016 - AО "Казпочта"</span>
			</div>
			<div class="col-sm-6 text-right">
				<ul class="list-inline social-list">
					<li>
						<a href="#">
							<i vht="li" class="ti-twitter-alt"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i vht="li" class="ti-facebook"></i>
						</a>
					</li>
					<li>
						<a href="#">
							<i vht="li" class="ti-instagram"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Наверх</a>
</footer>
</section>

<section id="pm-footer-2" class="vim" vbs="footer" vbp="другое" vbr="Тонкая секция">
<footer class="footer-2 bg-dark toggle-bg-mrv text-center-xs">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<a href="#"><img class="image-xxs fade-half" alt="Pic" src="../img/logo-light.png"></a>
			</div>

			<div class="col-sm-4 text-center">
				<span class="fade-half">
					&copy; Copyright 2016 - AО "Казпочта"
				</span>
			</div>

			<div class="col-sm-4 text-right text-center-xs">
				<ul class="list-inline social-list">
					<li><a href="#"><i vht="li" class="ti-twitter-alt"></i></a></li>
					<li><a href="#"><i vht="li" class="ti-facebook"></i></a></li>
					<li><a href="#"><i vht="li" class="ti-instagram"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
</section>

<section id="pm-footer-3" class="vim" vbs="footer" vbp="другое" vbr="Центрированная">
<footer class="footer-2 bg-primary">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center mb64 mb-xs-24">
				<a href="#">
					<img alt="Logo" class="image-xs mb16" src="../img/logo-light.png">
				</a>
				<p class="lead mb48 mb-xs-16">
					Проспект Абая, 222<br>
					Алматы<br>
					(03) 83726 4782
				</p>
				<ul class="list-inline social-list spread-children">
					<li><a href="#"><i vht="li" class="icon icon-sm ti-twitter-alt"></i></a></li>
					<li><a href="#"><i vht="li" class="icon icon-sm ti-facebook"></i></a></li>
					<li><a href="#"><i vht="li" class="icon icon-sm ti-instagram"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="row fade-half">
			<div class="col-sm-4 text-center-xs">
				<span>&copy; Copyright 2016 - AО "Казпочта"</span>
			</div>

			<div class="col-sm-4 text-center hidden-xs">
				<span>Веб дизайн & разработка</span>
			</div>

			<div class="col-sm-4 text-right hidden-xs">
				<span>hello@kazpost.co</span>
			</div>
		</div>
	</div>
</footer>
</section>

<section id="pm-footer-4" class="vim" vbs="footer" vbp="другое" vbr="Меню">
<footer class="footer-2 bg-dark toggle-bg-mrv pt96 pt-xs-40">
	<div class="container">
		<div class="row mb64 mb-xs-24">
			<div class="col-sm-12">
				<a href="#">
					<img alt="logo" class="image-xxs" src="../img/logo-light.png">
				</a>
			</div>
		</div>
		<div class="row mb64 mb-xs-24">
			<div class="col-md-3 col-sm-4">
				<ul>
					<li><a vht="parent" href="#"><h5 class="uppercase mb16 fade-on-hover">Главная</h5></a></li>
					<li><a vht="parent" href="#"><h5 class="uppercase mb16 fade-on-hover">О нас</h5></a></li>
					<li><a vht="parent" href="#"><h5 class="uppercase mb16 fade-on-hover">Особенности</h5></a></li>
					<li><a vht="parent" href="#"><h5 class="uppercase mb16 fade-on-hover">Цены</h5></a></li>
				</ul>
			</div>

			<div class="col-md-3 col-sm-4">
				<ul>
					<li><a vht="parent" href="#"><h5 class="uppercase mb16 fade-on-hover">Пресса</h5></a></li>
					<li><a vht="parent" href="#"><h5 class="uppercase mb16 fade-on-hover">Вопросы и ответы</h5></a></li>
					<li><a vht="parent" href="#"><h5 class="uppercase mb16 fade-on-hover">Контакты</h5></a></li>
				</ul>
			</div>

			<div class="col-md-4 col-md-offset-2 col-sm-4">
				<p class="lead">Подписывайтесь</p>
				<ul class="list-inline social-list">
					<li><a href="#"><i vht="li" class="ti-twitter-alt"></i></a></li>
					<li><a href="#"><i vht="li" class="ti-facebook"></i></a></li>
					<li><a href="#"><i vht="li" class="ti-instagram"></i></a></li>
				</ul>
			</div>
		</div>

		<div class="row fade-half">
			<div class="col-sm-12 text-center">
				<span>&copy; Copyright 2016 - AО "Казпочта"</span>
			</div>
		</div>
	</div>
</footer>
</section>

<section id="pm-footer-5" class="vim" vbs="footer" vbp="другое" vbr="Крупный текст">
<footer class="bg-primary pt96 pb96 pt-xs-48 pb-xs-48">
	<div class="container">
		<div class="row mb64 mb-xs-32">
			<div class="col-md-5 text-right text-left-xs">
				<h2 class="uppercase bold italic">Lorem ipsum dolor sit amet</h2>
			</div>
			<div class="col-md-2 hidden-sm hidden-xs text-center pt48">
				<i class="icon icon-lg ti-bolt display-block"></i>
			</div>
			<div class="col-md-5 col-sm-6">
				<p class="lead">
					Lorem ipsum dolor sit amet<br class="hidden-sm"> Lorem ipsum dolor sit amet
				</p>
				<h4 class="mb8">P: 0417 374 992</h4>
				<h4 class="mb8">E: train@kazpost.net</h4>
				<h4 class="mb0"><i class="ti-twitter-alt"></i></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
				<p class="fade-1-4">Lorem ipsum dolor sit amet<br> Lorem ipsum dolor sit amet
				<ul class="list-inline social-list mb0">
					<li><a vht="li" href="#"><i vht="li" class="icon icon-xs ti-twitter-alt"></i></a></li>
					<li><a vht="li" href="#"><i vht="li" class="icon icon-xs ti-facebook"></i></a></li>
					<li><a vht="li" href="#"><i vht="li" class="icon icon-xs ti-instagram"></i></a></li>
				</ul>
			</p></div>
		</div>
	</div>
</footer>
</section>
	</div>


	<div id="vdt" class="vih">
		<pre>
			<code id="vji" data-name="Original">
&lt;!doctype html&gt;
&lt;html lang="en"&gt;
&lt;head&gt;
	&lt;meta charset="utf-8"&gt;
	&lt;title&gt;[title]&lt;/title&gt;
	&lt;meta name="viewport" content="width=device-width, initial-scale=1.0"&gt;
	&lt;link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" /&gt;
	&lt;link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" /&gt;
	&lt;link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" /&gt;
	&lt;link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" /&gt;
	&lt;link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" /&gt;
	&lt;link href="css/theme.css" rel="stylesheet" type="text/css" media="all" /&gt;
	&lt;link href="css/custom.css" rel="stylesheet" type="text/css" media="all" /&gt;
	&lt;link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,600,700%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'&gt;
&lt;/head&gt;
&lt;body [optional-body-classes]&gt;
			</code>
		</pre>
	</div>


	<div id="vfv" class="vih">
		<pre>
			<code id="vif" name="Original">
&lt;script src="js/jquery.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/bootstrap.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/flexslider.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/lightbox.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/masonry.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/flickr.js"&gt;&lt;/script&gt;
	&lt;script src="js/twitterfetcher.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/spectragram.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/ytplayer.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/countdown.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/smooth-scroll.min.js"&gt;&lt;/script&gt;
	&lt;script src="js/parallax.js"&gt;&lt;/script&gt;
	&lt;script src="js/scripts.js"&gt;&lt;/script&gt;
&lt;/body&gt;
&lt;/html&gt;
			</code>
		</pre>
	</div>

	<div id="vbn" class="vih"></div>

	<div id="vbl" class="vih"></div>

	<ul id="vhl" class="vih"></ul>

	<div id="vdp" class="vih"></div>

	<script class="vaz" type="application/json">
		{
			"original":
			{
				"name":"Шаблонизатор",
				"pathToOriginal":"mixture/public/less/",
				"originalFileName":"theme",
				"colours":["#47b475"]
			},
			"schemes":
			[
				{
					"name":"Nature",
					"colours":["#91A05C"]
				},
				{
					"name":"Nearblack",
					"colours":["#13181D"]
				},
				{
					"name":"Navy",
					"colours":["#282a38"]
				},
				{
					"name":"Gunmetal",
					"colours":["#333347"]
				},
				{
					"name":"Startup",
					"colours":["#5edcd0"]
				},
				{
					"name":"Hyperblue",
					"colours":["#3b3be8"]
				},
				{
					"name":"Purple",
					"colours":["#9037ff"]
				},
				{
					"name":"Red",
					"colours":["#e31d3b"]
				},
				{
					"name":"Fire",
					"colours":["#fc4f4f"]
				},
				{
					"name":"Orange",
					"colours":["#e5692c"]
				},
				{
					"name":"Chipotle",
					"colours":["#f9ad19"]
				},
				{
					"name":"OffYellow",
					"colours":["#e3d820"]
				}

			]
		}
	</script>

	<script class="vmq" type="application/json">
		{
			"title":"Типография",
			"originalSet":{
				"setName":"Raleway",
				"fonts":[
					{
						"fontName":"Raleway"
					}
				],
				"css":[

					]
			},
			"optionalSets":[
				{
					"setName":"Oswald",
					"fonts":[
						{
							"fontName":"Oswald"
						}
					],
					"css":[
							"https://fonts.googleapis.com/css?family=Oswald:100,400,700"
						]
				},
				{
					"setName":"Roboto",
					"fonts":[
						{
							"fontName":"Roboto"
						}
					],
					"css":[
							"http://fonts.googleapis.com/css?family=Roboto:100,300,400,600,700"
						]
				},
				{
					"setName":"RobotoCondensed",
					"fonts":[
						{
							"fontName":"RobotoCondensed"
						}
					],
					"css":[
							"http://fonts.googleapis.com/css?family=Roboto+Condensed:100,300,400,700,700italic"
						]
				},
				{
					"setName":"OpenSans",
					"fonts":[
						{
							"fontName":"Opensans"
						}
					],
					"css":[
							"https://fonts.googleapis.com/css?family=Open+Sans:300,400,700"
						]
				},
				{
					"setName":"Titillium",
					"fonts":[
						{
							"fontName":"Titillium"
						}
					],
					"css":[
							"http://fonts.googleapis.com/css?family=Titillium+Web:100,300,400,600,700"
						]
				},
				{
					"setName":"Montserrat",
					"fonts":[
						{
							"fontName":"Montserrat"
						}
					],
					"css":[
							"http://fonts.googleapis.com/css?family=Montserrat:100,300,400,600,700"
						]
				},
				{
					"setName":"Dosis",
					"fonts":[
						{
							"fontName":"Dosis"
						}
					],
					"css":[
							"http://fonts.googleapis.com/css?family=Dosis:100,300,400,600,700"
						]
				},
				{
					"setName":"Poppins",
					"fonts":[
						{
							"fontName":"Poppins"
						}
					],
					"css":[
							"http://fonts.googleapis.com/css?family=Poppins:100,300,400,600,700"
						]
				}
			]
		}

	</script>

	<script id="vkk" type="application/json">
		{
			"options":[
				{
					"type":"toggle",
					"class":"boxed-layout",
					"text":"Коробочный макет",
					"initial":"off",
					"icon":"resize-width"
				},
				{
					"type":"toggle",
					"class":"btn-rounded",
					"text":"Округленные кнопки",
					"initial":"off",
					"icon":"brush"
				},
				{
					"type":"toggle",
					"class":"scroll-assist",
					"text":"Сглаживание прокрутки",
					"initial":"off",
					"icon":"resize-height"
				}
			]
		}
	</script>

	<script id="voy" type="application/json">
		{
			"options":[
				{
					"type":"toggle",
					"selector":"section",
					"menu":"utility",
					"submenu":"Управление разделами",
					"class":"hidden-xs",
					"text":"Скрыть в мобильном",
					"initial":"auto",
					"icon":"phone"
				},
				{
					"type":"toggle",
					"disposableSelector":".toggle-bg-mrv",
					"menu":"utility",
					"submenu":"Управление разделами",
					"class":"bg-dark",
					"text":"Темный фон",
					"initial":"auto",
					"icon":"contrast"
				},
				{
					"type":"toggle",
					"disposableSelector":".toggle-bg-mrv",
					"menu":"utility",
					"submenu":"Управление разделами",
					"class":"bg-secondary",
					"text":"Смещение фона",
					"initial":"auto",
					"icon":"contrast"
				},
				{
					"type":"toggle",
					"selector":"h1,h2,h3,h4,h5,h6",
					"menu":"context",
					"class":"uppercase",
					"text":"Заглавный",
					"initial":"auto",
					"icon":"text"
				},
				{
					"type":"toggle",
					"selector":"h1,h2,h3,h4,h5,h6",
					"menu":"context",
					"class":"bold",
					"text":"Жирный",
					"initial":"auto",
					"icon":"bold"
				},
				{
					"type":"toggle",
					"selector":"h1",
					"menu":"context",
					"class":"large",
					"text":"Увеличить",
					"initial":"auto",
					"icon":"text"
				},
				{
					"type":"toggle",
					"selector":".feature",
					"menu":"context",
					"class":"boxed",
					"text":"Коробочный",
					"initial":"auto",
					"icon":"brush"
				},
				{
					"type":"toggle",
					"selector":".feature",
					"menu":"context",
					"class":"bordered",
					"text":"Границы",
					"initial":"auto",
					"icon":"brush"
				},
				{
					"type":"toggle",
					"selector":".pricing-table",
					"menu":"context",
					"class":"boxed",
					"text":"Коробочный",
					"initial":"auto",
					"icon":"brush"
				},
				{
					"type":"toggle",
					"selector":".pricing-table",
					"menu":"context",
					"class":"emphasis",
					"text":"Выделить",
					"initial":"auto",
					"icon":"brush"
				},
				{
					"type":"toggle",
					"selector":".image-bg, .cover, section.image-slider",
					"menu":"utility",
					"class":"parallax",
					"text":"Параллакс",
					"initial":"auto",
					"icon":"data-transfer-upload"
				},
				{
					"type":"toggle",
					"selector":".row, div[class*='col-']",
					"menu":"context",
					"class":"text-center",
					"text":"Центрировать Текст",
					"initial":"auto",
					"icon":"align-center"
				},
				{
					"type":"toggle",
					"selector":".btn",
					"menu":"context",
					"class":"btn-filled",
					"text":"Залить кнопку",
					"initial":"auto",
					"icon":"brush"
				}
			]
		}
	</script>

	<script id="vnd" type="application/json">
		{
			"options":[
				{
					"buttonText":"Редактировать заполнитель",
					"modalTitle":"Редактировать заполнитель",
					"modalIntro":"Редактировать заполнитель для этого input. Заполнитель появляется перед началом набора текста.",
					"modalInputLabel":"Текст заполнителя:",
					"selector":"input[type='text'],textarea",
					"attribute":"placeholder",
					"modalInputIcon":"text"
				},
				{
					"buttonText":"Edit Value",
					"modalTitle":"Edit Input Value",
					"modalIntro":"Edit the value for this input. The value is the text that appears on the submit button.",
					"modalInputLabel":"Button Text:",
					"selector":"input[type='submit']",
					"attribute":"value",
					"modalInputIcon":"text"
				},
				{
					"buttonText":"Edit Feed",
					"modalTitle":"Edit Instagram Feed",
					"modalIntro":"Edit the Instagram feed by placing your desired username here.",
					"modalInputLabel":"Username:",
					"selector":".instafeed",
					"attribute":"data-user-name",
					"modalInputIcon":"instagram",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Flickr User ID",
					"modalTitle":"Edit Flickr User ID",
					"modalIntro":"Specify Flickr User ID for this album.",
					"modalInputLabel":"User ID:",
					"selector":"ul.flickr-feed",
					"attribute":"data-user-id",
					"modalInputIcon":"person",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Flickr Album",
					"modalTitle":"Edit Flickr Album",
					"modalIntro":"Specify Flickr Album ID.",
					"modalInputLabel":"Album ID:",
					"selector":"ul.flickr-feed",
					"attribute":"data-album-id",
					"modalInputIcon":"grid-three-up",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Twitter Widget",
					"modalTitle":"Edit Twitter Widget",
					"modalIntro":"Place your Twitter Widget ID here *See template docs for a detailed explanation on how to obtain your widget ID.",
					"modalInputLabel":"Widget ID:",
					"selector":".tweets-feed",
					"attribute":"data-widget-id",
					"modalInputIcon":"twitter",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Редактировать карту",
					"modalTitle":"Редактировать карту",
					"modalIntro":"Скопируйте код вставки карты и вставьте сюда.",
					"modalInputLabel":"Код вставки:",
					"selector":".map-holder iframe",
					"attribute":"no-src",
					"modalInputIcon":"map-marker",
					"sectionControl":"true"
				},
				{
					"buttonText":"Edit Media Embed",
					"modalTitle":"Edit Media Embed",
					"modalIntro":"Copy the iframe embed code from your media provider and paste it here.",
					"modalInputLabel":"Embed Code:",
					"selector":".embed-video-container > iframe",
					"attribute":"no-src",
					"modalInputIcon":"video",
					"sectionControl":"true"
				},
				{
					"buttonText":"Редактировать рассылку",
					"modalTitle":"Редактировать рассылку",
					"modalIntro":"Скопируйте код формы с Mailchimp или Campaign Monitor. Используйте классическую форму в Mailchimp. Используйте стандартную форму в Campaign Monitor. Оставьте пустым для поведения формы по умолчанию",
					"modalInputLabel":"Код вставки формы:",
					"selector":"form.form-newsletter iframe.mail-list-form",
					"attribute":"srcdoc",
					"modalInputIcon":"envelope-closed",
					"sectionControl":"true"
				},
				{
					"buttonText":"Редактировать сообщение успешной отправки",
					"modalTitle":"Редактировать сообщение успешной отправки",
					"modalIntro":"Укажите сообщение, которое появится если форма отправлена успешно.",
					"modalInputLabel":"Сообщение:",
					"selector":"form[data-success]",
					"attribute":"data-success",
					"modalInputIcon":"circle-check",
					"sectionControl":"true"
				},
				{
					"buttonText":"Редактировать сообщение об ошибке",
					"modalTitle":"Редактировать сообщение об ошибке",
					"modalIntro":"Укажите сообщение об ошибке, которое будет появляться если форма не прошла проверку.",
					"modalInputLabel":"Сообщение об ошибке:",
					"selector":"form[data-error]",
					"attribute":"data-error",
					"modalInputIcon":"circle-x",
					"sectionControl":"true"
				},
				{
					"buttonText":"Редактировать адрес перенаправления формы",
					"modalTitle":"Редактировать адрес перенаправления формы",
					"modalIntro":"Адрес перенаправления после успешной отправки формы. Оставьте пустым, чтобы не перенаправлять",
					"modalInputLabel":"Адрес перенаправления:",
					"selector":"form.form-email, form.form-newsletter",
					"attribute":"success-redirect",
					"modalInputIcon":"external-link",
					"sectionControl":"true"
				},
				{
					"buttonText":"Редактировать дату отсчета",
					"modalTitle":"Редактировать дату отсчета",
					"modalIntro":"Дата отсчета в формате ГГГГ/ДД/MM .",
					"modalInputLabel":"Дата:",
					"selector":".countdown",
					"attribute":"data-date",
					"modalInputIcon":"clock",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Редактировать значение индикатора",
					"modalTitle":"Редактировать значение индикатора",
					"modalIntro":"Change the value of the progress bar (out of 100)",
					"modalInputLabel":"Value:",
					"selector":".progress-bar",
					"attribute":"data-progress",
					"modalInputIcon":"dial",
					"sectionControl":"false",
					"refresh":"true"
				},
				{
					"buttonText":"Редактировать категорию проекта",
					"modalTitle":"Редактировать категорию проекта",
					"modalIntro":"Измените категорию этого проекта",
					"modalInputLabel":"Категория:",
					"selector":".project",
					"attribute":"data-filter",
					"modalInputIcon":"tags",
					"sectionControl":"false",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Gallery Title",
					"modalTitle":"Edit Gallery Title",
					"modalIntro":"Title for Lightbox Gallery",
					"modalInputLabel":"Gallery Title:",
					"selector":".lightbox-grid",
					"attribute":"data-gallery-title",
					"modalInputIcon":"tag",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Disqus Shortname",
					"modalTitle":"Edit Disqus Shortname",
					"modalIntro":"Shortname for Disqus Comments. Retrieve your site's shortname from the 'Settings' area in your Disqus dashboard. See the template documentation for greater detail.",
					"modalInputLabel":"Shortname:",
					"selector":".disqus-comments",
					"attribute":"data-shortname",
					"modalInputIcon":"tag",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Google Maps API Key",
					"modalTitle":"Google Maps API Key",
					"modalIntro":"Get your Maps API key from Google and paste it here to allow styled maps in your page.",
					"modalInputLabel":"API Key:",
					"selector":"div.map-canvas",
					"attribute":"data-maps-api-key",
					"modalInputIcon":"key",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Map Address",
					"modalTitle":"Map Address",
					"modalIntro":"Provide a specific address for Google Maps to center the map.",
					"modalInputLabel":"Full Address:",
					"selector":"div.map-canvas",
					"attribute":"data-address",
					"modalInputIcon":"map-marker",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Map Style",
					"modalTitle":"Map Style",
					"modalIntro":"Copy JSON style code from https://snazzymaps.com/ or http://goo.gl/GJEDaO and paste below.",
					"modalInputLabel":"Maps Style JSON:",
					"selector":"div.map-canvas",
					"attribute":"data-map-style",
					"modalInputIcon":"brush",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Edit Map Zoom Level",
					"modalTitle":"Map Zoom Level",
					"modalIntro":"Provide a zoom level (number) between 1 and 18 where 18 is closer and 1 is further away.",
					"modalInputLabel":"Zoom level:",
					"selector":"div.map-canvas",
					"attribute":"data-map-zoom",
					"modalInputIcon":"zoom-in",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Редактировать поставщика видео",
					"modalTitle":"Редактировать поставщика видео",
					"modalIntro":"Укажите 'youtube' или 'vimeo' как поставщик вашего медиа",
					"modalInputLabel":"Поставщик:",
					"selector":"iframe[data-provider]",
					"attribute":"data-provider",
					"modalInputIcon":"media-play",
					"sectionControl":"true",
					"refresh":"true"
				},
				{
					"buttonText":"Редактировать ID видео",
					"modalTitle":"Редактировать ID видео",
					"modalIntro":"Укажите ID видео полученную с URL",
					"modalInputLabel":"ID видео:",
					"selector":"iframe[data-video-id]",
					"attribute":"data-video-id",
					"modalInputIcon":"video",
					"sectionControl":"true",
					"refresh":"true"
				}
			]
		}
	</script>

	<script id="voi" type="application/json">
		{
			"iconPacks":[
				{
					"name":"Themify",
					"url":"http://goo.gl/I70F4d",
					"headString":"&lt;link href=&quot;css/themify-icons.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot; media=&quot;all&quot; /&gt;",
					"iconPrefix":"ti-",
					"iconClass":"ti",
					"icons":["ti-wand","ti-volume","ti-user","ti-unlock","ti-unlink","ti-trash","ti-thought","ti-target","ti-tag","ti-tablet","ti-star","ti-spray","ti-signal","ti-shopping-cart","ti-shopping-cart-full","ti-settings","ti-search","ti-zoom-in","ti-zoom-out","ti-cut","ti-ruler","ti-ruler-pencil","ti-ruler-alt","ti-bookmark","ti-bookmark-alt","ti-reload","ti-plus","ti-pin","ti-pencil","ti-pencil-alt","ti-paint-roller","ti-paint-bucket","ti-na","ti-mobile","ti-minus","ti-medall","ti-medall-alt","ti-marker","ti-marker-alt","ti-arrow-up","ti-arrow-right","ti-arrow-left","ti-arrow-down","ti-lock","ti-location-arrow","ti-link","ti-layout","ti-layers","ti-layers-alt","ti-key","ti-import","ti-image","ti-heart","ti-heart-broken","ti-hand-stop","ti-hand-open","ti-hand-drag","ti-folder","ti-flag","ti-flag-alt","ti-flag-alt-2","ti-eye","ti-export","ti-exchange-vertical","ti-desktop","ti-cup","ti-crown","ti-comments","ti-comment","ti-comment-alt","ti-close","ti-clip","ti-angle-up","ti-angle-right","ti-angle-left","ti-angle-down","ti-check","ti-check-box","ti-camera","ti-announcement","ti-brush","ti-briefcase","ti-bolt","ti-bolt-alt","ti-blackboard","ti-bag","ti-move","ti-arrows-vertical","ti-arrows-horizontal","ti-fullscreen","ti-arrow-top-right","ti-arrow-top-left","ti-arrow-circle-up","ti-arrow-circle-right","ti-arrow-circle-left","ti-arrow-circle-down","ti-angle-double-up","ti-angle-double-right","ti-angle-double-left","ti-angle-double-down","ti-zip","ti-world","ti-wheelchair","ti-view-list","ti-view-list-alt","ti-view-grid","ti-uppercase","ti-upload","ti-underline","ti-truck","ti-timer","ti-ticket","ti-thumb-up","ti-thumb-down","ti-text","ti-stats-up","ti-stats-down","ti-split-v","ti-split-h","ti-smallcap","ti-shine","ti-shift-right","ti-shift-left","ti-shield","ti-notepad","ti-server","ti-quote-right","ti-quote-left","ti-pulse","ti-printer","ti-power-off","ti-plug","ti-pie-chart","ti-paragraph","ti-panel","ti-package","ti-music","ti-music-alt","ti-mouse","ti-mouse-alt","ti-money","ti-microphone","ti-menu","ti-menu-alt","ti-map","ti-map-alt","ti-loop","ti-location-pin","ti-list","ti-light-bulb","ti-Italic","ti-info","ti-infinite","ti-id-badge","ti-hummer","ti-home","ti-help","ti-headphone","ti-harddrives","ti-harddrive","ti-gift","ti-game","ti-filter","ti-files","ti-file","ti-eraser","ti-envelope","ti-download","ti-direction","ti-direction-alt","ti-dashboard","ti-control-stop","ti-control-shuffle","ti-control-play","ti-control-pause","ti-control-forward","ti-control-backward","ti-cloud","ti-cloud-up","ti-cloud-down","ti-clipboard","ti-car","ti-calendar","ti-book","ti-bell","ti-basketball","ti-bar-chart","ti-bar-chart-alt","ti-back-right","ti-back-left","ti-arrows-corner","ti-archive","ti-anchor","ti-align-right","ti-align-left","ti-align-justify","ti-align-center","ti-alert","ti-alarm-clock","ti-agenda","ti-write","ti-window","ti-widgetized","ti-widget","ti-widget-alt","ti-wallet","ti-video-clapper","ti-video-camera","ti-vector","ti-themify-logo","ti-themify-favicon","ti-themify-favicon-alt","ti-support","ti-stamp","ti-split-v-alt","ti-slice","ti-shortcode","ti-shift-right-alt","ti-shift-left-alt","ti-ruler-alt-2","ti-receipt","ti-pin2","ti-pin-alt","ti-pencil-alt2","ti-palette","ti-more","ti-more-alt","ti-microphone-alt","ti-magnet","ti-line-double","ti-line-dotted","ti-line-dashed","ti-layout-width-full","ti-layout-width-default","ti-layout-width-default-alt","ti-layout-tab","ti-layout-tab-window","ti-layout-tab-v","ti-layout-tab-min","ti-layout-slider","ti-layout-slider-alt","ti-layout-sidebar-right","ti-layout-sidebar-none","ti-layout-sidebar-left","ti-layout-placeholder","ti-layout-menu","ti-layout-menu-v","ti-layout-menu-separated","ti-layout-menu-full","ti-layout-media-right-alt","ti-layout-media-right","ti-layout-media-overlay","ti-layout-media-overlay-alt","ti-layout-media-overlay-alt-2","ti-layout-media-left-alt","ti-layout-media-left","ti-layout-media-center-alt","ti-layout-media-center","ti-layout-list-thumb","ti-layout-list-thumb-alt","ti-layout-list-post","ti-layout-list-large-image","ti-layout-line-solid","ti-layout-grid4","ti-layout-grid3","ti-layout-grid2","ti-layout-grid2-thumb","ti-layout-cta-right","ti-layout-cta-left","ti-layout-cta-center","ti-layout-cta-btn-right","ti-layout-cta-btn-left","ti-layout-column4","ti-layout-column3","ti-layout-column2","ti-layout-accordion-separated","ti-layout-accordion-merged","ti-layout-accordion-list","ti-ink-pen","ti-info-alt","ti-help-alt","ti-headphone-alt","ti-hand-point-up","ti-hand-point-right","ti-hand-point-left","ti-hand-point-down","ti-gallery","ti-face-smile","ti-face-sad","ti-credit-card","ti-control-skip-forward","ti-control-skip-backward","ti-control-record","ti-control-eject","ti-comments-smiley","ti-brush-alt","ti-youtube","ti-vimeo","ti-twitter","ti-time","ti-tumblr","ti-skype","ti-share","ti-share-alt","ti-rocket","ti-pinterest","ti-new-window","ti-microsoft","ti-list-ol","ti-linkedin","ti-layout-sidebar-2","ti-layout-grid4-alt","ti-layout-grid3-alt","ti-layout-grid2-alt","ti-layout-column4-alt","ti-layout-column3-alt","ti-layout-column2-alt","ti-instagram","ti-google","ti-github","ti-flickr","ti-facebook","ti-dropbox","ti-dribbble","ti-apple","ti-android","ti-save","ti-save-alt","ti-yahoo","ti-wordpress","ti-vimeo-alt","ti-twitter-alt","ti-tumblr-alt","ti-trello","ti-stack-overflow","ti-soundcloud","ti-sharethis","ti-sharethis-alt","ti-reddit","ti-pinterest-alt","ti-microsoft-alt","ti-linux","ti-jsfiddle","ti-joomla","ti-html5","ti-flickr-alt","ti-email","ti-drupal","ti-dropbox-alt","ti-css3","ti-rss","ti-rss-alt"],
					"requiredBy":"[data-bullet*=\"ti-\"],[class*=\"modal\"],.slides,ul.accordion"
				},
				{
					"name":"Font Awesome",
					"url":"http://goo.gl/oFgIWn",
					"headString":"&lt;link href=&quot;css/font-awesome.min.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot; media=&quot;all&quot;&gt;",
					"iconPrefix":"fa-",
					"iconClass":"fa",
					"icons":["fa-glass","fa-music","fa-search","fa-envelope-o","fa-heart","fa-star","fa-star-o","fa-user","fa-film","fa-th-large","fa-th","fa-th-list","fa-check","fa-remove","fa-close","fa-times","fa-search-plus","fa-search-minus","fa-power-off","fa-signal","fa-gear","fa-cog","fa-trash-o","fa-home","fa-file-o","fa-clock-o","fa-road","fa-download","fa-arrow-circle-o-down","fa-arrow-circle-o-up","fa-inbox","fa-play-circle-o","fa-rotate-right","fa-repeat","fa-refresh","fa-list-alt","fa-lock","fa-flag","fa-headphones","fa-volume-off","fa-volume-down","fa-volume-up","fa-qrcode","fa-barcode","fa-tag","fa-tags","fa-book","fa-bookmark","fa-print","fa-camera","fa-font","fa-bold","fa-italic","fa-text-height","fa-text-width","fa-align-left","fa-align-center","fa-align-right","fa-align-justify","fa-list","fa-dedent","fa-outdent","fa-indent","fa-video-camera","fa-photo","fa-image","fa-picture-o","fa-pencil","fa-map-marker","fa-adjust","fa-tint","fa-edit","fa-pencil-square-o","fa-share-square-o","fa-check-square-o","fa-arrows","fa-step-backward","fa-fast-backward","fa-backward","fa-play","fa-pause","fa-stop","fa-forward","fa-fast-forward","fa-step-forward","fa-eject","fa-chevron-left","fa-chevron-right","fa-plus-circle","fa-minus-circle","fa-times-circle","fa-check-circle","fa-question-circle","fa-info-circle","fa-crosshairs","fa-times-circle-o","fa-check-circle-o","fa-ban","fa-arrow-left","fa-arrow-right","fa-arrow-up","fa-arrow-down","fa-mail-forward","fa-share","fa-expand","fa-compress","fa-plus","fa-minus","fa-asterisk","fa-exclamation-circle","fa-gift","fa-leaf","fa-fire","fa-eye","fa-eye-slash","fa-warning","fa-exclamation-triangle","fa-plane","fa-calendar","fa-random","fa-comment","fa-magnet","fa-chevron-up","fa-chevron-down","fa-retweet","fa-shopping-cart","fa-folder","fa-folder-open","fa-arrows-v","fa-arrows-h","fa-bar-chart-o","fa-bar-chart","fa-twitter-square","fa-facebook-square","fa-camera-retro","fa-key","fa-gears","fa-cogs","fa-comments","fa-thumbs-o-up","fa-thumbs-o-down","fa-star-half","fa-heart-o","fa-sign-out","fa-linkedin-square","fa-thumb-tack","fa-external-link","fa-sign-in","fa-trophy","fa-github-square","fa-upload","fa-lemon-o","fa-phone","fa-square-o","fa-bookmark-o","fa-phone-square","fa-twitter","fa-facebook-f","fa-facebook","fa-github","fa-unlock","fa-credit-card","fa-rss","fa-hdd-o","fa-bullhorn","fa-bell","fa-certificate","fa-hand-o-right","fa-hand-o-left","fa-hand-o-up","fa-hand-o-down","fa-arrow-circle-left","fa-arrow-circle-right","fa-arrow-circle-up","fa-arrow-circle-down","fa-globe","fa-wrench","fa-tasks","fa-filter","fa-briefcase","fa-arrows-alt","fa-group","fa-users","fa-chain","fa-link","fa-cloud","fa-flask","fa-cut","fa-scissors","fa-copy","fa-files-o","fa-paperclip","fa-save","fa-floppy-o","fa-square","fa-navicon","fa-reorder","fa-bars","fa-list-ul","fa-list-ol","fa-strikethrough","fa-underline","fa-table","fa-magic","fa-truck","fa-pinterest","fa-pinterest-square","fa-google-plus-square","fa-google-plus","fa-money","fa-caret-down","fa-caret-up","fa-caret-left","fa-caret-right","fa-columns","fa-unsorted","fa-sort","fa-sort-down","fa-sort-desc","fa-sort-up","fa-sort-asc","fa-envelope","fa-linkedin","fa-rotate-left","fa-undo","fa-legal","fa-gavel","fa-dashboard","fa-tachometer","fa-comment-o","fa-comments-o","fa-flash","fa-bolt","fa-sitemap","fa-umbrella","fa-paste","fa-clipboard","fa-lightbulb-o","fa-exchange","fa-cloud-download","fa-cloud-upload","fa-user-md","fa-stethoscope","fa-suitcase","fa-bell-o","fa-coffee","fa-cutlery","fa-file-text-o","fa-building-o","fa-hospital-o","fa-ambulance","fa-medkit","fa-fighter-jet","fa-beer","fa-h-square","fa-plus-square","fa-angle-double-left","fa-angle-double-right","fa-angle-double-up","fa-angle-double-down","fa-angle-left","fa-angle-right","fa-angle-up","fa-angle-down","fa-desktop","fa-laptop","fa-tablet","fa-mobile-phone","fa-mobile","fa-circle-o","fa-quote-left","fa-quote-right","fa-spinner","fa-circle","fa-mail-reply","fa-reply","fa-github-alt","fa-folder-o","fa-folder-open-o","fa-smile-o","fa-frown-o","fa-meh-o","fa-gamepad","fa-keyboard-o","fa-flag-o","fa-flag-checkered","fa-terminal","fa-code","fa-mail-reply-all","fa-reply-all","fa-star-half-empty","fa-star-half-full","fa-star-half-o","fa-location-arrow","fa-crop","fa-code-fork","fa-unlink","fa-chain-broken","fa-question","fa-info","fa-exclamation","fa-superscript","fa-subscript","fa-eraser","fa-puzzle-piece","fa-microphone","fa-microphone-slash","fa-shield","fa-calendar-o","fa-fire-extinguisher","fa-rocket","fa-maxcdn","fa-chevron-circle-left","fa-chevron-circle-right","fa-chevron-circle-up","fa-chevron-circle-down","fa-html5","fa-css3","fa-anchor","fa-unlock-alt","fa-bullseye","fa-ellipsis-h","fa-ellipsis-v","fa-rss-square","fa-play-circle","fa-ticket","fa-minus-square","fa-minus-square-o","fa-level-up","fa-level-down","fa-check-square","fa-pencil-square","fa-external-link-square","fa-share-square","fa-compass","fa-toggle-down","fa-caret-square-o-down","fa-toggle-up","fa-caret-square-o-up","fa-toggle-right","fa-caret-square-o-right","fa-euro","fa-eur","fa-gbp","fa-dollar","fa-usd","fa-rupee","fa-inr","fa-cny","fa-rmb","fa-yen","fa-jpy","fa-ruble","fa-rouble","fa-rub","fa-won","fa-krw","fa-bitcoin","fa-btc","fa-file","fa-file-text","fa-sort-alpha-asc","fa-sort-alpha-desc","fa-sort-amount-asc","fa-sort-amount-desc","fa-sort-numeric-asc","fa-sort-numeric-desc","fa-thumbs-up","fa-thumbs-down","fa-youtube-square","fa-youtube","fa-xing","fa-xing-square","fa-youtube-play","fa-dropbox","fa-stack-overflow","fa-instagram","fa-flickr","fa-adn","fa-bitbucket","fa-bitbucket-square","fa-tumblr","fa-tumblr-square","fa-long-arrow-down","fa-long-arrow-up","fa-long-arrow-left","fa-long-arrow-right","fa-apple","fa-windows","fa-android","fa-linux","fa-dribbble","fa-skype","fa-foursquare","fa-trello","fa-female","fa-male","fa-gittip","fa-gratipay","fa-sun-o","fa-moon-o","fa-archive","fa-bug","fa-vk","fa-weibo","fa-renren","fa-pagelines","fa-stack-exchange","fa-arrow-circle-o-right","fa-arrow-circle-o-left","fa-toggle-left","fa-caret-square-o-left","fa-dot-circle-o","fa-wheelchair","fa-vimeo-square","fa-turkish-lira","fa-try","fa-plus-square-o","fa-space-shuttle","fa-slack","fa-envelope-square","fa-wordpress","fa-openid","fa-institution","fa-bank","fa-university","fa-mortar-board","fa-graduation-cap","fa-yahoo","fa-google","fa-reddit","fa-reddit-square","fa-stumbleupon-circle","fa-stumbleupon","fa-delicious","fa-digg","fa-pied-piper","fa-pied-piper-alt","fa-drupal","fa-joomla","fa-language","fa-fax","fa-building","fa-child","fa-paw","fa-spoon","fa-cube","fa-cubes","fa-behance","fa-behance-square","fa-steam","fa-steam-square","fa-recycle","fa-automobile","fa-car","fa-cab","fa-taxi","fa-tree","fa-spotify","fa-deviantart","fa-soundcloud","fa-database","fa-file-pdf-o","fa-file-word-o","fa-file-excel-o","fa-file-powerpoint-o","fa-file-photo-o","fa-file-picture-o","fa-file-image-o","fa-file-zip-o","fa-file-archive-o","fa-file-sound-o","fa-file-audio-o","fa-file-movie-o","fa-file-video-o","fa-file-code-o","fa-vine","fa-codepen","fa-jsfiddle","fa-life-bouy","fa-life-buoy","fa-life-saver","fa-support","fa-life-ring","fa-circle-o-notch","fa-ra","fa-rebel","fa-ge","fa-empire","fa-git-square","fa-git","fa-hacker-news","fa-tencent-weibo","fa-qq","fa-wechat","fa-weixin","fa-send","fa-paper-plane","fa-send-o","fa-paper-plane-o","fa-history","fa-genderless","fa-circle-thin","fa-header","fa-paragraph","fa-sliders","fa-share-alt","fa-share-alt-square","fa-bomb","fa-soccer-ball-o","fa-futbol-o","fa-tty","fa-binoculars","fa-plug","fa-slideshare","fa-twitch","fa-yelp","fa-newspaper-o","fa-wifi","fa-calculator","fa-paypal","fa-google-wallet","fa-cc-visa","fa-cc-mastercard","fa-cc-discover","fa-cc-amex","fa-cc-paypal","fa-cc-stripe","fa-bell-slash","fa-bell-slash-o","fa-trash","fa-copyright","fa-at","fa-eyedropper","fa-paint-brush","fa-birthday-cake","fa-area-chart","fa-pie-chart","fa-line-chart","fa-lastfm","fa-lastfm-square","fa-toggle-off","fa-toggle-on","fa-bicycle","fa-bus","fa-ioxhost","fa-angellist","fa-cc","fa-shekel","fa-sheqel","fa-ils","fa-meanpath","fa-buysellads","fa-connectdevelop","fa-dashcube","fa-forumbee","fa-leanpub","fa-sellsy","fa-shirtsinbulk","fa-simplybuilt","fa-skyatlas","fa-cart-plus","fa-cart-arrow-down","fa-diamond","fa-ship","fa-user-secret","fa-motorcycle","fa-street-view","fa-heartbeat","fa-venus","fa-mars","fa-mercury","fa-transgender","fa-transgender-alt","fa-venus-double","fa-mars-double","fa-venus-mars","fa-mars-stroke","fa-mars-stroke-v","fa-mars-stroke-h","fa-neuter","fa-facebook-official","fa-pinterest-p","fa-whatsapp","fa-server","fa-user-plus","fa-user-times","fa-hotel","fa-bed","fa-viacoin","fa-train","fa-subway","fa-medium"],
					"requiredBy":"[data-bullet*=\"fa-\"]"
				},
				{
					"name":"ET Line",
					"url":"http://goo.gl/fqxVRW",
					"headString":"&lt;link href=&quot;css/et-line-icons.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot; media=&quot;all&quot;&gt;",
					"iconPrefix":"et-line-",
					"iconClass":"",
					"icons":["et-line-mobile","et-line-laptop","et-line-desktop","et-line-tablet","et-line-phone","et-line-document","et-line-documents","et-line-search","et-line-clipboard","et-line-newspaper","et-line-notebook","et-line-book-open","et-line-browser","et-line-calendar","et-line-presentation","et-line-picture","et-line-pictures","et-line-video","et-line-camera","et-line-printer","et-line-toolbox","et-line-briefcase","et-line-wallet","et-line-gift","et-line-bargraph","et-line-grid","et-line-expand","et-line-focus","et-line-edit","et-line-adjustments","et-line-ribbon","et-line-hourglass","et-line-lock","et-line-megaphone","et-line-shield","et-line-trophy","et-line-flag","et-line-map","et-line-puzzle","et-line-basket","et-line-envelope","et-line-streetsign","et-line-telescope","et-line-gears","et-line-key","et-line-paperclip","et-line-attachment","et-line-pricetags","et-line-lightbulb","et-line-layers","et-line-pencil","et-line-tools","et-line-tools-2","et-line-scissors","et-line-paintbrush","et-line-magnifying-glass","et-line-circle-compass","et-line-linegraph","et-line-mic","et-line-strategy","et-line-beaker","et-line-caution","et-line-recycle","et-line-anchor","et-line-profile-male","et-line-profile-female","et-line-bike","et-line-wine","et-line-hotairballoon","et-line-globe","et-line-genius","et-line-map-pin","et-line-dial","et-line-chat","et-line-heart","et-line-cloud","et-line-upload","et-line-download","et-line-target","et-line-hazardous","et-line-piechart","et-line-speedometer","et-line-global","et-line-compass","et-line-lifesaver","et-line-clock","et-line-aperture","et-line-quote","et-line-scope","et-line-alarmclock","et-line-refresh","et-line-happy","et-line-sad","et-line-facebook","et-line-twitter","et-line-googleplus","et-line-rss","et-line-tumblr","et-line-linkedin","et-line-dribbble"],
					"requiredBy":"[data-bullet*=\"et-line-\"]"
				},
				{
					"name":"Pixeden 7 Stroke",
					"url":"http://goo.gl/lcxupT",
					"headString":"&lt;link href=&quot;css/pe-icon-7-stroke.css&quot; rel=&quot;stylesheet&quot; type=&quot;text/css&quot; media=&quot;all&quot;&gt;",
					"iconPrefix":"pe-7s-",
					"iconClass":"",
					"icons":["pe-7s-album","pe-7s-arc","pe-7s-back-2","pe-7s-bandaid","pe-7s-car","pe-7s-diamond","pe-7s-door-lock","pe-7s-eyedropper","pe-7s-female","pe-7s-gym","pe-7s-hammer","pe-7s-headphones","pe-7s-helm","pe-7s-hourglass","pe-7s-leaf","pe-7s-magic-wand","pe-7s-male","pe-7s-map-2","pe-7s-next-2","pe-7s-paint-bucket","pe-7s-pendrive","pe-7s-photo","pe-7s-piggy","pe-7s-plugin","pe-7s-refresh-2","pe-7s-rocket","pe-7s-settings","pe-7s-shield","pe-7s-smile","pe-7s-usb","pe-7s-vector","pe-7s-wine","pe-7s-cloud-upload","pe-7s-cash","pe-7s-close","pe-7s-bluetooth","pe-7s-cloud-download","pe-7s-way","pe-7s-close-circle","pe-7s-id","pe-7s-angle-up","pe-7s-wristwatch","pe-7s-angle-up-circle","pe-7s-world","pe-7s-angle-right","pe-7s-volume","pe-7s-angle-right-circle","pe-7s-users","pe-7s-angle-left","pe-7s-user-female","pe-7s-angle-left-circle","pe-7s-up-arrow","pe-7s-angle-down","pe-7s-switch","pe-7s-angle-down-circle","pe-7s-scissors","pe-7s-wallet","pe-7s-safe","pe-7s-volume2","pe-7s-volume1","pe-7s-voicemail","pe-7s-video","pe-7s-user","pe-7s-upload","pe-7s-unlock","pe-7s-umbrella","pe-7s-trash","pe-7s-tools","pe-7s-timer","pe-7s-ticket","pe-7s-target","pe-7s-sun","pe-7s-study","pe-7s-stopwatch","pe-7s-star","pe-7s-speaker","pe-7s-signal","pe-7s-shuffle","pe-7s-shopbag","pe-7s-share","pe-7s-server","pe-7s-search","pe-7s-film","pe-7s-science","pe-7s-disk","pe-7s-ribbon","pe-7s-repeat","pe-7s-refresh","pe-7s-add-user","pe-7s-refresh-cloud","pe-7s-paperclip","pe-7s-radio","pe-7s-note2","pe-7s-print","pe-7s-network","pe-7s-prev","pe-7s-mute","pe-7s-power","pe-7s-medal","pe-7s-portfolio","pe-7s-like2","pe-7s-plus","pe-7s-left-arrow","pe-7s-play","pe-7s-key","pe-7s-plane","pe-7s-joy","pe-7s-photo-gallery","pe-7s-pin","pe-7s-phone","pe-7s-plug","pe-7s-pen","pe-7s-right-arrow","pe-7s-paper-plane","pe-7s-delete-user","pe-7s-paint","pe-7s-bottom-arrow","pe-7s-notebook","pe-7s-note","pe-7s-next","pe-7s-news-paper","pe-7s-musiclist","pe-7s-music","pe-7s-mouse","pe-7s-more","pe-7s-moon","pe-7s-monitor","pe-7s-micro","pe-7s-menu","pe-7s-map","pe-7s-map-marker","pe-7s-mail","pe-7s-mail-open","pe-7s-mail-open-file","pe-7s-magnet","pe-7s-loop","pe-7s-look","pe-7s-lock","pe-7s-lintern","pe-7s-link","pe-7s-like","pe-7s-light","pe-7s-less","pe-7s-keypad","pe-7s-junk","pe-7s-info","pe-7s-home","pe-7s-help2","pe-7s-help1","pe-7s-graph3","pe-7s-graph2","pe-7s-graph1","pe-7s-graph","pe-7s-global","pe-7s-gleam","pe-7s-glasses","pe-7s-gift","pe-7s-folder","pe-7s-flag","pe-7s-filter","pe-7s-file","pe-7s-expand1","pe-7s-exapnd2","pe-7s-edit","pe-7s-drop","pe-7s-drawer","pe-7s-download","pe-7s-display2","pe-7s-display1","pe-7s-diskette","pe-7s-date","pe-7s-cup","pe-7s-culture","pe-7s-crop","pe-7s-credit","pe-7s-copy-file","pe-7s-config","pe-7s-compass","pe-7s-comment","pe-7s-coffee","pe-7s-cloud","pe-7s-clock","pe-7s-check","pe-7s-chat","pe-7s-cart","pe-7s-camera","pe-7s-call","pe-7s-calculator","pe-7s-browser","pe-7s-box2","pe-7s-box1","pe-7s-bookmarks","pe-7s-bicycle","pe-7s-bell","pe-7s-battery","pe-7s-ball","pe-7s-back","pe-7s-attention","pe-7s-anchor","pe-7s-albums","pe-7s-alarm","pe-7s-airplay"],
					"requiredBy":"[data-bullet*=\"pe-7s-\"]"
				}
			]
		}
	</script>

	<script id="vpa" type="application/json">
		{
			"dynamicScripts":[
				{
					"scriptRef":"&lt;script src=\"js/flexslider.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":".slider-all-controls, .slider-paging-conrols, .slider-arrow-controls, .slider-thumb-controls, .logo-carousel, .tweets-slider"
				},
				{
					"scriptRef":"&lt;script src=\"js/lightbox.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":"[data-lightbox],.flickr-feed"
				},
				{
					"scriptRef":"&lt;script src=\"js/masonry.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":".masonry"
				},
				{
					"scriptRef":"&lt;script src=\"js/flickr.js\"&gt;&lt;/script&gt;",
					"requiredBy":".flickr-feed"
				},
				{
					"scriptRef":"&lt;script src=\"js/twitterfetcher.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":".tweets-feed"
				},
				{
					"scriptRef":"&lt;script src=\"js/spectragram.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":".instafeed"
				},
				{
					"scriptRef":"&lt;script src=\"js/ytplayer.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":".player"
				},
				{
					"scriptRef":"&lt;script src=\"js/countdown.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":".countdown"
				},
				{
					"scriptRef":"&lt;script src=\"js/smooth-scroll.min.js\"&gt;&lt;/script&gt;",
					"requiredBy":".inner-link"
				}
			]
		}
	</script>

	<script id="vpb" type="application/json">
		{
			"dynamicCSS":[
			{
				"styleRef":"&lt;link href=\"css/flexslider.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" /&gt;",
			  "requiredBy":".slider-all-controls, .slider-paging-conrols, .slider-arrow-controls, .slider-thumb-controls, .logo-carousel, .tweets-slider"
			},
			{
			  "styleRef":"&lt;link href=\"css/lightbox.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" /&gt;",
			  "requiredBy":"[data-lightbox], .flickr-feed"
			},
			{
			  "styleRef":"&lt;link href=\"css/ytplayer.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" /&gt;",
			  "requiredBy":".player"
			}
			]
		}



	</script>
	<div id="veu" class="vih">
<div class="nav-container">
</div>

<div class="main-container">
</div>

	</div>

	<script src="js/jquery.js"></script>
	<script src="js/underscore.min.js"></script>


	<script src="theme/js/bootstrap.min.js"></script>
	<script src="theme/js/flexslider.min.js"></script>
	<script src="theme/js/lightbox.min.js"></script>
	<script src="theme/js/masonry.min.js"></script>
	<script src="theme/js/flickr.js"></script>
	<script src="theme/js/twitterfetcher.min.js"></script>
	<script src="theme/js/spectragram.min.js"></script>
	<script src="theme/js/ytplayer.min.js"></script>
	<script src="theme/js/countdown.min.js"></script>
	<script src="theme/js/smooth-scroll.min.js"></script>
	<script src="theme/js/parallax.js"></script>

  <script type="text/javascript" src="js/pmbuilder.js"></script>
	<script src="js/alterClass.js"></script>
	<script src="js/jquery-ui-1.10.4.custom.min.js"></script>
	<script src="js/storage2.js"></script>
	<script src="js/reInit.js"></script>
	<script src="js/simpleModal.js"></script>
	<script src="js/jsZip.js"></script>
	<script src="js/saveAs.js"></script>
	<script src="js/pmbuilder.max.js" type="text/javascript" demo='<?php echo $demo; ?>' user='<?php echo $_SESSION["user"]["username"];?>' id="script"></script>
	<script src="customjs/createFile.js"></script>

</body>
</html>
