<?php
error_reporting(E_ERROR);
include "../includes/db_connect.php";
if (!empty($_FILES)) {
  var_dump($_FILES);
    session_start();
    if (!file_exists('../img/' . $_SESSION['user']['id'])) {
       mkdir('../img/' . $_SESSION['user']['id'], 0777, true);
    }
    $storeFolder = '../img/' . $_SESSION['user']['id'];
        $tempfile = utf8_decode($_FILES['image']['tmp_name']);
        $targetPath = $storeFolder . '/';
        $targetFile = $targetPath . $_FILES['image']['name'];
        if (move_uploaded_file($tempfile, $targetFile)) {
            echo "true from upload.php";
            try {
                $query = $mysqli->prepare("INSERT INTO user_images VALUES (NULL, ?, ?)");
                $path = $_SESSION['user']['id'].'/'.$_FILES['image']['name'];
                if ($query) {
                    $query -> bind_param('ss', $path, $_SESSION['user']['id']);
                    $query->execute();
                    echo "true";
                } else {
                    echo "error";
                }
            } catch(Exception $e) {
                echo $e;
            }
        } else {
            echo "false from upload.php";
        }

}

if(isset($_GET['data'])) {
    session_start();
    if ($_GET['data'] == 'user') {
      echo $_SESSION['user']['id'];
    } else if ($_GET['data'] == 'images') {
      try{
          $sql = "SELECT * FROM user_images WHERE user_id = " . $_SESSION['user']['id'];
          $result = $mysqli->query($sql);
          $images = array();
          if ($result) {
              while($row = $result->fetch_array()) {
                  $row['session_user'] = $_SESSION['user']['id'];
                  $images[] = $row;
              }
              echo json_encode($images);
          } else {
              echo $sql;
          }

      } catch (Exception $exception) {
          echo $exception;
      }
    }

}

if (isset($_POST["del_img_src"])) {
  try {
      $query = $mysqli->prepare("DELETE FROM user_images WHERE path = ?");
      if ($query) {
          $query -> bind_param('s', $_POST['del_img_src']);
          $query->execute();
          echo "true" . $_POST['del_img_src'];
      } else {
          echo "error";
      }
  } catch(Exception $e) {
      echo $e;
  }
}
