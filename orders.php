<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (!isset($_SESSION['user'])) {
	session_start();
}
if ($online) {
	try {
		$status = "Не обработано";
		$query = "SELECT * FROM orders WHERE admin_name = ? AND status LIKE ?";
		$stmt = $mysqli->prepare($query);
		$stmt->bind_param('ss', $_SESSION['user']['username'], $status);
		$stmt->execute();
		$result = $stmt->get_result();
	}catch(Exception $e) {
		echo $e;
	}
}

?>

<div class="nav-container">

	<nav class="bg-dark">
		<div class="nav-bar">
			<div class="module left">
				<a href="index.php">
					<img class="logo-db" src="img/post_logo_black.png" alt="Logo">
				</a>
			</div>
			<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
				<i class="ti-menu"></i>
			</div>
			<div class="module-group right">
				<div class="module left">
					<ul class="menu">
						<li>
							<a href="?page=orders">Заказы</a>
							<span class="label number"><?= $resultX->fetch_array()['order_num']; ?></span>
						</li>
					</ul>
				</div>
				<div class="module widget-handle language left">
					<ul class="menu">
						<li class="has-dropdown">
							<a href="#"><?= $_SESSION['user']['username']; ?></a>
							<ul>
								<li>
									<a href="#">настройки </a>
								</li>
								<li>
									<a href="?act=logout">выйти </a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>

		</div>
	</nav>

</div>

<div class="main-container">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h5 class="uppercase mb40">Поступившие заказы</h5>
				</div>
				<?php if ($result->num_rows > 0): ?>
					<?php while ($row = $result->fetch_array()) { $order_id = $row['id']; ?>
					<?php
					$query1 = "SELECT * FROM customers WHERE id = ?";
					$stmt1 = $mysqli->prepare($query1);
					$stmt1->bind_param('s', $row['customer_id']);
					$stmt1->execute();
					$result1 = $stmt1->get_result();
					?>
					<?php
					$query2 = "SELECT * FROM products WHERE order_id = ?";
					$stmt2 = $mysqli->prepare($query2);
					$stmt2->bind_param('s', $row['id']);
					$stmt2->execute();
					$result2 = $stmt2->get_result(); ?>
					<div class="col-md-12 mb16" style="border: 1px dashed black;">
						<h5 class="uppercase mb16">Заказ № <?php echo $row['id']; ?> <small>Общая сумма: <strong><?php echo $row['total_cost']; ?></strong></small></h5>
						<p class="lead">С какого сайта: <?php echo $row['which_site']; ?></p>
						<?php if ($row1 = $result1->fetch_array()) : ?>
							<p class="lead"><strong>Покупатель:</strong> <?php echo $row1['name'] . '. <strong>Почта:</strong> ' . $row1['email'] . ' <strong>Телефон:</strong> ' . $row1['phone'] . ' <strong>Адрес: </strong>' . $row1['city'] . ', ' . $row1['address']; ?></p>
							<?php while ($row2 = $result2->fetch_array()) { ?>
								<span><?= $row2['name'] . ': <strong>Цена: </strong>' . $row2['price'] . ' <strong>Кол-во: </strong>' . $row2['qty']; ?>&nbsp; <?php echo $row2['details'] ?></span><br>
							<?php } ?>
							<form action="index.php" method="post">
	    						<div class="select_inline">
										<label for="status">Укажите состояние заказа: </label>
										<select class="orders_select" name="status">
											<option value="Не обработано">Не обработано</option>
											<option value="Обработано">Обработано</option>
											<option value="Отказано">Отказано</option>
										</select>
										<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
										<button type="submit" name="change_status" class="btn orders_button">Подтвердить</button>
	    						</div>
							</form>
						<?php endif; ?>
<!-- where's your fucking god?! -->
					</div>
					<?php } ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</div>
