<?php
error_reporting(E_ALL & ~E_NOTICE);
if (!isset($_SESSION['user'])) {
	session_start();
}
if ($online) {
	try {
		$query = "SELECT * FROM user_sites WHERE user_id = ?";
		$stmt = $mysqli->prepare($query);
		$stmt->bind_param('s', $_SESSION['user']['id']);
		$stmt->execute();
		$result = $stmt->get_result();
	}catch(Exception $e) {
		echo $e;
	}

}

?>
<!--NAVIGATION BAR STARTS HERE-->
<div class="nav-container">

	<nav class="bg-dark">
		<div class="nav-bar">
			<div class="module left">
				<a href="index.php">
					<img class="logo-db" src="img/post_logo_black.png" alt="Logo">
				</a>
			</div>
			<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
				<i class="ti-menu"></i>
			</div>
			<div class="module-group right">
				<div class="module left">
					<ul class="menu">
						<li>
							<a href="?page=orders">Заказы</a>
							<span class="label number"><?php echo $resultX->fetch_array()['order_num']; ?></span>
						</li>
					</ul>
				</div>
				<div class="module widget-handle language left">
					<ul class="menu">
						<li class="has-dropdown">
							<a href="#"><?php echo $_SESSION['user']['username']; ?></a>
							<ul>
								<li>
									<a href="#">настройки </a>
								</li>
								<li>
									<a href="?act=logout">выйти </a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>

		</div>
	</nav>

</div>
<!--NAVIGATION BAR ENDS HERE-->

<div class="main-container">
	<section class="pm-icons-6-1483016464662 bg-secondary section-one">
		<div class="container">


			<div class="row text-center">
				<div class="col-md-12">
					<h1 class="thin">Ваши сайты<br></h1>
				</div>
			</div>

			<div class="row mb64 mb-xs-0">
				<div class="col-sm-8 text-center col-md-12">
					<p class="lead">
						Можете открыть свои сайты и продолжить там, где остановились.
					</p>
				</div>
				<div class="col-sm-12">
					<ul class="lead" data-bullet="ti-world">
						<?php
						if($result->num_rows > 0) {
							while ($row = $result->fetch_array()) { ?>
							<li>
								<span>
									<?php echo $row['name']; ?>
								</span>
								<span class="right-aligned-elements">
									<a href="<?php echo "./pmbuilder/" . $row['preview_path']; ?>">
										<i title="Посмотреть" class="ti-eye"></i>
									</a>
									<a href="<?php echo "./pmbuilder?s=". $row['id'] ."" ?>">
										<i title="Редактировать" class="ti-pencil"></i>
									</a>
									<a href="#" class='site-id' site-id="<?php echo $row['id']; ?>">
										<i title="Удалить" class="ti-close"></i>
									</a>
								</span>
							</li>
							<hr>
							<?php
						}
					} else { ?>
					<p class="lead text-center">У вас нет сохраненных сайтов <br>
						<span class="fa fa-frown-o big-icon"></span></p>
						<?php  }?>
					</ul>
				</div>
			</div>

		</div>
	</section>
	<?php
	try {

		$array = array();
		$sql = "SELECT * FROM categories";
		$query = $mysqli->query($sql);
		while($row = $query->fetch_array()) {
			$array[] = $row;
		}
	} catch(Exception $e) {
		echo $e;
	}
	?>
	<section class="pm-tabs-4-1483016446612">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="uppercase mb80">готовые шаблоны<br></h4>
					<div class="tabbed-content button-tabs vertical">
						<ul class="tabs">
							<?php for ($i=0; $i < count($array); $i++) {
								$sql2 = "SELECT * FROM templates WHERE cat_id = " . $array[$i]['id'];
								$query2 = $mysqli->query($sql2);
								?>
								<li>
									<div class="tab-title">
										<span><?php echo $array[$i]['name']; ?></span>
									</div>
									<div class="tab-content">
										<h5 class="uppercase"><?php echo $array[$i]['name']; ?></h5><hr>
										<ul class="tab-images">
											<?php while ($row = $query2->fetch_array()) { ?>
											<li class="image-caption cast-shadow hover-caption">
												<img src="./pmbuilder/<?php echo $row['image_path']; ?>" alt="arch">
												<div class="caption">
													<p>
														<a class="caption-link" href="./pmbuilder/<?php echo $row['preview_path'];?>" target="_blank"><i class="ti-eye"></i>&nbsp;Посмотреть</a>
													</p>
													<p><a href="./pmbuilder/?demo=<?php echo $row['name']; ?>" class="caption-link"><i class="ti-pencil"></i>&nbsp;Редактировать</a></p>
												</div>
											</li>
											<?php } ?>
										</ul>
									</div>
								</li>
								<?php } ?>
							</ul>
						</div>

					</div>
				</div>

			</div>

		</section>
	</div>
