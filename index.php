<?php
// header('Content-Type: text/html; charset=utf-8');
ob_start();
session_start();
include "includes/db_connect.php";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$online = false;
$page = "main.php";
if (isset($_SESSION['user'])) {
  $online = true;
}
if ($online) {
  if (isset($_POST['site_id'])) {
    try {
      $sql = "DELETE FROM user_sites WHERE id = " . $_POST['site_id'];
      echo $sql;
      if ($query = $mysqli->query($sql)) {
        $sql = "SELECT * FROM user_sites WHERE user_id = " . $_SESSION['user']['id'];
      } else {
        echo "FALSE";
      }
    } catch (Exception $e) {
      echo $e;
    }
    echo $_POST['site_id'];
    exit;
  }
  if (isset($_POST['change_status'])) {
    $order_id = $_POST['order_id'];
    $status = $_POST['status'];
    $query = $mysqli->prepare("UPDATE orders SET status = ? WHERE id = ?");
    $query->bind_param('si', $status, $order_id);
    $query->execute();
    header("Location:?page=orders");
  }
  if (isset($_GET['page'])) {
    switch ($_GET['page']) {
      case  "list":
        $page = "list.php";
        break;
      case 'orders':
        $page = 'orders.php';
        break;
      default:
        $page = "list.php";
        break;
    }
  } else {
    $page = "list.php";
  }
  if (isset($_GET['act'])) {
    if ($_GET['act'] == 'logout') {
      session_destroy();
      $online = false;
      header("Location:?page=main");
    }
  }

  try {
		$query = "SELECT COUNT(*) AS order_num FROM orders WHERE admin_name = ? AND status LIKE 'Не обработано'";
		$stmt = $mysqli->prepare($query);
		$stmt->bind_param('s', $_SESSION['user']['username']);
		$stmt->execute();
		$resultX = $stmt->get_result();
	} catch (Exception $e) {
		echo $e;
	}

} else {
  if (isset($_GET['page'])){
    $page = "main.php";
    switch ($_GET['page']) {
      case "main":
        $page = "main.php";
        break;
      default:
        $page = "main.php";
        break;
    }
  }
  if(isset($_GET['act'])) {
    if($_GET['act'] == 'login') {
      if(isset($_POST['username']) && isset($_POST['p'])) {
        $username = $_POST['username'];
        $password = $_POST['p'];
        $stmt = $mysqli->prepare("SELECT * FROM users WHERE username = ? AND password = ? LIMIT 1");
        if($stmt) {
          $stmt->bind_param('ss', $username, $password);
          $stmt->execute();
          $result = $stmt->get_result();
          if($row = $result->fetch_array()) {
            $_SESSION['user'] = $row;
            header('Location:?page=list');
          } else {
            header("Location: ./");
          }
        }
      }
    }
  }
}


?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Шаблонизатор KazPost</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--		<link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />-->
		<link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/theme-gunmetal.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="css/font-roboto.css" rel="stylesheet" type="text/css">
		<link href='http://fonts.googleapis.com/css?family=Raleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
		</head>
	<body class="scroll-assist">
		<?php
    if ($online) {
      include "$page";
		}
    else {
      include "./components/notlogged/$page";
    }
    ?>


		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/flickr.js"></script>
		<script src="js/flexslider.min.js"></script>
		<script src="js/lightbox.min.js"></script>
		<script src="js/masonry.min.js"></script>
		<script src="js/twitterfetcher.min.js"></script>
		<script src="js/spectragram.min.js"></script>
		<script src="js/ytplayer.min.js"></script>
		<script src="js/countdown.min.js"></script>
		<!-- <script src="js/smooth-scroll.min.js"></script> -->
		<script src="js/parallax.js"></script>
    <script src="js/typed.js"></script>
		<script src="js/scripts.js"></script>
    <script src="js/custom.js"></script>
	</body>
</html>
