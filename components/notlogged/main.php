<script>

</script>
<?php
	include 'includes/register.inc.php';
	include 'includes/functions.php';
	if (!empty($error_msg)) {
		echo $error_msg;
	}
?>
<div class="main-container">
	<section class="cover fullscreen image-slider slider-all-controls controls-inside parallax">
		<ul class="slides">
			<li class="overlay image-bg bg-light">
        <div class="header-link">
          <span class="header-link__content">
            POSTMARKET.KZ
          </span>
        </div>
				<!-- <div class="background-image-holder">
					<img alt="image" class="background-image" src="img/home23.jpg" />
				</div> -->
				<div class="container v-align-transform">
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1 text-center">
							<h2 class="mb40 mb-xs-16 large">Cоздайте свой сайт <span class="element"></span></h2>
              <h5>
                Простая и легкая платформа для создания <br>сайтов от АО "Казпочта"
              </h5>
							<div class="modal-container">
							  <a class="btn btn-lg btn-filled btn-modal" href="#">Создать</a>
                <div class='pm_modal'>
                  <div class="tabbed-content text-tabs">
                    <ul class="tabs">
                      <li class="active">
                        <div class="tab-title">
                          <span>Вход</span>
                        </div>
                        <div class="tab-content">
                          <form action="?act=login" method="post">
                              <input type="text" placeholder="Имя пользователя" name="username"> <br>
                              <input type="password" placeholder="Ваш пароль" name="p"> <br>
                              <input type="submit" value="Войти" class="btn btn-sm">
                          </form>
                        </div>
                      </li>
                      <li>
                        <div class="tab-title">
                          <span>Регистрация</span>
                        </div>
                        <div class="tab-content">
                          <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
          								  <input type="text" placeholder="Ваш логин" name="username"> <br>
          								  <input type="text" placeholder="Ваша почта" name="email"> <br>
          								  <input type="password" placeholder="Ваш пароль" name="p"> <br>
          								  <input type="submit" value="Зарегистрироваться" class="btn btn-sm">
          								</form>
                        </div>
                      </li>
                    </ul>
                  </div>

                </div>
							  <!-- <div class="pm_modal">
								<h4 style="text-align:center;">Для того, чтобы продолжить, пожалуйста, авторизуйтесь</h4>
								<hr>
								<form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
								  <input type="text" placeholder="Ваш логин" name="username"> <br>
								  <input type="text" placeholder="Ваша почта" name="email"> <br>
								  <input type="password" placeholder="Ваш пароль" name="p"> <br>
								  <input type="submit" value="Зарегистрироваться" class="btn btn-sm">
								</form>
							  </div> -->
							</div>
						</div>
					</div>
					<!--end of row-->
				</div>
				<!--end of container-->
			</li>
		</ul>
	</section>

		<!--reserve blocks 2-->
</div>
