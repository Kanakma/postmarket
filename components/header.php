<div class="nav-container">
    <a id="top"></a>
    <nav class="bg-dark">
        <div class="nav-bar">
            <div class="module left">
                <a href="<?php $_SERVER['DOCUMENT_ROOT']; ?>">
                
                    <img class="logo logo-light" alt="PM" src="img/post_logo_black.png" />
                </a>
            </div>
            <div class="module-group right">
                <div class="module modal-container">
                        <ul class="menu">
                            <li>
                                <a href="#" class="btn-modal">Войти</a>
                            </li>
                        </ul>
                        <div class='pm_modal'>
                            <h4>Введите свои данные</h4>
                            <hr>
                            <form action="?act=login" method="post">
                                <input type="text" placeholder="Имя пользователя" name="username"> <br>
                                <input type="password" placeholder="Ваш пароль" name="p"> <br>
                                <input type="submit" value="Войти" class="btn btn-sm">
                            </form>
                        </div>
                </div>
            </div>
            <!--end of module group-->
        </div>
    </nav>
</div>